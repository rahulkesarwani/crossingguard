package com.crossingguard;

import android.app.Application;
import android.content.Context;
import androidx.multidex.MultiDex;

import com.crossingguard.controller.IDataManager;
import com.crossingguard.di.components.ApplicationComponent;
import com.crossingguard.di.modules.ApplicationModule;
import com.crossingguard.di.components.DaggerApplicationComponent;
import com.crossingguard.service.ServiceSingleton;
import com.google.firebase.database.FirebaseDatabase;

import javax.inject.Inject;


public class MyApp extends Application {
	
	ApplicationComponent appComponent;

	@Inject
	IDataManager mIDataManager;
	private static MyApp mInstance;


	@Override
	public void onCreate() {
		super.onCreate();
		mInstance = this;
		ServiceSingleton.setAlarm(true);
		FirebaseDatabase.getInstance().setPersistenceEnabled(true);

		initializeInjector();

		// Setup handler for uncaught exceptions.
		Thread.setDefaultUncaughtExceptionHandler (this::handleUncaughtException);
	}
	
	private void initializeInjector(){
		appComponent = DaggerApplicationComponent.builder()
				.applicationModule(new ApplicationModule(this))
				.build();
		
		appComponent.inject(this);
		
	}
	
	public ApplicationComponent getAppComponent(){
		return appComponent;
	}

	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(base);
		MultiDex.install(this);
	}

	public void handleUncaughtException (Thread thread, Throwable e)
	{
		Thread.UncaughtExceptionHandler uch = Thread.getDefaultUncaughtExceptionHandler();
		e.printStackTrace(); // not all Android versions will print the stack trace automatically

		System.out.println("UncaughtException is handled!");

		System.exit(-1);
	}

	public static synchronized MyApp getInstance() {
		return mInstance;
	}

}