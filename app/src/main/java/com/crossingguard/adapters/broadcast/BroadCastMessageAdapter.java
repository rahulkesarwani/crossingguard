package com.crossingguard.adapters.broadcast;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.RecyclerView;

import com.crossingguard.R;
import com.crossingguard.models.createguard.GuardModel;
import com.crossingguard.models.firebase.BroadCastMessage;
import com.crossingguard.ui.createnewguard.CreateGuardActivity;
import com.crossingguard.utils.AppConstants;
import com.crossingguard.utils.CheckNullable;
import com.crossingguard.utils.TimeAgo;
import com.crossingguard.utils.customviews.CustomTextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BroadCastMessageAdapter extends RecyclerView.Adapter {
    private final String TAG = BroadCastMessageAdapter.class.getCanonicalName();
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;


    private Context mContext = null;
    private ArrayList<BroadCastMessage> list;
    private TimeAgo timeAgo;

    public BroadCastMessageAdapter(Context mContext, ArrayList<BroadCastMessage> list) {

        this.mContext = mContext;
        this.list = list;
        timeAgo = new TimeAgo();
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflater_broadcast_message, parent, false);
            vh = new MessageVH(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.more_progress_item, parent, false);
            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof MessageVH) {

            ((MessageVH) holder).messageTV.setText(CheckNullable.checkNullable(list.get(position).getMessage()));


            String result = timeAgo.locale(mContext).getCommentTimeAgo(list.get(position).getCreatedAt(), "dd/MM/yyyy h:mm:ss a");
            ((MessageVH) holder).createdDateTV.setText(CheckNullable.checkNullable(result));

        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class MessageVH extends RecyclerView.ViewHolder {

        @BindView(R.id.messageTV)
        CustomTextView messageTV;
        @BindView(R.id.createdDateTV)
        CustomTextView createdDateTV;


        public MessageVH(View v) {
            super(v);
            ButterKnife.bind(this, v);

        }

    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.progressBarPagination)
        ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}
