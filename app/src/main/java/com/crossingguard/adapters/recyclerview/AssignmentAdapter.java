package com.crossingguard.adapters.recyclerview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.crossingguard.R;
import com.crossingguard.models.assignments.AssignmentModel;
import com.crossingguard.utils.CheckNullable;
import com.crossingguard.utils.customviews.CustomTextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AssignmentAdapter extends RecyclerView.Adapter {
    private final String TAG = AssignmentAdapter.class.getCanonicalName();
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private Context mContext = null;
    private ArrayList<AssignmentModel> list;
    private AssignClickListener assignListerner;

    public AssignmentAdapter(Context mContext, ArrayList<AssignmentModel> list, AssignClickListener assignListerner) {

        this.mContext = mContext;
        this.list = list;
        this.assignListerner = assignListerner;
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflater_assign_adapter, parent, false);
            vh = new LocationVH(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.more_progress_item, parent, false);
            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof LocationVH) {

            String locationName = "#" + CheckNullable.checkNullable(list.get(position).getGeofenceId()) + " - " + CheckNullable.checkNullable(list.get(position).getLocationName());
            ((LocationVH) holder).placeNameTV.setText(locationName);

            TimingAssignAdapter tImingAssignAdapter = new TimingAssignAdapter(mContext, list.get(position).getTimings(), new TimingAssignAdapter.AssignClickListener() {
                @Override
                public void onAssignClicked(int childPosition) {
                    if (assignListerner != null)
                        assignListerner.onAssignClicked(position, childPosition);
                }
            });
            ((LocationVH) holder).timingRV.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false));
            ((LocationVH) holder).timingRV.setAdapter(tImingAssignAdapter);

        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class LocationVH extends RecyclerView.ViewHolder {
        @BindView(R.id.placeNameTV)
        CustomTextView placeNameTV;
        @BindView(R.id.timingRV)
        RecyclerView timingRV;


        public LocationVH(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.progressBarPagination)
        ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    public interface AssignClickListener {
        void onAssignClicked(int position, int childPosition);
    }


}
