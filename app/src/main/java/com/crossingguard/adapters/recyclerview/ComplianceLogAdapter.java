package com.crossingguard.adapters.recyclerview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.RecyclerView;

import com.crossingguard.R;
import com.crossingguard.models.compliance.ComplianceLog;
import com.crossingguard.utils.CheckNullable;
import com.crossingguard.utils.customviews.CustomTextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ComplianceLogAdapter extends RecyclerView.Adapter {
    private final String TAG = ComplianceLogAdapter.class.getCanonicalName();
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private Context mContext = null;
    private List<ComplianceLog> list;

    public ComplianceLogAdapter(Context mContext, List<ComplianceLog> list) {

        this.mContext = mContext;
        this.list = list;

    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflater_compliancelog, parent, false);
            vh = new ComplianceLogVH(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.more_progress_item, parent, false);
            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof ComplianceLogVH) {

            String locationName = CheckNullable.checkNullable(list.get(position).getLocationName())+" - "+"#"+CheckNullable.checkNullable(list.get(position).getGeofenceId());
            ((ComplianceLogVH) holder).locationNameTV.setText(locationName);

            ((ComplianceLogVH) holder).guardNameTV.setText(CheckNullable.checkNullable(list.get(position).getFirstName())+" "+CheckNullable.checkNullable(list.get(position).getLastName()));

            if(CheckNullable.checkNullable(list.get(position).getIsGuardPresent())){
                ((ComplianceLogVH) holder).currentstateIV.setImageResource(R.color.green_primary_color);
            }else{
                ((ComplianceLogVH) holder).currentstateIV.setImageResource(R.color.red_color);
            }

        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ComplianceLogVH extends RecyclerView.ViewHolder {
        @BindView(R.id.profileIV)
        CircleImageView profileIV;
        @BindView(R.id.locationNameTV)
        CustomTextView locationNameTV;
        @BindView(R.id.guardNameTV)
        CustomTextView guardNameTV;
        @BindView(R.id.currentstateIV)
        CircleImageView currentstateIV;


        public ComplianceLogVH(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.progressBarPagination)
        ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}
