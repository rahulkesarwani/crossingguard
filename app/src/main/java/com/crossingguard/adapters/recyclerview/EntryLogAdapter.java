package com.crossingguard.adapters.recyclerview;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.RecyclerView;

import com.crossingguard.R;
import com.crossingguard.models.checkins.Log;
import com.crossingguard.utils.CheckNullable;
import com.crossingguard.utils.Helper;
import com.crossingguard.utils.customviews.CustomTextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class EntryLogAdapter extends RecyclerView.Adapter {
    private final String TAG = EntryLogAdapter.class.getCanonicalName();
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;


    private Context mContext = null;
    private List<Log> list;

    public EntryLogAdapter(Context mContext, List<Log> list) {

        this.mContext = mContext;
        this.list = list;

    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflater_entry_log_adapter, parent, false);
            vh = new EntryLogVH(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.more_progress_item, parent, false);
            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof EntryLogVH) {

            String guardName = CheckNullable.checkNullable(list.get(position).getFirstName()+" "+CheckNullable.checkNullable(list.get(position).getLastName()));

            ((EntryLogVH) holder).guardNameTV.setText(guardName);

            String locationName = "#" + CheckNullable.checkNullable(list.get(position).getGeofenceId() + " - " + CheckNullable.checkNullable(list.get(position).getLocationName()));

            ((EntryLogVH) holder).locationNameTV.setText(locationName);

            if (TextUtils.isEmpty(list.get(position).getCheckIn())) {
                ((EntryLogVH) holder).intTimeTV.setText("In - --");
            } else {
                String time = Helper.convertDateFormmat(CheckNullable.checkNullable(list.get(position).getCheckIn()), "yyyy-MM-dd HH:mm:ss", "hh:mm a");
                ((EntryLogVH) holder).intTimeTV.setText("In - " + time);
            }

            if (TextUtils.isEmpty(list.get(position).getCheckOut())) {
                ((EntryLogVH) holder).outtTimeTV.setText("Out - --");
            } else {
                String time = Helper.convertDateFormmat(CheckNullable.checkNullable(list.get(position).getCheckOut()), "yyyy-MM-dd HH:mm:ss", "hh:mm a");
              if(CheckNullable.checkNullable(list.get(position).getAutoCheckout()).equalsIgnoreCase("true")){
                  ((EntryLogVH) holder).outtTimeTV.setText("Out - " + time+" (auto)");
              }else{
                  ((EntryLogVH) holder).outtTimeTV.setText("Out - " + time);
              }

              if(TextUtils.isEmpty(list.get(position).getGeoAddress())){
                    ((EntryLogVH) holder).geoAddressTV.setVisibility(View.GONE);
              }else{
                  ((EntryLogVH) holder).geoAddressTV.setVisibility(View.VISIBLE);
                  ((EntryLogVH) holder).geoAddressTV.setText(CheckNullable.checkNullable(list.get(position).getGeoAddress()));
              }

            }
        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class EntryLogVH extends RecyclerView.ViewHolder {
        @BindView(R.id.profileIV)
        CircleImageView profileIV;
        @BindView(R.id.guardNameTV)
        CustomTextView guardNameTV;
        @BindView(R.id.intTimeTV)
        CustomTextView intTimeTV;
        @BindView(R.id.outtTimeTV)
        CustomTextView outtTimeTV;
        @BindView(R.id.locationNameTV)
        CustomTextView locationNameTV;
        @BindView(R.id.geoAddressTV)
        CustomTextView geoAddressTV;

        public EntryLogVH(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.progressBarPagination)
        ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}
