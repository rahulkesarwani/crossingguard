package com.crossingguard.adapters.recyclerview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.crossingguard.R;
import com.crossingguard.models.checkins.EntryLog;
import com.crossingguard.utils.CheckNullable;
import com.crossingguard.utils.customviews.CustomTextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EntryLogGroupAdapter extends RecyclerView.Adapter {
    private final String TAG = EntryLogGroupAdapter.class.getCanonicalName();
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;


    private Context mContext = null;
    private ArrayList<EntryLog> list;

    public EntryLogGroupAdapter(Context mContext, ArrayList<EntryLog> list) {

        this.mContext = mContext;
        this.list = list;

    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflater_checkin_adapter, parent, false);
            vh = new CheckInVH(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.more_progress_item, parent, false);
            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof CheckInVH) {

            ((CheckInVH) holder).dateTV.setText(CheckNullable.checkNullable(list.get(position).getDate()));

            EntryLogAdapter entryLogAdapter = new EntryLogAdapter(mContext, list.get(position).getLog());
            ((CheckInVH) holder).recyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
            ((CheckInVH) holder).recyclerView.setAdapter(entryLogAdapter);
        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class CheckInVH extends RecyclerView.ViewHolder {
        @BindView(R.id.dateTV)
        CustomTextView dateTV;
        @BindView(R.id.recyclerView)
        RecyclerView recyclerView;

        public CheckInVH(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.progressBarPagination)
        ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}
