package com.crossingguard.adapters.recyclerview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.RecyclerView;

import com.crossingguard.R;
import com.crossingguard.models.locations.LocationModel;
import com.crossingguard.utils.CheckNullable;
import com.crossingguard.utils.customviews.CustomTextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GuardAssignmentAdapter extends RecyclerView.Adapter {
    private final String TAG = GuardAssignmentAdapter.class.getCanonicalName();
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;


    private Context mContext = null;
    private ArrayList<LocationModel> list;

    public GuardAssignmentAdapter(Context mContext, ArrayList<LocationModel> list) {

        this.mContext = mContext;
        this.list = list;

    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflater_assign_guard, parent, false);
            vh = new LocationVH(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.more_progress_item, parent, false);
            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof LocationVH) {

            String locationName = "#" + CheckNullable.checkNullable(list.get(position).getGeofence().getId()) + " - " + CheckNullable.checkNullable(list.get(position).getName());
            ((LocationVH) holder).placeNameTV.setText(locationName);

            ((LocationVH) holder).timeTV.setText(list.get(position).getAssignedFormattedTimes());

        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class LocationVH extends RecyclerView.ViewHolder {
        @BindView(R.id.placeNameTV)
        CustomTextView placeNameTV;
        @BindView(R.id.timeTV)
        CustomTextView timeTV;

        public LocationVH(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.progressBarPagination)
        ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    public interface AssignClickListener {
        void onAssignClicked(int position, int childPosition);
    }


}
