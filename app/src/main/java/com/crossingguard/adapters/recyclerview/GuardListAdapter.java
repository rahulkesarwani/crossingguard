package com.crossingguard.adapters.recyclerview;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.RecyclerView;

import com.crossingguard.R;
import com.crossingguard.models.createguard.GuardModel;
import com.crossingguard.ui.createnewguard.CreateGuardActivity;
import com.crossingguard.utils.AppConstants;
import com.crossingguard.utils.CheckNullable;
import com.crossingguard.utils.customviews.CustomTextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GuardListAdapter extends RecyclerView.Adapter {
    private final String TAG = GuardListAdapter.class.getCanonicalName();
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;


    private Context mContext = null;
    private ArrayList<GuardModel> list = new ArrayList<>();

    public GuardListAdapter(Context mContext, ArrayList<GuardModel> list) {

        this.mContext = mContext;
        this.list = list;

    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflater_guard_list_adapter, parent, false);
            vh = new GuardVH(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.more_progress_item, parent, false);
            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof GuardVH) {

            ((GuardVH) holder).nameTV.setText(CheckNullable.checkNullable(list.get(position).getFirstName()) + " " + CheckNullable.checkNullable(list.get(position).getLastName()));
        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class GuardVH extends RecyclerView.ViewHolder {

        @BindView(R.id.nameTV)
        CustomTextView nameTV;
        @BindView(R.id.rowRL)
        LinearLayout rowRL;

        public GuardVH(View v) {
            super(v);
            ButterKnife.bind(this, v);

            rowRL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(mContext, CreateGuardActivity.class);
                    intent.putExtra(AppConstants.GUARD_DATA,list.get(getAdapterPosition()));
                    mContext.startActivity(intent);
                }
            });
        }

    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.progressBarPagination)
        ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}
