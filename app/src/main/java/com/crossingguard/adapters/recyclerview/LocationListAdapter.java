package com.crossingguard.adapters.recyclerview;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.RecyclerView;

import com.crossingguard.R;
import com.crossingguard.models.locations.LocationModel;
import com.crossingguard.ui.addlocation.AddLocationActivity;
import com.crossingguard.utils.AppConstants;
import com.crossingguard.utils.CheckNullable;
import com.crossingguard.utils.customviews.CustomTextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LocationListAdapter extends RecyclerView.Adapter {
    private final String TAG = LocationListAdapter.class.getCanonicalName();
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;


    private Context mContext = null;
    private ArrayList<LocationModel> list = new ArrayList<>();

    public LocationListAdapter(Context mContext, ArrayList<LocationModel> list) {

        this.mContext = mContext;
        this.list = list;

    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflater_location_adapter, parent, false);
            vh = new LocationVH(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.more_progress_item, parent, false);
            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof LocationVH) {
            String geofenceId = CheckNullable.checkNullable(list.get(position).getGeofence().getId());
            ((LocationVH) holder).placeNameTV.setText("#" + geofenceId + " - " + CheckNullable.checkNullable(list.get(position).getName()));
            ((LocationVH) holder).locationNameTV.setText(CheckNullable.checkNullable(list.get(position).getGeofence().getAddress()));
        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class LocationVH extends RecyclerView.ViewHolder {
        @BindView(R.id.placeNameTV)
        CustomTextView placeNameTV;
        @BindView(R.id.locationNameTV)
        CustomTextView locationNameTV;
        @BindView(R.id.parentLL)
        LinearLayout parentLL;


        public LocationVH(View v) {
            super(v);
            ButterKnife.bind(this, v);

            parentLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(mContext, AddLocationActivity.class);
                    intent.putExtra(AppConstants.LOCATION_DATA,list.get(getAdapterPosition()));
                    mContext.startActivity(intent);
                }
            });
        }

    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.progressBarPagination)
        ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}
