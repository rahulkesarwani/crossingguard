package com.crossingguard.adapters.recyclerview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.RecyclerView;

import com.crossingguard.R;
import com.crossingguard.utils.customviews.CustomTextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TimingAdapter extends RecyclerView.Adapter {
    private final String TAG = TimingAdapter.class.getCanonicalName();
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;


    private Context mContext = null;
    private ArrayList<String> list = new ArrayList<>();

    public TimingAdapter(Context mContext, ArrayList<String> list) {

        this.mContext = mContext;
        this.list = list;

    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflater_timing, parent, false);
            vh = new LocationVH(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.more_progress_item, parent, false);
            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof LocationVH) {

            String time[] = list.get(position).split(";");
            ((LocationVH) holder).timingTV.setText(time[0] + " - " + time[1]);

        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class LocationVH extends RecyclerView.ViewHolder {

        @BindView(R.id.timingTV)
        CustomTextView timingTV;
        @BindView(R.id.deleteIV)
        ImageView deleteIV;

        public LocationVH(View v) {
            super(v);
            ButterKnife.bind(this, v);
            deleteIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    list.remove(getAdapterPosition());
                    notifyDataSetChanged();
                }
            });
        }

    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.progressBarPagination)
        ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}
