package com.crossingguard.adapters.recyclerview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.recyclerview.widget.RecyclerView;

import com.crossingguard.R;
import com.crossingguard.models.assignments.Timing;
import com.crossingguard.utils.CheckNullable;
import com.crossingguard.utils.customviews.CustomTextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TimingAssignAdapter extends RecyclerView.Adapter {
    private final String TAG = TimingAssignAdapter.class.getCanonicalName();
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private Context mContext = null;
    private List<Timing> list;
    private AssignClickListener assignListerner;

    public TimingAssignAdapter(Context mContext, List<Timing> list, AssignClickListener assignListerner) {

        this.mContext = mContext;
        this.list = list;
        this.assignListerner = assignListerner;
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.inflater_timing_assign_adapter, parent, false);
            vh = new LocationVH(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.more_progress_item, parent, false);
            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof LocationVH) {

            ((LocationVH) holder).timeTV.setText(CheckNullable.checkNullable(list.get(position).getTime()));

            if (list.get(position).getGuard() != null) {
                ((LocationVH) holder).assignlinkBtn.setText("Change");
                ((LocationVH) holder).guardNameTV.setText(CheckNullable.checkNullable(list.get(position).getGuard().getName()));

                ((LocationVH) holder).guardNameTV.setVisibility(View.VISIBLE);
            } else {
                ((LocationVH) holder).assignlinkBtn.setText("Assign");
                ((LocationVH) holder).guardNameTV.setVisibility(View.GONE);

            }

        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }


    public class LocationVH extends RecyclerView.ViewHolder {
        @BindView(R.id.timeTV)
        CustomTextView timeTV;
        @BindView(R.id.guardNameTV)
        CustomTextView guardNameTV;
        @BindView(R.id.assignlinkBtn)
        CustomTextView assignlinkBtn;


        public LocationVH(View v) {
            super(v);
            ButterKnife.bind(this, v);

            assignlinkBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    assignListerner.onAssignClicked(getAdapterPosition());
                }
            });
        }

    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.progressBarPagination)
        ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    public interface AssignClickListener {
        void onAssignClicked(int position);
    }


}
