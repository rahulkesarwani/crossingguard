package com.crossingguard.adapters.viewpageradapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.crossingguard.utils.customviews.FollowUseWrapHeightViewPager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rahul on 20/9/16.
 */

public class HomePagerAdapter extends FragmentPagerAdapter {

    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();
    private int mCurrentPosition = -1;
    public HomePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFragment(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);


    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }

    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        super.setPrimaryItem(container, position, object);

        if ((container instanceof FollowUseWrapHeightViewPager)) {


            if (position != mCurrentPosition) {
                Fragment fragment = (Fragment) object;
                FollowUseWrapHeightViewPager pager = (FollowUseWrapHeightViewPager) container;
                if (fragment != null && fragment.getView() != null) {
                    mCurrentPosition = position;
                    pager.onPageChanged(fragment.getView());
                }
            }
        }
    }
}

