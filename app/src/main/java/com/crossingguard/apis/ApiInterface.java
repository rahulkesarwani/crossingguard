package com.crossingguard.apis;

import com.crossingguard.models.assignments.AssignRequestModel;
import com.crossingguard.models.assignments.AssignmentListModel;
import com.crossingguard.models.checkins.CheckInOutRequestModel;
import com.crossingguard.models.checkins.EntryLogResponseModel;
import com.crossingguard.models.compliance.ComplianceResponseModel;
import com.crossingguard.models.firebase.BroadCastMessage;
import com.crossingguard.models.guardsassignedlocations.GuardLocationResponseModel;
import com.crossingguard.models.locations.LocationListModel;
import com.crossingguard.models.locations.LocationModel;
import com.crossingguard.models.createguard.GuardListModel;
import com.crossingguard.models.createguard.GuardModel;
import com.crossingguard.models.logins.LoginRequestModel;
import com.crossingguard.models.logins.LoginResponseModel;

import java.util.HashMap;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;


/**
 * Created by rahul on 28/6/17.
 */

public interface ApiInterface {


    @POST("auth/login")
    Observable<Response<LoginResponseModel>> hitLoginApi(@Body LoginRequestModel loginRequestModel);

    // guards
    @POST("guards")
    Observable<Response<LoginResponseModel>> hitCreateGuardApi(@Body GuardModel requestModel);

    @PUT("guards/{id}")
    Observable<Response<LoginResponseModel>> hitUpdateGuardApi(@Path("id") Integer id, @Body GuardModel requestModel);


    @GET("guards")
    Observable<Response<GuardListModel>> hitGuardListModel(@QueryMap HashMap<String, String> requestMap);

    // locations
    @POST("locations")
    Observable<Response<LoginResponseModel>> hitLocationAddApi(@Body LocationModel requestModel);


    // locations
    @PUT("locations/{id}")
    Observable<Response<LoginResponseModel>> hitLocationEditApi(@Path("id") int id, @Body LocationModel requestModel);


    @GET("locations")
    Observable<Response<LocationListModel>> hitLocationListApi();

    //assignments
    @GET("assign")
    Observable<Response<AssignmentListModel>> hitAssignmentListModel();


    @POST("assign")
    Observable<Response<LoginResponseModel>> hitGuardAssigmentApi(@Body AssignRequestModel requestModel);


    //checkins
    @GET("entry-log")
    Observable<Response<EntryLogResponseModel>> hitEntryLogModel(@Query("page") int page);

    @POST
    Observable<Response<LoginResponseModel>> hitCheckInOutApi(@Url String url, @Body CheckInOutRequestModel requestModel);


    //    @DELETE("assign/remove-guard")
    @HTTP(method = "DELETE", path = "assign/remove-guard", hasBody = true)
    Observable<Response<LoginResponseModel>> hitNoGuardApi(@Body AssignRequestModel requestModel);


    // both active location and compliance issues
    @GET
    Observable<Response<ComplianceResponseModel>> hitComplianceLogApi(@Url String url);


    @GET("guards/locations")
    Observable<Response<GuardLocationResponseModel>> hitGuardAssignedLocationApi();

    // send push to all guard
    @POST("sendNotification")
    Observable<Response<LoginResponseModel>> hitSendNotificationToGuard(@Body BroadCastMessage broadCastMessage);

}