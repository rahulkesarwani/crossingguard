/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.crossingguard.baseactivities;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.crossingguard.R;
import com.crossingguard.apis.ApiInterface;
import com.crossingguard.baseactivities.base.MvpView;
import com.crossingguard.di.components.ActivityComponent;
import com.crossingguard.utils.ApiUtility;
import com.crossingguard.utils.ConnectionDetector;
import com.crossingguard.utils.MySharedPreferences;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import javax.net.ssl.SSLException;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLKeyException;

import butterknife.Unbinder;
import io.reactivex.disposables.CompositeDisposable;


/**
 * Created by ilkay on 10/03/17.
 */

public abstract class BaseFragment extends Fragment implements MvpView, View.OnClickListener {

    private BaseActivity activity;
    private Unbinder mUnBinder;

    protected CompositeDisposable mCompositeDisposable;
    protected ApiInterface commonApiInterface;
    public MySharedPreferences mySharedPreferences;
    public View[] errorViews;
    public ConnectionDetector mConnectionDetector;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
        mCompositeDisposable = new CompositeDisposable();
        commonApiInterface = ApiUtility.getInstance().getRetrofit().create(ApiInterface.class);
        mySharedPreferences = new MySharedPreferences(getContext());
        mConnectionDetector = new ConnectionDetector(getActivity());
        errorViews = new View[9];
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getFragmentLayout(), container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getTitle() != 0) getActivity().setTitle(getTitle());
    }

    protected abstract int getTitle();

    protected abstract int getFragmentLayout();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity) {
            BaseActivity activity = (BaseActivity) context;
            this.activity = activity;
            activity.onFragmentAttached();
        }
    }

    @Override
    public void showLoading() {
        if (activity != null) {
            activity.showLoading();
        }
    }

    @Override
    public void hideLoading() {
        if (activity != null) {
            activity.hideLoading();
        }
    }


    @Override
    public void onDetach() {
        activity = null;
        super.onDetach();
    }

    public ActivityComponent getActivityComponent() {
        return activity.getActivityComponent();
    }

    public BaseActivity getBaseActivity() {
        return activity;
    }

    public void setUnBinder(Unbinder unBinder) {
        mUnBinder = unBinder;
    }

    @Override
    public void onDestroy() {
        if (mUnBinder != null) {
            mUnBinder.unbind();
        }
        super.onDestroy();
    }

    public interface Callback {

        void onFragmentAttached();

        void onFragmentDetached(String tag);
    }


    protected void initErrorViews(View view) {
        errorViews[0] = view.findViewById(R.id.noInternetLayout);
        errorViews[1] = view.findViewById(R.id.slowInternetLayout);
        errorViews[2] = view.findViewById(R.id.serverErrorLayout);
        errorViews[3] = view.findViewById(R.id.noDataFoundLayout);
        errorViews[4] = view.findViewById(R.id.full_screen_progress_layout_white_complete);  /* full screen progress bar with background*/
        errorViews[5] = view.findViewById(R.id.full_screen_progress_layout);  /* full screen progress bar with trasnparent (No Background)*/
        errorViews[6] = view.findViewById(R.id.no_location_found);  /* No Location Found educate user to turn on location*/
        errorViews[7] = view.findViewById(R.id.noNotificationLayout);  /* No Location Found*/
        errorViews[8] = view;
        //findViewById(R.id.error_views_layout);
        errorViews[0].findViewById(R.id.try_again_button_internet).setOnClickListener(this);
        errorViews[1].findViewById(R.id.try_again_button_slownet).setOnClickListener(this);
        errorViews[2].findViewById(R.id.try_again_button_server).setOnClickListener(this);
        errorViews[3].findViewById(R.id.try_again_button_no_data).setOnClickListener(this);
        errorViews[6].findViewById(R.id.noLocationFoundButton).setOnClickListener(this);

        errorViews[8].setVisibility(View.VISIBLE);
    }


    protected abstract void handleNoInternet();

    protected abstract void handleSlowInternet();

    protected abstract void handleServerError();


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.try_again_button_internet:
                errorViews[0].setVisibility(View.GONE);
                handleNoInternet();
                break;
            case R.id.try_again_button_slownet:
                errorViews[1].setVisibility(View.GONE);
                handleSlowInternet();
                break;
            case R.id.try_again_button_server:
                errorViews[2].setVisibility(View.GONE);
                handleServerError();
                break;
            case R.id.try_again_button_no_data:
                errorViews[3].setVisibility(View.GONE);

            case R.id.noLocationFoundButton:
                errorViews[6].setVisibility(View.GONE);

                break;
        }
    }

    protected void hideProgressBar() {
        errorViews[4].setVisibility(View.GONE);
        errorViews[5].setVisibility(View.GONE);
    }

    protected boolean isResponseOK(short code) {
        errorViews[4].setVisibility(View.GONE);
        errorViews[5].setVisibility(View.GONE);
        boolean flag;
        switch (code) {
            case 404:
                errorViews[8].setVisibility(View.VISIBLE);
                errorViews[3].setVisibility(View.VISIBLE);
                flag = false;
                break;
            case 500:
                errorViews[8].setVisibility(View.VISIBLE);
                errorViews[2].setVisibility(View.VISIBLE);
                flag = false;
                break;
            case 502:
                errorViews[8].setVisibility(View.VISIBLE);
                errorViews[2].setVisibility(View.VISIBLE);
                flag = false;
                break;
            case 302:
                errorViews[8].setVisibility(View.VISIBLE);
                errorViews[2].setVisibility(View.VISIBLE);
                flag = false;
                break;
            case 401:
                errorViews[8].setVisibility(View.VISIBLE);
                errorViews[2].setVisibility(View.VISIBLE);
                /*startActivity(new Intent(this, GuardHomeActivity.class));*/
                flag = false;
                break;
            case 400:
                errorViews[8].setVisibility(View.VISIBLE);
                errorViews[2].setVisibility(View.VISIBLE);
                flag = false;
                break;
            case 408:
//                client request time out
                errorViews[8].setVisibility(View.VISIBLE);
                errorViews[1].setVisibility(View.VISIBLE);
                flag = false;
                break;
            case 200:  //GET
                flag = true;
                break;
            case 201:  // POST
                flag = true;
                break;
            case 205: // DELETE
                flag = true;
                break;
            case 204:
                flag = false;
                errorViews[8].setVisibility(View.VISIBLE);
                errorViews[3].setVisibility(View.VISIBLE);
                break;

            default:
                flag = false;
                errorViews[8].setVisibility(View.VISIBLE);
                errorViews[2].setVisibility(View.VISIBLE);
                flag = false;
        }
        return flag;
    }

    public void handleFailure(Exception exc) {
        errorViews[4].setVisibility(View.GONE);
        errorViews[5].setVisibility(View.GONE);
        try {
            throw new Exception(exc);
        } catch (UnknownHostException e) {
            errorViews[8].setVisibility(View.VISIBLE);
            errorViews[1].setVisibility(View.VISIBLE);
        } catch (SSLHandshakeException e) {
            errorViews[8].setVisibility(View.VISIBLE);
            errorViews[1].setVisibility(View.VISIBLE);
        } catch (SSLKeyException e) {
            errorViews[8].setVisibility(View.VISIBLE);
            errorViews[1].setVisibility(View.VISIBLE);
        } catch (SSLException e) {
            errorViews[8].setVisibility(View.VISIBLE);
            errorViews[1].setVisibility(View.VISIBLE);
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            errorViews[8].setVisibility(View.VISIBLE);
            errorViews[2].setVisibility(View.VISIBLE);
        } catch (ConnectException e) {
            e.printStackTrace();
            errorViews[8].setVisibility(View.VISIBLE);
            errorViews[2].setVisibility(View.VISIBLE);
        } catch (IllegalStateException e) {
            errorViews[8].setVisibility(View.VISIBLE);
            errorViews[2].setVisibility(View.VISIBLE);
            e.printStackTrace();
        } catch (Exception e) {
            errorViews[8].setVisibility(View.VISIBLE);
            errorViews[1].setVisibility(View.VISIBLE);
            e.printStackTrace();
        } catch (Throwable e) {
            errorViews[8].setVisibility(View.VISIBLE);
            errorViews[1].setVisibility(View.VISIBLE);
            e.printStackTrace();
        }
    }


    protected boolean checkBeforeApiHit() {

        if (mConnectionDetector.isInternetConnected()) {
            errorViews[4].setVisibility(View.VISIBLE);
            return true;
        } else {
            errorViews[0].setVisibility(View.VISIBLE);
            return false;
        }
    }


    protected boolean checkBeforeApiHit(int page) {

        if (mConnectionDetector.isInternetConnected()) {
            if (page == 1)
                errorViews[4].setVisibility(View.VISIBLE);
            return true;
        } else {
            errorViews[0].setVisibility(View.VISIBLE);
            return false;
        }
    }

    public void showToast(String msg) {
        if (msg != null)
            Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }
}
