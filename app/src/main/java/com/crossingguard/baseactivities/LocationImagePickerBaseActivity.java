package com.crossingguard.baseactivities;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AlertDialog;

import android.util.Log;

import com.crossingguard.BuildConfig;
import com.crossingguard.R;
import com.crossingguard.utils.AppConstants;
import com.crossingguard.utils.CompressImage;
import com.crossingguard.utils.MySharedPreferences;
import com.crossingguard.utils.UploadUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by rahul on 1/5/2018.
 */

public abstract class LocationImagePickerBaseActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    /*for Gallery Camera Picture Pick*/
    protected static final int SELECT_PICTURE = 102;
    protected static final int REQUEST_IMAGE_CAPTURE = 103;
    private UploadUtils uploadUtils;
    private static final int REQUEST_GALLERY_CODE_SETTINGS = 170;
    private boolean isPhotoClicked;
    private CompressImage compressImage;

    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private static final int REQUEST_CODE_PERMISSION_GALLERY = 10;

    private static final int REQUEST_CODE_PERMISSION_STORAGE = 11;
    private boolean isOpenCamera;
    private boolean mEnterFromGallerySetting;
    protected String resultPath = "";
    private final String TAG = "LocationBaseActivity";

    /*For Location Pick*/
    private GoogleApiClient mGoogleApiClient;
    protected Location mLastLocation;
    private static final int REQUEST_CODE_SETTING = 168;
    private String mCurrentLatitute;
    private String mCurrentLongitute;
    private static final int REQUEST_CODE_PERMISSION = 1;
    private List<Address> address = new ArrayList<>();
    private Geocoder mGeocoder;

    private LocationRequest mLocationRequest;
    private static final int REQUEST_CHECK_SETTINGS = 0x1;

    private MySharedPreferences mySharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        compressImage = new CompressImage();
        uploadUtils = new UploadUtils(this);
        mySharedPreferences = new MySharedPreferences(this);
    }


    protected void pickFromCamera() {
        isOpenCamera = true;
        if (Build.VERSION.SDK_INT >= 23) {
            try {
                verifyCameraPermissions();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {

            try {
                openCamera();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public void verifyCameraPermissions() throws IOException {
        int permission = ActivityCompat.checkSelfPermission(LocationImagePickerBaseActivity.this, Manifest.permission.CAMERA);
        int camera = ActivityCompat.checkSelfPermission(LocationImagePickerBaseActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permission != PackageManager.PERMISSION_GRANTED || camera != PackageManager.PERMISSION_GRANTED) {
            //Toast.makeText(this, "permission is not granted", Toast.LENGTH_SHORT).show();
            ActivityCompat.requestPermissions(LocationImagePickerBaseActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_PERMISSION_GALLERY);
        } else {

            openCamera();
            //    Toast.makeText(this, "permission is already granted", Toast.LENGTH_SHORT).show();
        }
    }

    private void requestCameraPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            //    Toast.makeText(this, "shouldShowRequestPermissionRationale_if", Toast.LENGTH_SHORT).show();
        }
        ActivityCompat.requestPermissions(LocationImagePickerBaseActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_PERMISSION_GALLERY);
    }


    private void openGalleryPermissionDailogDetail() {
        AlertDialog.Builder alert = new AlertDialog.Builder(
                this);
        alert.setTitle(getResources().getString(R.string.permission));
        alert.setMessage(getResources().getString(R.string.permissionneededtotakeyoupicture));
        alert.setPositiveButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        });
        alert.setNegativeButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                requestCameraPermission();
            }
        });
        alert.show();
    }

    public void openGalleryAppPermissionDailog() {
        AlertDialog.Builder alert = new AlertDialog.Builder(
                this);
        alert.setTitle(getResources().getString(R.string.grantpermission));
        alert.setMessage(getResources().getString(R.string.opensettingsforopencamera));
        alert.setPositiveButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        });
        alert.setNegativeButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent myAppSettings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getPackageName()));

                startActivityForResult(myAppSettings, REQUEST_GALLERY_CODE_SETTINGS);
            }
        });
        alert.show();

    }


    protected void pickFromGallery() {
        isOpenCamera = false;
        if (Build.VERSION.SDK_INT >= 23) {
            verifyStoragePermissions();
        } else {
            openGallery();
        }
    }


    private void openCamera() throws IOException {
        isPhotoClicked = false;
        resultPath = "";
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                return;
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Log.d(TAG, "openCamera: " + photoFile);
                Uri photoURI = FileProvider.getUriForFile(LocationImagePickerBaseActivity.this,
                        BuildConfig.APPLICATION_ID + ".provider",
                        createImageFile());

                List<ResolveInfo> resInfoList = getPackageManager().queryIntentActivities(takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);
                for (ResolveInfo resolveInfo : resInfoList) {
                    String packageName = resolveInfo.activityInfo.packageName;
                    grantUriPermission(packageName, photoURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Camera");
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        //  resultPath = "file:" + image.getAbsolutePath();
        resultPath = image.getAbsolutePath();
        return image;
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            isPhotoClicked = true;
            if (requestCode == SELECT_PICTURE) {
                Uri selectedFileURI = data.getData();
                String schemeOfUri = selectedFileURI.getScheme();

                Log.d(TAG, "onActivityResult: uri" + schemeOfUri);
                if (schemeOfUri.equals("content")) {
                    try {

                        resultPath = uploadUtils.getDriveFileAbsolutePath(selectedFileURI);
                        Log.d(TAG, "onActivityResult: " + resultPath);
                        if (resultPath.endsWith(".JPG") || resultPath.endsWith(".JPEG") || resultPath.endsWith(".PNG") || resultPath.endsWith(".jpg") || resultPath.endsWith(".jpeg") || resultPath.endsWith(".png")) {

                            // Glide.with(this).load(resultPath).asBitmap().centerCrop().placeholder(R.drawable.ic_profile).into(simpleDraweeView);

                        } else {
                            //  HandleAPIUtility.showToast(LocationImagePickerBaseActivity.this, getResources().getString(R.string.formatnotvalid_string));
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();

                    }

                } else {

                    Log.d(TAG, "File : " + uploadUtils.getPath(this, selectedFileURI));
                    resultPath = uploadUtils.getPath(this, selectedFileURI);
                    if (resultPath.endsWith(".JPG") || resultPath.endsWith(".JPEG") || resultPath.endsWith(".PNG") || resultPath.endsWith(".jpg") || resultPath.endsWith(".jpeg") || resultPath.endsWith(".png")) {
                        //    resultPath.replace(".JPG",".jpg");
                        //   Glide.with(this).load(resultPath).asBitmap().centerCrop().placeholder(R.drawable.ic_profile).into(simpleDraweeView);
                    } else {
                        //  HandleAPIUtility.showToast(LocationImagePickerBaseActivity.this, getResources().getString(R.string.formatnotvalid_string));
                    }
                }

                resultPath = compressImage.compressImage(resultPath);
                Log.d(TAG, "onActivityResult: " + resultPath);
            }
            if (requestCode == REQUEST_IMAGE_CAPTURE) {


                Uri imageUri = null;

                imageUri = Uri.parse(resultPath);

                Log.d(TAG, "onActivityResult: " + resultPath);
                File file = new File(imageUri.getPath());
                try {
                    InputStream ims = new FileInputStream(file);
//                    Glide.with(this).load(resultPath).asBitmap().centerCrop().placeholder(R.drawable.ic_profile).into(simpleDraweeView);
                    //  ivPreview.setImageBitmap(BitmapFactory.decodeStream(ims));
                } catch (FileNotFoundException e) {
                    return;
                }

                // ScanFile so it will be appeared on Gallery
                MediaScannerConnection.scanFile(LocationImagePickerBaseActivity.this,
                        new String[]{imageUri.getPath()}, null,
                        new MediaScannerConnection.OnScanCompletedListener() {
                            public void onScanCompleted(String path, Uri uri) {
                            }
                        });

                resultPath = compressImage.compressImage(imageUri.toString());
                Log.d(TAG, "onActivityResult: " + resultPath);
            }
            if (requestCode == REQUEST_GALLERY_CODE_SETTINGS) {
                mEnterFromGallerySetting = true;
            }
            if (requestCode == REQUEST_CHECK_SETTINGS) {
                final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
                switch (requestCode) {
                    case REQUEST_CHECK_SETTINGS:
                        switch (resultCode) {
                            case Activity.RESULT_OK:
                                startLocationUpdates();
                                //getLocation();
                                break;

                            case Activity.RESULT_CANCELED:
                                //show gps not enabled message
                                Log.d(TAG, "onActivityResult: canceled");
                                if (mGoogleApiClient != null)
                                    mGoogleApiClient.disconnect();
                                break;

                            default:
                                //something went wrong message

                                break;

                        }
                }
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CODE_PERMISSION_GALLERY) {
            // BEGIN_INCLUDE(permission_result)
            // Received permission result for camera permission.
            // Check if the only required permission has been granted

            String camerapermission = permissions[0];
            String storagepermission = permissions[1];
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                // Toast.makeText(ProfilePictureWalkActivity.this, "PERMISSION_GRANTED", Toast.LENGTH_SHORT).show();

                try {
                    openCamera();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA);
                if (!showRationale) {

                    mEnterFromGallerySetting = false;
                    openGalleryAppPermissionDailog();


                    //      Toast.makeText(this, "Permission Denied with Never Ask Again", Toast.LENGTH_SHORT).show();
                } else if (Manifest.permission.CAMERA.equals(camerapermission)) {

                    //Toast.makeText(this, "Permission Denied without Never Ask Again", Toast.LENGTH_SHORT).show();
                    openGalleryPermissionDailogDetail();
                }

            } else if (grantResults[1] == PackageManager.PERMISSION_DENIED) {
                boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                if (!showRationale) {

                    mEnterFromGallerySetting = false;
                    openGalleryAppPermissionDailog();


                    // Toast.makeText(this, "Permission Denied with Never Ask Again", Toast.LENGTH_SHORT).show();
                } else if (Manifest.permission.WRITE_EXTERNAL_STORAGE.equals(storagepermission)) {

                    //  Toast.makeText(this, "Permission Denied without Never Ask Again", Toast.LENGTH_SHORT).show();
                    openGalleryPermissionDailogDetail();
                }

            }

        } else if (requestCode == REQUEST_CODE_PERMISSION_STORAGE) {
            for (int i = 0, len = permissions.length; i < len; i++) {
                String permission = permissions[i];
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    // Toast.makeText(ProfilePictureWalkActivity.this, "PERMISSION_GRANTED", Toast.LENGTH_SHORT).show();

                    openGallery();

                } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    if (!showRationale) {

                        mEnterFromGallerySetting = false;
                        openGalleryAppPermissionDailog();


                        //     Toast.makeText(this, "Permission Denied with Never Ask Again", Toast.LENGTH_SHORT).show();
                    } else if (Manifest.permission.WRITE_EXTERNAL_STORAGE.equals(permission)) {

                        // Toast.makeText(this, "Permission Denied without Never Ask Again", Toast.LENGTH_SHORT).show();
                        openGalleryPermissionDailogDetail();
                    }
                }
            }
        } else if (requestCode == REQUEST_CODE_PERMISSION) {
            // BEGIN_INCLUDE(permission_result)
            // Received permission result for camera permission.
            // Check if the only required permission has been granted
            for (int i = 0, len = permissions.length; i < len; i++) {
                String permission = permissions[i];
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    //Toast.makeText(LocationImagePickerBaseActivity.this, "PERMISSION_GRANTED", Toast.LENGTH_SHORT).show();
                    getLocation();
                } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION);
                    if (!showRationale) {
                        // user denied flagging NEVER ASK AGAIN
                        // you can either enable some fall back,
                        // disable features of your app
                        // or open another dialog explaining
                        // again the permission and directing to
                        // the app setting
                        openAppPermissionDailog();

                    } else if (Manifest.permission.ACCESS_FINE_LOCATION.equals(permission)) {
                        // showRationale(permission, R.string.permission_denied_contacts);
                        // user denied WITHOUT never ask again
                        // this is a good place to explain the user
                        // why you need the permission and ask if he want
                        // to accept it (the rationale)
                        openPermissionDailogDetail();
                    }
                }
            }
        }
    }


    public void openAppPermissionDailog() {
        AlertDialog.Builder alert = new AlertDialog.Builder(
                this);
        alert.setTitle(getResources().getString(R.string.grantpermission));
        alert.setMessage(getResources().getString(R.string.opentoturnonlocation));
        alert.setPositiveButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        });
        alert.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //dialog.dismiss();
                Intent myAppSettings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getPackageName()));
                myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
                myAppSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivityForResult(myAppSettings, REQUEST_CODE_SETTING);
                finish();

            }
        });
        alert.show();
    }


    @Override
    protected void onStop() {
        super.onStop();
        try {
            if (mGoogleApiClient.isConnected()) {
                stopLocationUpdates();
            }
        } catch (Exception e) {
        }
        try {
            if (mGoogleApiClient != null)
                mGoogleApiClient.disconnect();
        } catch (Exception ae) {

        }
        Log.d(TAG, "onStop: ");

    }


    @Override
    public void onResume() {
        super.onResume();

        if (mEnterFromGallerySetting) {

            mEnterFromGallerySetting = false;
            if (isOpenCamera) {
                try {
                    verifyCameraPermissions();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                verifyStoragePermissions();
            }
        }
    }

    public void verifyStoragePermissions() {
        int permission = ActivityCompat.checkSelfPermission(LocationImagePickerBaseActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(LocationImagePickerBaseActivity.this, PERMISSIONS_STORAGE, REQUEST_CODE_PERMISSION_STORAGE);

        } else {
            openGallery();

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // stopLocationUpdates();
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        Log.d(TAG, "Location update stopped .......................");
    }


    private void openGallery() {
        isPhotoClicked = false;
        resultPath = "";
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    SELECT_PICTURE);
        } catch (android.content.ActivityNotFoundException ex) {
        }
//        /*Intent intent = new Intent();
//        intent.setType("**/*//*");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_PICTURE);*/
    }


    protected void initLocationWithCheck() {
        if (Build.VERSION.SDK_INT >= 23) {
            verifyLocationPermissions();
        } else {
            getLocation();
        }
    }

    public void verifyLocationPermissions() {
        int permission = ActivityCompat.checkSelfPermission(LocationImagePickerBaseActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        int coarsepermission = ActivityCompat.checkSelfPermission(LocationImagePickerBaseActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION);
        if (permission != PackageManager.PERMISSION_GRANTED || coarsepermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(LocationImagePickerBaseActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_CODE_PERMISSION);
        } else {
            getLocation();
        }
    }


    private void getLocation() {
        if (BuildConfig.DEBUG)
            Log.d(TAG, "getLocation: executed");
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
//                    .addApi(Places.GEO_DATA_API)
//                    .addApi(Places.PLACE_DETECTION_API)
                    .build();
        }
        mGoogleApiClient.connect();

    }


    protected void startLocationUpdates() {


        if (Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission(LocationImagePickerBaseActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(LocationImagePickerBaseActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, LocationImagePickerBaseActivity.this);
        }
    }


    @Override
    public void onLocationChanged(Location location) {

        mCurrentLatitute = String.valueOf(location.getLatitude());
        mCurrentLongitute = String.valueOf(location.getLongitude());
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "onLocationChanged: ");
        }
        mGeocoder = new Geocoder(LocationImagePickerBaseActivity.this, Locale.getDefault());

        try {
            address = mGeocoder.getFromLocation(Double.parseDouble(mCurrentLatitute), Double.parseDouble(mCurrentLongitute), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            getAddress();
        } catch (IOException e) {

        }
    }

    private void openPermissionDailogDetail() {
        AlertDialog.Builder alert = new AlertDialog.Builder(
                this);
        alert.setTitle(R.string.denyDialogHeading);
        alert.setMessage(R.string.denyDialogDetail);
        alert.setPositiveButton(R.string.imsure, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                // take user to ActivitySelectCountry
               /* Intent intent = new Intent(ActivitySplash.this, ActivitySelectCountry.class);
                Bundle bundle = new Bundle();
                bundle.putInt("comeFrom", mComeFrom);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();*/


            }
        });
        alert.setNegativeButton(R.string.retry, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                requestLocationPermission();
            }
        });
        alert.show();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG, "onConnected: ");
        createLocationRequest();
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient,
                        builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult locationSettingsResult) {

                final Status status = locationSettingsResult.getStatus();
                Log.d(TAG, "onResult: " + status);
                //final LocationSettingsStates mLocationSettingsStates = locationSettingsResult.getLocationSettingsStates();

                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // showCurrentPlace();
                        if (ActivityCompat.checkSelfPermission(LocationImagePickerBaseActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(LocationImagePickerBaseActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

                        if (mLastLocation != null) {

                            if (BuildConfig.DEBUG) {
                                Log.d(TAG, "latitude: -  " + mLastLocation.getLatitude());
                                Log.d(TAG, "longitude: -  " + mLastLocation.getLongitude());

                            }

                            mCurrentLatitute = String.valueOf(mLastLocation.getLatitude());
                            mCurrentLongitute = String.valueOf(mLastLocation.getLongitude());

                            mGeocoder = new Geocoder(LocationImagePickerBaseActivity.this, Locale.getDefault());

                            try {
                                address = mGeocoder.getFromLocation(Double.parseDouble(mCurrentLatitute), Double.parseDouble(mCurrentLongitute), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                                getAddress();
                                if (BuildConfig.DEBUG) {
                                    Log.d(TAG, "onResult: " + address);
                                }
                                Log.d(TAG, "onResult: " + address.get(0).getAddressLine(1));

                            } catch (Exception ee) {
                                ee.printStackTrace();

                            }

                        } else {
                            startLocationUpdates();
                        }

                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied, but this can be fixed
                        // by showing the user a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(LocationImagePickerBaseActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.

                        }
                        mGoogleApiClient.disconnect();
                        break;

                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // show error message
                        break;
                    case LocationSettingsStatusCodes.CANCELED:
                        Log.d(TAG, "onResult: canceled");
                        mGoogleApiClient.disconnect();
//                        mGoogleApiClient=null;
                        break;

                }
            }

        });
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "onConnectionSuspended: ");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        //show error message
//        finish();
        Log.d(TAG, "onConnectionFailed: ");
    }


    protected void createLocationRequest() {

        Log.d(TAG, "createLocationRequest: ");
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }


    private void getAddress() {
        if (address != null) {
            //String mUserAddLine = address.get(0).getAddressLine(0);//house no. // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String mUserAddLine = "";
            String sector = address.get(0).getSubLocality();
            String locality = address.get(0).getLocality();
            String country = address.get(0).getCountryName();

            mUserAddLine = mUserAddLine + sector + ", " + locality;
            addDatatoSharedPreference(mCurrentLatitute, mCurrentLongitute, mUserAddLine);
            Log.d(TAG, "getAddress: " + mCurrentLatitute + " " + mCurrentLongitute + " " + mUserAddLine);
            mGoogleApiClient.disconnect();

        }
    }

    private void requestLocationPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            //   Toast.makeText(this, "shouldShowRequestPermissionRationale_if", Toast.LENGTH_SHORT).show();
        }
        ActivityCompat.requestPermissions(LocationImagePickerBaseActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_CODE_PERMISSION);
    }


    private void addDatatoSharedPreference(String lat, String lon, String address) {
        mySharedPreferences.putString(AppConstants.ADDRESS, address);
        mySharedPreferences.putString(AppConstants.LATITUDE, lat);
        mySharedPreferences.putString(AppConstants.LONGITUDE, lon);
        gotCurrentLocation();
    }

    protected abstract void gotCurrentLocation();

}
