package com.crossingguard.controller;

import android.content.Context;


import com.crossingguard.apis.ApiInterface;
import com.crossingguard.controller.api.IApiHelper;
import com.crossingguard.controller.firebase.IFirebaseHelper;
import com.crossingguard.controller.pref.IPreferenceHelper;
import com.crossingguard.di.annotations.ApplicationContext;
import com.crossingguard.utils.MySharedPreferences;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.disposables.CompositeDisposable;


/**
 * Created by ilkay on 11/03/2017.
 */

@Singleton
public class DataManager implements IDataManager {

    private final Context mContext;
    private final IPreferenceHelper mIPreferenceHelper;
    private final IApiHelper mIApiHelper;
    private final IFirebaseHelper iFirebaseHelper;

    @Inject
    public DataManager(@ApplicationContext Context mContext, IPreferenceHelper mIPreferenceHelper, IApiHelper mIApiHelper, IFirebaseHelper iFirebaseHelper) {
        this.mContext = mContext;
        this.mIPreferenceHelper = mIPreferenceHelper;
        this.mIApiHelper = mIApiHelper;
        this.iFirebaseHelper = iFirebaseHelper;
    }

    @Override
    public boolean getDatabaseCreatedStatus() {
        return mIPreferenceHelper.getDatabaseCreatedStatus();
    }

    @Override
    public void setDatabaseCreatedStatus() {
        mIPreferenceHelper.setDatabaseCreatedStatus();
    }

    @Override
    public CompositeDisposable getCompositeDisposable() {
        return mIApiHelper.getCompositeDisposable();
    }

    @Override
    public ApiInterface getApiInterface() {
        return mIApiHelper.getApiInterface();
    }

    @Override
    public boolean isInternetConnected() {
        return mIApiHelper.isInternetConnected();
    }

    @Override
    public MySharedPreferences getMySharedPreferences() {
        return null;
    }

    @Override
    public FirebaseDatabase getFirebaseRootNode() {
        return iFirebaseHelper.getFirebaseRootNode();
    }

    @Override
    public DatabaseReference getDatabaseRef(String nodeName) {
        return iFirebaseHelper.getDatabaseRef(nodeName);
    }
}
