package com.crossingguard.controller.firebase;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.crossingguard.di.annotations.ApplicationContext;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import javax.inject.Inject;
import javax.inject.Singleton;


/**
 * Created by iaktas on 10.03.2017.
 */

@Singleton
public class FirebaseHelper implements IFirebaseHelper {


    FirebaseDatabase rootNode;

    @Inject
    public FirebaseHelper() {
        rootNode = FirebaseDatabase.getInstance();
    }

    @Override
    public FirebaseDatabase getFirebaseRootNode() {
        return rootNode;
    }

    @Override
    public DatabaseReference getDatabaseRef(String nodeName) {
        return rootNode.getReference(nodeName);
    }
}
