package com.crossingguard.db.dao;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.crossingguard.db.entity.CheckInOutDBModel;

import io.reactivex.Maybe;

@Dao
public interface CheckInOutEventDao {


    @Query("DELETE FROM CheckInOutDBModel")
    public void nukeTable();

    @Insert
    long add(CheckInOutDBModel checkInOutDBModel);

    @Query("DELETE FROM CheckInOutDBModel where id=:id")
    void delete(long id);

    @Query("SELECT * FROM CheckInOutDBModel order by updatedAt asc limit 1")
    Maybe<CheckInOutDBModel> getPendingTaskFromQueue();

    @Query("UPDATE CheckInOutDBModel SET updatedAt = :updatedAt WHERE id = :id")
    void updateProcessingTime(long id, Long updatedAt);

    @Update
    void updateVideoEntity(CheckInOutDBModel videoEntityModel);

    @Query("SELECT * FROM CheckInOutDBModel where id =:id")
    Maybe<CheckInOutDBModel> getById(long id);

}
