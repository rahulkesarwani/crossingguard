package com.crossingguard.db.entity;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class CheckInOutDBModel implements Parcelable {

    @NonNull
    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    @SerializedName("id")
    @Expose
    private Long id;


    @ColumnInfo(name = "eventType")
    @SerializedName("eventType")
    @Expose
    private String eventType;

    @ColumnInfo(name = "locationId")
    @SerializedName("locationId")
    @Expose
    private Integer locationId;

    @ColumnInfo(name = "userId")
    @SerializedName("userId")
    @Expose
    private Integer userId;

    @ColumnInfo(name = "createdAt")
    @SerializedName("createdAt")
    @Expose
    private Long createdAt;

    @ColumnInfo(name = "updatedAt")
    @SerializedName("updatedAt")
    @Expose
    private Long updatedAt;

    @ColumnInfo(name = "secret")
    @SerializedName("secret")
    @Expose
    private String secret;

    @ColumnInfo(name = "geoLng")
    @SerializedName("geoLng")
    @Expose
    private String geoLng;

    @ColumnInfo(name = "geoLat")
    @SerializedName("geoLat")
    @Expose
    private String geoLat;

    @ColumnInfo(name = "geoAddress")
    @SerializedName("geoAddress")
    @Expose
    private String geoAddress;

    public final static Creator<CheckInOutDBModel> CREATOR = new Creator<CheckInOutDBModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public CheckInOutDBModel createFromParcel(Parcel in) {
            return new CheckInOutDBModel(in);
        }

        public CheckInOutDBModel[] newArray(int size) {
            return (new CheckInOutDBModel[size]);
        }

    };

    protected CheckInOutDBModel(Parcel in) {
        this.id = ((Long) in.readValue((Long.class.getClassLoader())));
        this.eventType = ((String) in.readValue((String.class.getClassLoader())));
        this.userId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.locationId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.createdAt = ((Long) in.readValue((Long.class.getClassLoader())));
        this.updatedAt = ((Long) in.readValue((Long.class.getClassLoader())));
        this.secret = ((String) in.readValue((String.class.getClassLoader())));
        this.geoLng = ((String) in.readValue((String.class.getClassLoader())));
        this.geoLat = ((String) in.readValue((String.class.getClassLoader())));
        this.geoAddress = ((String) in.readValue((String.class.getClassLoader())));
    }

    public CheckInOutDBModel() {
    }


    @Override
    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(eventType);
        dest.writeValue(userId);
        dest.writeValue(locationId);
        dest.writeValue(createdAt);
        dest.writeValue(updatedAt);
        dest.writeValue(secret);
        dest.writeValue(geoLat);
        dest.writeValue(geoLng);
        dest.writeValue(geoAddress);
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setId(@NonNull Long id) {
        this.id = id;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    @NonNull
    public Long getId() {
        return id;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getEventType() {
        return eventType;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public Long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getGeoAddress() {
        return geoAddress;
    }

    public void setGeoLat(String geoLat) {
        this.geoLat = geoLat;
    }

    public String getGeoLat() {
        return geoLat;
    }

    public void setGeoLng(String geoLng) {
        this.geoLng = geoLng;
    }

    public String getGeoLng() {
        return geoLng;
    }

    public void setGeoAddress(String geoAddress) {
        this.geoAddress = geoAddress;
    }
}
