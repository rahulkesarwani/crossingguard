/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.crossingguard.di.components;



import com.crossingguard.baseactivities.BaseActivity;
import com.crossingguard.di.annotations.PerActivity;
import com.crossingguard.di.modules.ActivityModule;
import com.crossingguard.ui.addlocation.AddLocationActivity;
import com.crossingguard.ui.broadcastmessage.BroadcastMessageActivity;
import com.crossingguard.ui.createnewguard.CreateGuardActivity;
import com.crossingguard.ui.guardhome.GuardHomeActivity;
import com.crossingguard.ui.locationassignment.LocationAssignmentActivity;
import com.crossingguard.ui.login.LoginActivity;
import com.crossingguard.ui.profile.ProfileActivity;
import com.crossingguard.ui.supervisorhome.SuperVisorHomeActivity;

import dagger.Component;

/**
 * Created by iaktas on 24/04/17.
 */


@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {


    void inject(BaseActivity baseActivity);

    void inject(LoginActivity activity);

    void inject(SuperVisorHomeActivity activity);

    void inject(CreateGuardActivity createGuardActivity);

    void inject(ProfileActivity profileActivity);

    void inject(AddLocationActivity addLocationActivity);

    void inject(GuardHomeActivity guardHomeActivity);

    void inject(LocationAssignmentActivity locationAssignmentActivity);

    void inject(BroadcastMessageActivity broadcastMessageActivity);
}
