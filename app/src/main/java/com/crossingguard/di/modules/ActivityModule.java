package com.crossingguard.di.modules;

import android.app.Activity;


import com.crossingguard.controller.IDataManager;
import com.crossingguard.di.annotations.PerActivity;
import com.crossingguard.ui.addlocation.AddLocationMvpPresenter;
import com.crossingguard.ui.addlocation.AddLocationMvpView;
import com.crossingguard.ui.addlocation.AddLocationPresenter;
import com.crossingguard.ui.broadcastmessage.BroadcastMessageMvpPresenter;
import com.crossingguard.ui.broadcastmessage.BroadcastMessageMvpView;
import com.crossingguard.ui.broadcastmessage.BroadcastMessagePresenter;
import com.crossingguard.ui.createnewguard.CreateGuardMvpPresenter;
import com.crossingguard.ui.createnewguard.CreateGuardMvpView;
import com.crossingguard.ui.createnewguard.CreateGuardPresenter;
import com.crossingguard.ui.guardhome.GuardHomeMvpPresenter;
import com.crossingguard.ui.guardhome.GuardHomeMvpView;
import com.crossingguard.ui.guardhome.GuardHomePresenter;
import com.crossingguard.ui.locationassignment.LocationAssignmentMvpPresenter;
import com.crossingguard.ui.locationassignment.LocationAssignmentMvpView;
import com.crossingguard.ui.locationassignment.LocationAssignmentPresenter;
import com.crossingguard.ui.login.LoginMvpPresenter;
import com.crossingguard.ui.login.LoginMvpView;
import com.crossingguard.ui.login.LoginPresenter;
import com.crossingguard.ui.profile.ProfileMvpPresenter;
import com.crossingguard.ui.profile.ProfileMvpView;
import com.crossingguard.ui.profile.ProfilePresenter;
import com.crossingguard.ui.supervisorhome.SuperVisorHomeMvpPresenter;
import com.crossingguard.ui.supervisorhome.SuperVisorHomeMvpView;
import com.crossingguard.ui.supervisorhome.SuperVisorHomePresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ilkay on 10/03/2017.
 */

@Module
public class ActivityModule {
    Activity activity;

    public ActivityModule(Activity activity) {
        this.activity = activity;
    }

    @Provides
    Activity provideActivity() {
        return activity;
    }



    @Provides
    @PerActivity
    LoginMvpPresenter<LoginMvpView> providesLoginPresenter(IDataManager IDataManager) {
        return new LoginPresenter<>(IDataManager);
    }

    @Provides
    @PerActivity
    SuperVisorHomeMvpPresenter<SuperVisorHomeMvpView> providesSuperVisorHomePresenter(IDataManager IDataManager) {
        return new SuperVisorHomePresenter<>(IDataManager);
    }

    @Provides
    @PerActivity
    CreateGuardMvpPresenter<CreateGuardMvpView> providesCreateGuardPresenter(IDataManager IDataManager) {
        return new CreateGuardPresenter<>(IDataManager);
    }

    @Provides
    @PerActivity
    ProfileMvpPresenter<ProfileMvpView> providesProfilePresenter(IDataManager IDataManager) {
        return new ProfilePresenter<>(IDataManager);
    }

    @Provides
    @PerActivity
    AddLocationMvpPresenter<AddLocationMvpView> providesAddLocationPresenter(IDataManager IDataManager) {
        return new AddLocationPresenter<>(IDataManager);
    }


    @Provides
    @PerActivity
    GuardHomeMvpPresenter<GuardHomeMvpView> providesGuardomePresenter(IDataManager IDataManager) {
        return new GuardHomePresenter<>(IDataManager);
    }

    @Provides
    @PerActivity
    LocationAssignmentMvpPresenter<LocationAssignmentMvpView> providesLocationAssignmentPresenter(IDataManager IDataManager) {
        return new LocationAssignmentPresenter<>(IDataManager);
    }

    @Provides
    @PerActivity
    BroadcastMessageMvpPresenter<BroadcastMessageMvpView> providesBroadcastMessagePresenter(IDataManager IDataManager) {
        return new BroadcastMessagePresenter<>(IDataManager);
    }
}
