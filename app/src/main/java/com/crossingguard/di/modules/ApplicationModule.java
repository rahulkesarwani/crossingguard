package com.crossingguard.di.modules;

import android.app.Application;
import android.content.Context;

import com.crossingguard.controller.DataManager;
import com.crossingguard.controller.IDataManager;
import com.crossingguard.controller.api.ApiHelper;
import com.crossingguard.controller.api.IApiHelper;
import com.crossingguard.controller.firebase.FirebaseHelper;
import com.crossingguard.controller.firebase.IFirebaseHelper;
import com.crossingguard.controller.pref.IPreferenceHelper;
import com.crossingguard.controller.pref.PreferenceHelper;
import com.crossingguard.di.annotations.ApplicationContext;
import com.google.firebase.database.FirebaseDatabase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by rahul on 09/03/2017.
 */

@Module
public class ApplicationModule {
	
	private final Application app;

	public ApplicationModule(Application app) {
		this.app = app;
	}
	
	@Provides
	@ApplicationContext
    Context provideContext() {
		return app;
	}
	
	@Provides
    Application provideApplication(){
		return app;
	}
	
	@Provides
	@Singleton
	IDataManager provideDataManager(@ApplicationContext Context context, IPreferenceHelper mIPreferenceHelper, IApiHelper mIApiHelper,IFirebaseHelper iFirebaseHelper) {
		return new DataManager( context, mIPreferenceHelper, mIApiHelper,iFirebaseHelper);
	}

	/*@Provides
	@Singleton
	IDbHelper provideDbHelper(DatabaseManager databaseManager) {
		return new DbHelper(databaseManager);
	}*/

	@Provides
	@Singleton
	IPreferenceHelper providePreferencesHelper(@ApplicationContext Context context) {
		return new PreferenceHelper(context);
	}
	
	@Provides
	@Singleton
	IApiHelper provideApiHelper(@ApplicationContext Context context) {
		return new ApiHelper(context);
	}


	@Provides
	@Singleton
	IFirebaseHelper provideFirebaseHelper(@ApplicationContext Context context) {
		return new FirebaseHelper();
	}
}
