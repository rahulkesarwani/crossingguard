package com.crossingguard.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;

import androidx.appcompat.widget.AppCompatSpinner;

import com.crossingguard.R;
import com.crossingguard.models.assignments.AssignRequestModel;
import com.crossingguard.models.assignments.AssignmentModel;
import com.crossingguard.models.createguard.GuardModel;
import com.crossingguard.utils.CheckNullable;
import com.crossingguard.utils.MessagesUtils;
import com.crossingguard.utils.customviews.CustomButton;
import com.crossingguard.utils.customviews.CustomTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by rahul
 */

public class AssignGuardDialog extends Dialog {


    @BindView(R.id.locationTV)
    CustomTextView locationTV;
    @BindView(R.id.assignBtn)
    CustomButton assignBtn;
    @BindView(R.id.guardSpinner)
    AppCompatSpinner guardSpinner;
    @BindView(R.id.timeTV)
    CustomTextView timeTV;
    @BindView(R.id.noGuardBtn)
    CustomTextView noGuardBtn;

    private Context context;

    private String TAG = AssignGuardDialog.class.getCanonicalName();
    private AssignListerner okListerner;
    private AssignmentModel locationModel;
    private ArrayList<GuardModel> guardModelArrayList;
    private int childPosition;

    public AssignGuardDialog(Context context, AssignmentModel locationModel, int childPosition, ArrayList<GuardModel> guardModelArrayList) {
        super(context, R.style.mydialog_90);
        this.context = context;
        this.locationModel = locationModel;
        this.childPosition = childPosition;
        this.guardModelArrayList = guardModelArrayList;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.dialog_assignguard);
        ButterKnife.bind(this);

        initViews();

    }

    private void initViews() {

        String locationName = "Location : #" + CheckNullable.checkNullable(locationModel.getGeofenceId()) + " - " + CheckNullable.checkNullable(locationModel.getLocationName());
        locationTV.setText(locationName);
        timeTV.setText("Time : "+CheckNullable.checkNullable(locationModel.getTimings().get(childPosition).getTime()));

        if(locationModel.getTimings().get(childPosition).getGuard()!=null){
            noGuardBtn.setVisibility(View.VISIBLE);
        }else{
            noGuardBtn.setVisibility(View.INVISIBLE);
        }

        initGuardsSpinner();

    }

    @OnClick({R.id.assignBtn,R.id.noGuardBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.assignBtn:
                if (validateFields()) {

                    AssignRequestModel assignRequestModel = new AssignRequestModel();
                    assignRequestModel.setLocationId(locationModel.getLocationId());
                    assignRequestModel.setUserId(guardModelArrayList.get(((int) guardSpinner.getSelectedItemId() - 1)).getId());

                    // assign time
                    List<String> selectedTiming=new ArrayList<>();

                    String splittiming[]=locationModel.getTimings().get(childPosition).getTime().split("-");

                    selectedTiming.add(splittiming[0].trim());
                    selectedTiming.add(splittiming[1].trim());
                    assignRequestModel.setTime(selectedTiming);

                    okListerner.onDoneClicked(assignRequestModel);
                    dismiss();
                }

                break;
            case R.id.noGuardBtn:
                if(okListerner!=null){

                    AssignRequestModel assignRequestModel = new AssignRequestModel();
                    assignRequestModel.setLocationId(locationModel.getLocationId());

                    // assign time
                    List<String> selectedTiming=new ArrayList<>();
                    String splittiming[]=locationModel.getTimings().get(childPosition).getTime().split("-");

                    selectedTiming.add(splittiming[0].trim());
                    selectedTiming.add(splittiming[1].trim());
                    assignRequestModel.setTime(selectedTiming);
                    okListerner.onNoGuardClicked(assignRequestModel);
                    dismiss();
                }

                break;
        }
    }


    private boolean validateFields() {

        if (guardSpinner.getSelectedItemId() == 0) {
            MessagesUtils.showToastError(context, "Select Guard First");
            return false;
        } else {
            return true;
        }
    }


    public interface AssignListerner {
        void onDoneClicked(AssignRequestModel assignRequestModel);

        void onNoGuardClicked(AssignRequestModel assignRequestModel);
    }

    public void setonAssignListener(AssignListerner listener) {
        okListerner = listener;
    }


    private void initGuardsSpinner() {

        String[] guards = new String[guardModelArrayList.size() + 1];
        guards[0] = "Make a selection";
        for (int i = 0; i < guardModelArrayList.size(); i++) {
            guards[i + 1] = CheckNullable.checkNullable(guardModelArrayList.get(i).getFirstName()) + " " + CheckNullable.checkNullable(guardModelArrayList.get(i).getLastName());
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, guards);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        guardSpinner.setAdapter(dataAdapter);

    }
}