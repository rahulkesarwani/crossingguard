package com.crossingguard.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.borax12.materialdaterangepicker.time.RadialPickerLayout;
import com.borax12.materialdaterangepicker.time.TimePickerDialog;
import com.crossingguard.R;
import com.crossingguard.adapters.recyclerview.TimingAdapter;
import com.crossingguard.models.locations.LocationModel;
import com.crossingguard.ui.addlocation.AddLocationActivity;
import com.crossingguard.utils.CheckNullable;
import com.crossingguard.utils.customviews.CustomButton;
import com.crossingguard.utils.customviews.CustomEditText;
import com.crossingguard.utils.customviews.CustomTextView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.warkiz.tickseekbar.OnSeekChangeListener;
import com.warkiz.tickseekbar.SeekParams;
import com.warkiz.tickseekbar.TickSeekBar;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;


/**
 * Created by rahul
 */

public class NewGeofenceDialog extends Dialog implements TimePickerDialog.OnTimeSetListener {

    @BindView(R.id.geofenceIDET)
    CustomEditText geofenceIDET;
    @BindView(R.id.locationET)
    CustomEditText locationET;
    @BindView(R.id.addNewLocation)
    FloatingActionButton addNewLocation;
    @BindView(R.id.timeRV)
    RecyclerView timeRV;
    @BindView(R.id.doneBtn)
    CustomButton doneBtn;
    @BindView(R.id.seekBar)
    TickSeekBar seekBar;
    @BindView(R.id.radiusTV)
    CustomTextView radiusTV;

    private Context context;

    private String TAG = NewGeofenceDialog.class.getCanonicalName();
    private DoneListerner okListerner;
    private LocationModel addNewLocationModel;

    private TimingAdapter timingAdapter;

    private ArrayList<String> timinigsList = new ArrayList<>();
    private boolean isEditView;
    private Float radius;

    public NewGeofenceDialog(Context context, LocationModel addNewLocationModel, boolean isEditView) {
        super(context, R.style.mydialog_90);
        this.context = context;
        this.addNewLocationModel = addNewLocationModel;
        this.isEditView = isEditView;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.dialog_new_geofence);
        ButterKnife.bind(this);

        initViews();
        setupRecycler();
        if (addNewLocationModel.getGeofence() != null) {
            timinigsList.addAll(addNewLocationModel.getCoverageTimes());
            timingAdapter.notifyDataSetChanged();
            try {

                radius = addNewLocationModel.getGeofence().getRadius();
                seekBar.setProgress(addNewLocationModel.getGeofence().getRadius());

            } catch (Exception e) {
            }

            geofenceIDET.setText("" + CheckNullable.checkNullable(addNewLocationModel.getGeofence().getId()));
            locationET.setText(CheckNullable.checkNullable(addNewLocationModel.getName()));
            if (isEditView) {
                geofenceIDET.setEnabled(false);
            }
        }
    }

    private void initViews() {

        seekBar.setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {
                radius = Float.parseFloat(seekParams.tickText);
            }

            @Override
            public void onStartTrackingTouch(TickSeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(TickSeekBar seekBar) {
            }

        });
    }

    @OnClick({R.id.addNewLocation, R.id.doneBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.addNewLocation:
                showClockDialog();
                break;
            case R.id.doneBtn:
                if (validateFields()) {

                    addNewLocationModel.getGeofence().setRadius(radius);
                    addNewLocationModel.getGeofence().setId(geofenceIDET.getText().toString().trim());
                    addNewLocationModel.setName(locationET.getText().toString().trim());
                    addNewLocationModel.setCoverageTimes(timinigsList);
                    okListerner.onDoneClicked(addNewLocationModel);
                    dismiss();
                }

                break;
        }
    }


    private boolean validateFields() {

        if (geofenceIDET.getText().toString().trim().length() == 0) {
            geofenceIDET.setError("Enter GeofenceModel Id");
            return false;
        } else if (locationET.getText().toString().trim().length() == 0) {
            locationET.setError("Enter Location Name");
            return false;
        } else if (timinigsList.size() == 0) {
            Toasty.error(context, "Put timings");
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int hourOfDayEnd, int minuteEnd) {
        String hourString = hourOfDay < 10 ? "0" + hourOfDay : "" + hourOfDay;
        String minuteString = minute < 10 ? "0" + minute : "" + minute;
        String hourStringEnd = hourOfDayEnd < 10 ? "0" + hourOfDayEnd : "" + hourOfDayEnd;
        String minuteStringEnd = minuteEnd < 10 ? "0" + minuteEnd : "" + minuteEnd;
        String time = "You picked the following time: From - " + hourOfDay + "h" + minute + " To - " + hourOfDayEnd + "h" + minuteEnd;
        Log.d(TAG, "onTimeSet: " + time);


        String timing = hourString + ":" + minuteString + ";" + hourStringEnd + ":" + minuteStringEnd;
        timinigsList.add(timing);
        timingAdapter.notifyDataSetChanged();
        timeRV.scrollToPosition(timinigsList.size());
    }


    public interface DoneListerner {
        void onDoneClicked(LocationModel addNewLocationModel);
    }

    public void setonDoneListener(DoneListerner listener) {
        okListerner = listener;
    }

    private void setupRecycler() {


        timingAdapter = new TimingAdapter(context, timinigsList);
        timeRV.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        timeRV.setAdapter(timingAdapter);
    }


    private void showClockDialog() {
        Calendar now = Calendar.getInstance();
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                true
        );
        tpd.setOnTimeSetListener(this);
        tpd.show(((AddLocationActivity) context).getFragmentManager(), "Timing");
    }

}