package com.crossingguard.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Timinig implements Parcelable {

    @SerializedName("from")
    @Expose
    private String from;
    @SerializedName("to")
    @Expose
    private String to;
    public final static Creator<Timinig> CREATOR = new Creator<Timinig>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Timinig createFromParcel(Parcel in) {
            return new Timinig(in);
        }

        public Timinig[] newArray(int size) {
            return (new Timinig[size]);
        }

    };

    protected Timinig(Parcel in) {
        this.from = ((String) in.readValue((String.class.getClassLoader())));
        this.to = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Timinig() {
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(from);
        dest.writeValue(to);
    }

    public int describeContents() {
        return 0;
    }

}