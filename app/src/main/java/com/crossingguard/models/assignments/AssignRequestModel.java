package com.crossingguard.models.assignments;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AssignRequestModel implements Parcelable {

    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("location_id")
    @Expose
    private Integer locationId;
    @SerializedName("time")
    @Expose
    private List<String> time = null;
    public final static Parcelable.Creator<AssignRequestModel> CREATOR = new Creator<AssignRequestModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public AssignRequestModel createFromParcel(Parcel in) {
            return new AssignRequestModel(in);
        }

        public AssignRequestModel[] newArray(int size) {
            return (new AssignRequestModel[size]);
        }

    };

    protected AssignRequestModel(Parcel in) {
        this.userId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.locationId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        in.readList(this.time, (java.lang.String.class.getClassLoader()));
    }

    public AssignRequestModel() {
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public List<String> getTime() {
        return time;
    }

    public void setTime(List<String> time) {
        this.time = time;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(userId);
        dest.writeValue(locationId);
        dest.writeList(time);
    }

    public int describeContents() {
        return 0;
    }

}