package com.crossingguard.models.assignments;

import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AssignmentListModel implements Parcelable {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<AssignmentModel> data = null;
    public final static Parcelable.Creator<AssignmentListModel> CREATOR = new Creator<AssignmentListModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public AssignmentListModel createFromParcel(Parcel in) {
            return new AssignmentListModel(in);
        }

        public AssignmentListModel[] newArray(int size) {
            return (new AssignmentListModel[size]);
        }

    };

    protected AssignmentListModel(Parcel in) {
        this.success = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.data, (AssignmentModel.class.getClassLoader()));
    }

    public AssignmentListModel() {
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<AssignmentModel> getData() {
        return data;
    }

    public void setData(List<AssignmentModel> data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(success);
        dest.writeValue(message);
        dest.writeList(data);
    }

    public int describeContents() {
        return 0;
    }

}