package com.crossingguard.models.assignments;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class AssignmentModel implements Parcelable {


    @SerializedName("location_id")
    @Expose
    private Integer locationId;
    @SerializedName("geofence_id")
    @Expose
    private String geofenceId;
    @SerializedName("location_name")
    @Expose
    private String locationName;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("timings")
    @Expose
    private List<Timing> timings = new ArrayList<>();
    public final static Parcelable.Creator<AssignmentModel> CREATOR = new Creator<AssignmentModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public AssignmentModel createFromParcel(Parcel in) {
            return new AssignmentModel(in);
        }

        public AssignmentModel[] newArray(int size) {
            return (new AssignmentModel[size]);
        }

    };

    protected AssignmentModel(Parcel in) {
        this.locationId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.geofenceId = ((String) in.readValue((String.class.getClassLoader())));
        this.locationName = ((String) in.readValue((String.class.getClassLoader())));
        this.address = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.timings, (Timing.class.getClassLoader()));
    }

    public AssignmentModel() {
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public String getGeofenceId() {
        return geofenceId;
    }

    public void setGeofenceId(String geofenceId) {
        this.geofenceId = geofenceId;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Timing> getTimings() {
        return timings;
    }

    public void setTimings(List<Timing> timings) {
        this.timings = timings;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(locationId);
        dest.writeValue(geofenceId);
        dest.writeValue(locationName);
        dest.writeValue(address);
        dest.writeList(timings);
    }

    public int describeContents() {
        return 0;
    }


}