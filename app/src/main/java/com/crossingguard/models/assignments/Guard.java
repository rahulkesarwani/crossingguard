package com.crossingguard.models.assignments;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Guard implements Parcelable
{

@SerializedName("guard_id")
@Expose
private Integer guardId;
@SerializedName("name")
@Expose
private String name;
public final static Parcelable.Creator<Guard> CREATOR = new Creator<Guard>() {


@SuppressWarnings({
"unchecked"
})
public Guard createFromParcel(Parcel in) {
return new Guard(in);
}

public Guard[] newArray(int size) {
return (new Guard[size]);
}

}
;

protected Guard(Parcel in) {
this.guardId = ((Integer) in.readValue((Integer.class.getClassLoader())));
this.name = ((String) in.readValue((String.class.getClassLoader())));
}

public Guard() {
}

public Integer getGuardId() {
return guardId;
}

public void setGuardId(Integer guardId) {
this.guardId = guardId;
}

public String getName() {
return name;
}

public void setName(String name) {
this.name = name;
}

public void writeToParcel(Parcel dest, int flags) {
dest.writeValue(guardId);
dest.writeValue(name);
}

public int describeContents() {
return 0;
}

}