package com.crossingguard.models.assignments;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Timing implements Parcelable {

    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("guard")
    @Expose
    private Guard guard;
    public final static Parcelable.Creator<Timing> CREATOR = new Creator<Timing>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Timing createFromParcel(Parcel in) {
            return new Timing(in);
        }

        public Timing[] newArray(int size) {
            return (new Timing[size]);
        }

    };

    protected Timing(Parcel in) {
        this.time = ((String) in.readValue((String.class.getClassLoader())));
        this.guard = ((Guard) in.readValue((Guard.class.getClassLoader())));
    }

    public Timing() {
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Guard getGuard() {
        return guard;
    }

    public void setGuard(Guard guard) {
        this.guard = guard;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(time);
        dest.writeValue(guard);
    }

    public int describeContents() {
        return 0;
    }

}