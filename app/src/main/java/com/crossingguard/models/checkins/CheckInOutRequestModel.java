package com.crossingguard.models.checkins;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckInOutRequestModel implements Parcelable {

    @SerializedName("location_id")
    @Expose
    private Integer locationId;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("user_key")
    @Expose
    private String secret;
    @SerializedName("trigger_time")
    @Expose
    private String triggerTime;
    @SerializedName("geo_lat")
    @Expose
    private String geoLat;
    @SerializedName("geo_lng")
    @Expose
    private String geoLng;
    @SerializedName("geo_address")
    @Expose
    private String geoAddress;
    public final static Parcelable.Creator<CheckInOutRequestModel> CREATOR = new Creator<CheckInOutRequestModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public CheckInOutRequestModel createFromParcel(Parcel in) {
            return new CheckInOutRequestModel(in);
        }

        public CheckInOutRequestModel[] newArray(int size) {
            return (new CheckInOutRequestModel[size]);
        }

    };

    protected CheckInOutRequestModel(Parcel in) {
        this.locationId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.userId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.secret = ((String) in.readValue((String.class.getClassLoader())));
        this.triggerTime = ((String) in.readValue((String.class.getClassLoader())));
        this.geoLat = ((String) in.readValue((String.class.getClassLoader())));
        this.geoLng = ((String) in.readValue((String.class.getClassLoader())));
        this.geoAddress = ((String) in.readValue((String.class.getClassLoader())));
    }

    public CheckInOutRequestModel() {
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getTriggerTime() {
        return triggerTime;
    }

    public void setTriggerTime(String triggerTime) {
        this.triggerTime = triggerTime;
    }


    public void setGeoAddress(String geoAddress) {
        this.geoAddress = geoAddress;
    }

    public void setGeoLng(String geoLng) {
        this.geoLng = geoLng;
    }

    public void setGeoLat(String geoLat) {
        this.geoLat = geoLat;
    }

    public String getGeoLng() {
        return geoLng;
    }

    public String getGeoLat() {
        return geoLat;
    }

    public String getGeoAddress() {
        return geoAddress;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(locationId);
        dest.writeValue(userId);
        dest.writeValue(secret);
        dest.writeValue(triggerTime);
        dest.writeValue(geoLat);
        dest.writeValue(geoLng);
        dest.writeValue(geoAddress);
    }

    public int describeContents() {
        return 0;
    }

}