package com.crossingguard.models.checkins;

import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data implements Parcelable {

    @SerializedName("last_page")
    @Expose
    private Integer lastPage;
    @SerializedName("entry_log")
    @Expose
    private List<EntryLog> entryLog = null;
    public final static Parcelable.Creator<Data> CREATOR = new Creator<Data>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Data createFromParcel(Parcel in) {
            return new Data(in);
        }

        public Data[] newArray(int size) {
            return (new Data[size]);
        }

    };

    protected Data(Parcel in) {
        this.lastPage = ((Integer) in.readValue((Integer.class.getClassLoader())));
        in.readList(this.entryLog, (EntryLog.class.getClassLoader()));
    }

    public Data() {
    }

    public Integer getLastPage() {
        return lastPage;
    }

    public void setLastPage(Integer lastPage) {
        this.lastPage = lastPage;
    }

    public List<EntryLog> getEntryLog() {
        return entryLog;
    }

    public void setEntryLog(List<EntryLog> entryLog) {
        this.entryLog = entryLog;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(lastPage);
        dest.writeList(entryLog);
    }

    public int describeContents() {
        return 0;
    }

}