package com.crossingguard.models.checkins;

import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EntryLog implements Parcelable {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("log")
    @Expose
    private List<Log> log = null;
    public final static Parcelable.Creator<EntryLog> CREATOR = new Creator<EntryLog>() {


        @SuppressWarnings({
                "unchecked"
        })
        public EntryLog createFromParcel(Parcel in) {
            return new EntryLog(in);
        }

        public EntryLog[] newArray(int size) {
            return (new EntryLog[size]);
        }

    };

    protected EntryLog(Parcel in) {
        this.date = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.log, (Log.class.getClassLoader()));
    }

    public EntryLog() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<Log> getLog() {
        return log;
    }

    public void setLog(List<Log> log) {
        this.log = log;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(date);
        dest.writeList(log);
    }

    public int describeContents() {
        return 0;
    }

}