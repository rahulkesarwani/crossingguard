package com.crossingguard.models.checkins;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Log implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("check-in")
    @Expose
    private String checkIn;
    @SerializedName("check-out")
    @Expose
    private String checkOut;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("location_id")
    @Expose
    private Integer locationId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("geofence_id")
    @Expose
    private String geofenceId;
    @SerializedName("location_name")
    @Expose
    private String locationName;
    @SerializedName("is_auto_checkout")
    @Expose
    private String isAutoCheckout;
    @SerializedName("geo_lat")
    @Expose
    private String geoLat;
    @SerializedName("geo_lng")
    @Expose
    private String geoLng;
    @SerializedName("geo_address")
    @Expose
    private String geoAddress;
    public final static Parcelable.Creator<Log> CREATOR = new Creator<Log>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Log createFromParcel(Parcel in) {
            return new Log(in);
        }

        public Log[] newArray(int size) {
            return (new Log[size]);
        }

    };

    protected Log(Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.checkIn = ((String) in.readValue((String.class.getClassLoader())));
        this.checkOut = ((String) in.readValue((String.class.getClassLoader())));
        this.userId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.locationId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.createdAt = ((String) in.readValue((String.class.getClassLoader())));
        this.updatedAt = ((String) in.readValue((String.class.getClassLoader())));
        this.firstName = ((String) in.readValue((String.class.getClassLoader())));
        this.lastName = ((String) in.readValue((String.class.getClassLoader())));
        this.geofenceId = ((String) in.readValue((String.class.getClassLoader())));
        this.locationName = ((String) in.readValue((String.class.getClassLoader())));
        this.isAutoCheckout = ((String) in.readValue((String.class.getClassLoader())));
        this.geoLat = ((String) in.readValue((String.class.getClassLoader())));
        this.geoLng = ((String) in.readValue((String.class.getClassLoader())));
        this.geoAddress = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Log() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(String checkIn) {
        this.checkIn = checkIn;
    }

    public String getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(String checkOut) {
        this.checkOut = checkOut;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGeofenceId() {
        return geofenceId;
    }

    public void setGeofenceId(String geofenceId) {
        this.geofenceId = geofenceId;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getAutoCheckout() {
        return isAutoCheckout;
    }

    public void setAutoCheckout(String autoCheckout) {
        isAutoCheckout = autoCheckout;
    }


    public String getGeoAddress() {
        return geoAddress;
    }

    public String getGeoLat() {
        return geoLat;
    }

    public void setGeoLat(String geoLat) {
        this.geoLat = geoLat;
    }

    public String getGeoLng() {
        return geoLng;
    }

    public void setGeoLng(String geoLng) {
        this.geoLng = geoLng;
    }

    public void setGeoAddress(String geoAddress) {
        this.geoAddress = geoAddress;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(checkIn);
        dest.writeValue(checkOut);
        dest.writeValue(userId);
        dest.writeValue(locationId);
        dest.writeValue(createdAt);
        dest.writeValue(updatedAt);
        dest.writeValue(firstName);
        dest.writeValue(lastName);
        dest.writeValue(geofenceId);
        dest.writeValue(locationName);
        dest.writeValue(isAutoCheckout);
        dest.writeValue(geoLat);
        dest.writeValue(geoLng);
        dest.writeValue(geoAddress);
    }

    public int describeContents() {
        return 0;
    }

}