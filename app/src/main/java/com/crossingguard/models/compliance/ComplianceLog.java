package com.crossingguard.models.compliance;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ComplianceLog implements Parcelable {

    @SerializedName("geofence_id")
    @Expose
    private String geofenceId;
    @SerializedName("location_name")
    @Expose
    private String locationName;
    @SerializedName("is_guard_present")
    @Expose
    private Boolean isGuardPresent;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    public final static Parcelable.Creator<ComplianceLog> CREATOR = new Creator<ComplianceLog>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ComplianceLog createFromParcel(Parcel in) {
            return new ComplianceLog(in);
        }

        public ComplianceLog[] newArray(int size) {
            return (new ComplianceLog[size]);
        }

    };

    protected ComplianceLog(Parcel in) {
        this.geofenceId = ((String) in.readValue((String.class.getClassLoader())));
        this.locationName = ((String) in.readValue((String.class.getClassLoader())));
        this.isGuardPresent = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.firstName = ((String) in.readValue((String.class.getClassLoader())));
        this.lastName = ((String) in.readValue((String.class.getClassLoader())));
    }

    public ComplianceLog() {
    }

    public String getGeofenceId() {
        return geofenceId;
    }

    public void setGeofenceId(String geofenceId) {
        this.geofenceId = geofenceId;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public Boolean getIsGuardPresent() {
        return isGuardPresent;
    }

    public void setIsGuardPresent(Boolean isGuardPresent) {
        this.isGuardPresent = isGuardPresent;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(geofenceId);
        dest.writeValue(locationName);
        dest.writeValue(isGuardPresent);
        dest.writeValue(firstName);
        dest.writeValue(lastName);
    }

    public int describeContents() {
        return 0;
    }

}