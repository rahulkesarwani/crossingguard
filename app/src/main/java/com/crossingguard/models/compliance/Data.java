package com.crossingguard.models.compliance;

import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data implements Parcelable {

    @SerializedName("last_page")
    @Expose
    private Integer lastPage;
    @SerializedName("compliance_log")
    @Expose
    private List<ComplianceLog> complianceLog = null;
    public final static Parcelable.Creator<Data> CREATOR = new Creator<Data>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Data createFromParcel(Parcel in) {
            return new Data(in);
        }

        public Data[] newArray(int size) {
            return (new Data[size]);
        }

    };

    protected Data(Parcel in) {
        this.lastPage = ((Integer) in.readValue((Integer.class.getClassLoader())));
        in.readList(this.complianceLog, (ComplianceLog.class.getClassLoader()));
    }

    public Data() {
    }

    public Integer getLastPage() {
        return lastPage;
    }

    public void setLastPage(Integer lastPage) {
        this.lastPage = lastPage;
    }

    public List<ComplianceLog> getComplianceLog() {
        return complianceLog;
    }

    public void setComplianceLog(List<ComplianceLog> complianceLog) {
        this.complianceLog = complianceLog;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(lastPage);
        dest.writeList(complianceLog);
    }

    public int describeContents() {
        return 0;
    }

}