package com.crossingguard.models.createguard;

import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GuardListModel implements Parcelable {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<GuardModel> data = null;
    public final static Parcelable.Creator<GuardListModel> CREATOR = new Creator<GuardListModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public GuardListModel createFromParcel(Parcel in) {
            return new GuardListModel(in);
        }

        public GuardListModel[] newArray(int size) {
            return (new GuardListModel[size]);
        }

    };

    protected GuardListModel(Parcel in) {
        this.success = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.data, (GuardModel.class.getClassLoader()));
    }

    public GuardListModel() {
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<GuardModel> getData() {
        return data;
    }

    public void setData(List<GuardModel> data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(success);
        dest.writeValue(message);
        dest.writeList(data);
    }

    public int describeContents() {
        return 0;
    }

}