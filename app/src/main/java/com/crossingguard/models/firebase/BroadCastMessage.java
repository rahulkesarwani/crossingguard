package com.crossingguard.models.firebase;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BroadCastMessage implements Parcelable
{
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    public final static Parcelable.Creator<BroadCastMessage> CREATOR = new Creator<BroadCastMessage>() {


        @SuppressWarnings({
                "unchecked"
        })
        public BroadCastMessage createFromParcel(Parcel in) {
            return new BroadCastMessage(in);
        }

        public BroadCastMessage[] newArray(int size) {
            return (new BroadCastMessage[size]);
        }

    }
            ;

    protected BroadCastMessage(Parcel in) {
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.createdAt = ((String) in.readValue((String.class.getClassLoader())));
        this.timestamp = ((String) in.readValue((String.class.getClassLoader())));
    }

    public BroadCastMessage() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(message);
        dest.writeValue(createdAt);
        dest.writeValue(timestamp);
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public boolean equals(@androidx.annotation.Nullable Object obj) {
        BroadCastMessage message = (BroadCastMessage) obj;
        return timestamp.matches(message.getTimestamp());
    }
}