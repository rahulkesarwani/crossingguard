package com.crossingguard.models.guardsassignedlocations;

import android.os.Parcel;
import android.os.Parcelable;

import com.crossingguard.models.locations.LocationModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class GuardLocationResponseModel implements Parcelable {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<LocationModel> locations = new ArrayList<>();
    public final static Creator<GuardLocationResponseModel> CREATOR = new Creator<GuardLocationResponseModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public GuardLocationResponseModel createFromParcel(Parcel in) {
            return new GuardLocationResponseModel(in);
        }

        public GuardLocationResponseModel[] newArray(int size) {
            return (new GuardLocationResponseModel[size]);
        }

    };

    protected GuardLocationResponseModel(Parcel in) {
        this.success = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.locations, (LocationModel.class.getClassLoader()));
    }

    public GuardLocationResponseModel() {
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<LocationModel> getLocations() {
        return locations;
    }

    public void setLocations(List<LocationModel> locations) {
        this.locations = locations;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(success);
        dest.writeValue(message);
        dest.writeList(locations);
    }

    public int describeContents() {
        return 0;
    }


    @Override
    public String toString() {
        return "" + success + ":" + message + ":" + locations.toString();
    }
}