package com.crossingguard.models.locations;

import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LocationListModel implements Parcelable {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<LocationModel> data = null;
    public final static Parcelable.Creator<LocationListModel> CREATOR = new Creator<LocationListModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public LocationListModel createFromParcel(Parcel in) {
            return new LocationListModel(in);
        }

        public LocationListModel[] newArray(int size) {
            return (new LocationListModel[size]);
        }

    };

    protected LocationListModel(Parcel in) {
        this.success = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.data, (LocationModel.class.getClassLoader()));
    }

    public LocationListModel() {
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<LocationModel> getData() {
        return data;
    }

    public void setData(List<LocationModel> data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(success);
        dest.writeValue(message);
        dest.writeList(data);
    }

    public int describeContents() {
        return 0;
    }

}