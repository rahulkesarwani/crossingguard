package com.crossingguard.models.locations;

import java.util.ArrayList;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

import com.crossingguard.models.firebase.BroadCastMessage;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LocationModel implements Parcelable
{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("coverage_times")
    @Expose
    private List<String> coverageTimes = new ArrayList<>();
    @SerializedName("geofence")
    @Expose
    private GeofenceModel geofence;
    @SerializedName("is_guard_available")
    @Expose
    private Boolean isGuardAvailable;
    @SerializedName("pivot")
    @Expose
    private Pivot pivot;
    //custom list after process
    @SerializedName("assigned_times")
    @Expose
    private List<String> assignedTimes = new ArrayList<>();

    public final static Parcelable.Creator<LocationModel> CREATOR = new Creator<LocationModel>() {

        @SuppressWarnings({
                "unchecked"
        })
        public LocationModel createFromParcel(Parcel in) {
            return new LocationModel(in);
        }

        public LocationModel[] newArray(int size) {
            return (new LocationModel[size]);
        }

    }
            ;

    protected LocationModel(Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.coverageTimes, (java.lang.String.class.getClassLoader()));
        this.geofence = ((GeofenceModel) in.readValue((GeofenceModel.class.getClassLoader())));
        this.isGuardAvailable = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.pivot = ((Pivot) in.readValue((Pivot.class.getClassLoader())));
        in.readList(this.assignedTimes, (java.lang.String.class.getClassLoader()));
    }

    public LocationModel() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getCoverageTimes() {
        return coverageTimes;
    }

    public void setCoverageTimes(List<String> coverageTimes) {
        this.coverageTimes = coverageTimes;
    }

    public GeofenceModel getGeofence() {
        return geofence;
    }

    public void setGeofence(GeofenceModel geofence) {
        this.geofence = geofence;
    }

    public Boolean getGuardAvailable() {
        return isGuardAvailable;
    }

    public void setGuardAvailable(Boolean guardAvailable) {
        isGuardAvailable = guardAvailable;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Pivot getPivot() {
        return pivot;
    }

    public void setPivot(Pivot pivot) {
        this.pivot = pivot;
    }

    public List<String> getAssignedTimes() {
        return assignedTimes;
    }


    public String getAssignedFormattedTimes() {
        StringBuilder times=new StringBuilder();

        for(int i=0;i<assignedTimes.size();i++){
            times.append(assignedTimes.get(i).replaceAll("\\[","").replaceAll("]","").replaceAll(";"," - ").replaceAll("\"", "")+"\n");
        }

        return times.toString();
    }
    public void setAssignedTimes(List<String> assignedTimes) {
        this.assignedTimes = assignedTimes;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(name);
        dest.writeList(coverageTimes);
        dest.writeValue(geofence);
        dest.writeValue(isGuardAvailable);
        dest.writeValue(pivot);
        dest.writeList(assignedTimes);
    }

    public int describeContents() {
        return 0;
    }


    @Override
    public String toString() {
        return ""+id+":"+name+":"+coverageTimes.toString()+":"+geofence+":"+isGuardAvailable;
    }

    @Override
    public boolean equals(@androidx.annotation.Nullable Object obj) {
        LocationModel locationModel = (LocationModel) obj;
        return id.equals(((LocationModel) obj).getId());
    }
}
