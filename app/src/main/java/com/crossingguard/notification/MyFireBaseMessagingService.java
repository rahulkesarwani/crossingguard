package com.crossingguard.notification;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.crossingguard.ui.broadcastmessage.BroadcastMessageActivity;
import com.crossingguard.ui.guardhome.GuardHomeActivity;
import com.crossingguard.ui.login.LoginActivity;
import com.crossingguard.utils.AppConstants;
import com.crossingguard.utils.LogHelper;
import com.crossingguard.utils.MySharedPreferences;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.greenrobot.eventbus.EventBus;


/**
 * Created by rahul on 12/15/2017.
 */

public class MyFireBaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFireBaseMessagingService.class.getSimpleName();

    private NotificationUtils notificationUtils;

    @Override
    public void onNewToken(String token) {
        LogHelper.d(TAG, "Refreshed token: " + token);


        MySharedPreferences.getInstance().putBoolean(AppConstants.IS_TOKEN_REFRESHED, true);
        MySharedPreferences.getInstance().putString(AppConstants.FCM_TOKEN, token);
    }


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        LogHelper.d(TAG, "From: " + remoteMessage.getFrom());

        MySharedPreferences.getInstance().putInt(AppConstants.NOTIFICATION_COUNTER, (MySharedPreferences.getInstance().getInt(AppConstants.NOTIFICATION_COUNTER, 0) + 1));
        EventBus.getDefault().post(AppConstants.EVENT_CONSTANTS.EVENT_BROADCAST_NOTIFICATION);

        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            LogHelper.d(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
//            handleNotification(remoteMessage.getNotification().getBody());
//            handleDataMessage(remoteMessage.getNotification().getBody());
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            LogHelper.d(TAG, "Data Payload: " + remoteMessage.getData());

            try {
                // JSONObject json = new JSONObject(remoteMessage.getData().toString());

                String title = remoteMessage.getData().get("title");
                handleDataMessage(remoteMessage.getData().get("message"), remoteMessage.getData().get("image"), title);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        } else {
            Log.e(TAG, "onMessageReceived: else");
        }
    }


    private void handleDataMessage(String message, String imageUrl, String title) {

        Intent resultIntent;
        String accesToken = MySharedPreferences.getInstance().getString(AppConstants.ACCESS_TOKEN, "");
        // check for image attachment
        if (TextUtils.isEmpty(imageUrl)) {
            if (!TextUtils.isEmpty(accesToken)) {
                resultIntent = new Intent(getApplicationContext(), BroadcastMessageActivity.class);
            } else {
                resultIntent = new Intent(getApplicationContext(), LoginActivity.class);
            }
            showNotificationMessage(getApplicationContext(), title, message, String.valueOf(System.currentTimeMillis()), resultIntent);
        } else {
            if (!TextUtils.isEmpty(accesToken)) {
                resultIntent = new Intent(getApplicationContext(), BroadcastMessageActivity.class);
            } else {
                resultIntent = new Intent(getApplicationContext(), LoginActivity.class);
            }
            // image is present, show notification with image
            showNotificationMessageWithBigImage(getApplicationContext(), title, message, String.valueOf(System.currentTimeMillis()), resultIntent, imageUrl);
        }
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String
            timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String
            message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }
}
