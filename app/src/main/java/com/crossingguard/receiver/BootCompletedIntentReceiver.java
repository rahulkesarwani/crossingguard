package com.crossingguard.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.crossingguard.service.GeofenceLocationService;
import com.crossingguard.utils.AppConstants;
import com.crossingguard.utils.MySharedPreferences;
import com.crossingguard.utils.Utility;


/**
 * Created by shankar on 7/5/2017.
 */

public class BootCompletedIntentReceiver extends BroadcastReceiver {
    private MySharedPreferences mySharedPreferences;
    @Override
    public void onReceive(Context context, Intent intent) {
        mySharedPreferences=new MySharedPreferences(context);
        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {

            String accesToken = mySharedPreferences.getString(AppConstants.ACCESS_TOKEN, "");
            if (!TextUtils.isEmpty(accesToken)) {
                String userType= mySharedPreferences.getString(AppConstants.LOGIN_TYPE,"");
                if(!(userType.equalsIgnoreCase("supervisor"))){
                    if (!Utility.isMyServiceRunning(GeofenceLocationService.class, context)) {
                        Utility.createNotificationChannel(context);
                        Utility.StartCheckerService(context);
                    }
                }
            }
        }
    }
}