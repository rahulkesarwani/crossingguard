package com.crossingguard.service;

import android.Manifest;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;

import com.crossingguard.R;
import com.crossingguard.apis.ApiInterface;
import com.crossingguard.models.guardsassignedlocations.GuardLocationResponseModel;
import com.crossingguard.models.logins.Data;
import com.crossingguard.utils.ApiUtility;
import com.crossingguard.utils.AppConstants;
import com.crossingguard.utils.MySharedPreferences;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class GeofenceLocationService extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final String TAG = GeofenceLocationService.class.getSimpleName();
    protected GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private long interval;
    protected int accuracy;
    protected int minDisplacement;

//    private GeofencingRequest geofencingRequest;
//    private PendingIntent pendingIntent;
//    private MySharedPreferences mySharedPreferences;
//    private CompositeDisposable compositeDisposable;
    protected ApiInterface commonApiInterface;
//    private Handler refreshDataHandler = null;
//    private Runnable refreshDataRunnable = null;
//    private int userId;
//    private String userEmail;
private List<Address> address = new ArrayList<>();
    private Geocoder mGeocoder;
    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand: ");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Intent notificationIntent = new Intent(this, GeofenceLocationService.class);
            PendingIntent notificationPendingIntent = PendingIntent.getActivity(this,
                    0, notificationIntent, 0);

            Notification notification = new NotificationCompat.Builder(this, AppConstants.CHANNEL_ID)
                    .setContentTitle("Location Update")
                    .setContentText("Checking Location")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentIntent(notificationPendingIntent)
                    .build();

            startForeground(1, notification);

            //do heavy work on a background thread
            //stopSelf();
            super.onStartCommand(intent, flags, startId);
            return START_STICKY;
        } else {
            super.onStartCommand(intent, flags, startId);
        }
        return START_STICKY;


    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void setupLocationListener(Context context, int locationAccuracy, int interval, int minDisplacement) {
        this.interval = interval;
        this.accuracy = locationAccuracy;
        this.minDisplacement = minDisplacement;

        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();

        if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(context) == ConnectionResult.SUCCESS) {
            mGoogleApiClient.connect();

        }
    }


    @Override
    public void onConnected(Bundle bundle) {

//        startGeofencing();
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(accuracy);
        mLocationRequest.setInterval(interval);
        mLocationRequest.setFastestInterval(1000);
        if (minDisplacement > 0) {
            mLocationRequest.setSmallestDisplacement(minDisplacement);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "onLocationChanged: " + location.getLatitude() + "    " + location.getLongitude());

        mGeocoder = new Geocoder(GeofenceLocationService.this, Locale.getDefault());

        try {
            address = mGeocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            getAddress(location);

        } catch (Exception e) {

        }

    }


    private void getAddress(Location location) {
        if (address != null) {
            //String mUserAddLine = address.get(0).getAddressLine(0);//house no. // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String mUserAddLine = "";
            String sector = address.get(0).getSubLocality();
            String locality = address.get(0).getLocality();
            String country = address.get(0).getCountryName();

            mUserAddLine = mUserAddLine + sector + ", " + locality;
//            addDatatoSharedPreference(mCurrentLatitute, mCurrentLongitute, address.get(0).getAddressLine(0));
            Log.d(TAG, "getAddress: " + address.get(0).getAddressLine(0));
            MySharedPreferences.getInstance().putString(AppConstants.LONGITUDE, address.get(0).getAddressLine(0));
            mGoogleApiClient.disconnect();
            EventBus.getDefault().post(location);
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }


    @Override
    public void onCreate() {

//        mySharedPreferences = new MySharedPreferences(this);
//        compositeDisposable = new CompositeDisposable();
//        commonApiInterface = ApiUtility.getInstance().getRetrofit(this).create(ApiInterface.class);
//        Data loginData = (Data) mySharedPreferences.getObj(AppConstants.LOGIN_DATA, Data.class);
//        if (loginData != null) {
//            userId = loginData.getUser().getId();
//            userEmail = loginData.getUser().getEmail();
//        }

        setupLocationListener(this, LocationRequest.PRIORITY_HIGH_ACCURACY, 5000, 1);
//        startCheckingUpdatedDataForGuard();
    }


//    private void startGeofencing() {
//        if (getGeofencesList().size() > 0) {
//            Log.d(TAG, "Start geofencing monitoring call");
//            pendingIntent = getGeofencePendingIntent();
//            geofencingRequest = new GeofencingRequest.Builder()
//                    .setInitialTrigger(Geofence.GEOFENCE_TRANSITION_ENTER)
//                    .addGeofences(getGeofencesList())
//                    .build();
//
//            if (!mGoogleApiClient.isConnected()) {
//                Log.d(TAG, "Google API client not connected");
//            } else {
//                try {
//                    LocationServices.GeofencingApi.addGeofences(mGoogleApiClient, geofencingRequest, pendingIntent).setResultCallback(new ResultCallback<Status>() {
//                        @Override
//                        public void onResult(@NonNull Status status) {
//                            if (status.isSuccess()) {
//                                Log.d(TAG, "Successfully Geofencing Connected");
//                            } else {
//                                Log.d(TAG, "Failed to add Geofencing " + status.getStatus());
//                            }
//                        }
//                    });
//                } catch (SecurityException e) {
//                    Log.d(TAG, e.getMessage());
//                }
//            }
//        }
//    }

//    @NonNull
//    private List<Geofence> getGeofencesList() {
//
//        GuardLocationResponseModel responeModel = (GuardLocationResponseModel) mySharedPreferences.getObj(AppConstants.GUARD_LOCATION_DATA, GuardLocationResponseModel.class);
//
//        List<Geofence> geofences = new ArrayList<>();
//
//        for (int i = 0; i < responeModel.getLocations().size(); i++) {
//
////            Log.d(TAG, "getGeofencesList: " + responeModel.getLocations().get(i).getGeofence().getLatitude());
////            Log.d(TAG, "getGeofencesList: " + responeModel.getLocations().get(i).getGeofence().getLongitude());
////            Log.d(TAG, "getGeofencesList: " + responeModel.getLocations().get(i).getGeofence().getRadius());
//
//            geofences.add(new Geofence.Builder()
//                    .setRequestId(AppConstants.GEOFENCE_REQUEST_ID + System.currentTimeMillis() + ":" + responeModel.getLocations().get(i).getId() + ":" + userId + ":" + userEmail)
//                    .setExpirationDuration(Geofence.NEVER_EXPIRE)
//                    .setCircularRegion(responeModel.getLocations().get(i).getGeofence().getLatitude(), responeModel.getLocations().get(i).getGeofence().getLongitude(), responeModel.getLocations().get(i).getGeofence().getRadius())
//                    .setNotificationResponsiveness(1000)
//                    .setLoiteringDelay(5000)
//                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT)
//                    .build());
//        }
//        return geofences;
//    }

//    private PendingIntent getGeofencePendingIntent() {
//        if (pendingIntent != null) {
//            return pendingIntent;
//        }
//        Intent intent = new Intent(this, GeofenceRegistrationService.class);
//        return PendingIntent.getService(this, 0, intent, PendingIntent.
//                FLAG_UPDATE_CURRENT);
//    }

//    public void stopGeoFencing() {
//        pendingIntent = getGeofencePendingIntent();
//        LocationServices.GeofencingApi.removeGeofences(mGoogleApiClient, pendingIntent)
//                .setResultCallback(new ResultCallback<Status>() {
//                    @Override
//                    public void onResult(@NonNull Status status) {
//                        if (status.isSuccess())
//                            Log.d(TAG, "Stop geofencing");
//                        else
//                            Log.d(TAG, "Not stop geofencing");
//                    }
//                });
//    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy: ");
//        stopGeoFencing();
//        stopMembersLocationUpdateOnMap();
        super.onDestroy();
    }


//    private void hitApiToPullLatestLocation() {
//        compositeDisposable.add(
//                commonApiInterface.hitGuardAssignedLocationApi()
//                        .subscribeOn(Schedulers.io())
//                        .observeOn(AndroidSchedulers.mainThread())
//                        .subscribe(commonResponse -> {
//                            onSuccess(commonResponse);
//                        }, throwable -> {
//                        })
//        );
//    }


//    public void onSuccess(Response<GuardLocationResponseModel> response) {
//        if (response.code() == 200) {
//            if (response.body().getLocations().size() > 0) {
//
//                GuardLocationResponseModel localData = (GuardLocationResponseModel) mySharedPreferences.getObj(AppConstants.GUARD_LOCATION_DATA, GuardLocationResponseModel.class);
//
//                //if server pulled data is changed from the last stored data then update data and remove and set geofence again.
//
//                boolean check = isDataChanged(response.body(), localData);
//                Log.d(TAG, "onSuccess: " + check);
//                if (check) {
//
//                    mySharedPreferences.setObj(AppConstants.GUARD_LOCATION_DATA, response.body());
//                    //if data changed remove geofencing
//                    stopGeoFencing();
//
//                    //start geofencing again.
//                    startGeofencing();
//                    EventBus.getDefault().post(response.body());
//                }
//
//            } else {
////                MessagesUtils.showToastError(this,"No Location assigned");
//                stopGeoFencing();
//                mySharedPreferences.setObj(AppConstants.GUARD_LOCATION_DATA, null);
//                EventBus.getDefault().post(response.body());
//            }
//        }
//    }

//    private boolean isDataChanged(GuardLocationResponseModel serverResponse, GuardLocationResponseModel localData) {
//
//        if (localData == null)
//            return true;
//
//        Log.d(TAG, "isDataChanged: " + serverResponse.toString() + " \n localdata: " + localData.toString());
//
//        if (!serverResponse.toString().equalsIgnoreCase(localData.toString())) {
//            return true;
//        } else {
//            return false;
//        }
//    }


//    private void startCheckingUpdatedDataForGuard() {
//        refreshDataHandler = new Handler();
//        refreshDataRunnable = new Runnable() {
//
//            @Override
//            public void run() {
//                hitApiToPullLatestLocation();
////                7 seconds    //7000
//                refreshDataHandler.postDelayed(this, 7000);
//            }
//        };
//        refreshDataHandler.postDelayed(refreshDataRunnable, 0);
//    }
//
//    private void stopMembersLocationUpdateOnMap() {
//        refreshDataHandler.removeCallbacks(refreshDataRunnable);
//    }
}
