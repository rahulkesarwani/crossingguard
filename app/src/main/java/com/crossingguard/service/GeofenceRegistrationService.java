//package com.crossingguard.service;
//
//import android.app.IntentService;
//import android.app.NotificationChannel;
//import android.app.NotificationManager;
//import android.app.PendingIntent;
//import android.content.Context;
//import android.content.Intent;
//import android.graphics.BitmapFactory;
//import android.graphics.Color;
//import android.os.Build;
//import android.util.Log;
//
//import androidx.annotation.NonNull;
//import androidx.annotation.Nullable;
//import androidx.core.app.NotificationCompat;
//import androidx.core.app.TaskStackBuilder;
//
//import com.crossingguard.R;
//import com.crossingguard.db.AppDatabase;
//import com.crossingguard.db.entity.CheckInOutDBModel;
//import com.crossingguard.ui.guardhome.GuardHomeActivity;
//import com.crossingguard.utils.AppConstants;
//import com.crossingguard.utils.Helper;
//import com.google.android.gms.location.Geofence;
//import com.google.android.gms.location.GeofencingEvent;
//
//import java.util.List;
//import java.util.concurrent.Callable;
//
//import io.reactivex.Observable;
//import io.reactivex.android.schedulers.AndroidSchedulers;
//import io.reactivex.functions.Consumer;
//import io.reactivex.schedulers.Schedulers;
//
//public class GeofenceRegistrationService extends IntentService {
//
//    private static final String TAG = "GeofenceLocationService";
//
//
//    public GeofenceRegistrationService() {
//        super(TAG);
//    }
//
//    @Override
//    public void onCreate() {
//        super.onCreate();
//
//    }
//
//    @Override
//    protected void onHandleIntent(@Nullable Intent intent) {
//
//        Log.d(TAG, "onHandleIntent: ");
//        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
//        if (geofencingEvent.hasError()) {
//            Log.d(TAG, "GeofencingEvent error " + geofencingEvent.getErrorCode());
//        } else {
//            int transaction = geofencingEvent.getGeofenceTransition();
//            List<Geofence> geofences = geofencingEvent.getTriggeringGeofences();
//            for (int i = 0; i < geofences.size(); i++) {
//
//                Geofence geofence = geofences.get(i);
//                if (transaction == Geofence.GEOFENCE_TRANSITION_ENTER) {
//                    sendNotification("Entered");
//                    Log.d(TAG, "You are inside " + geofence.getRequestId());
//
//                    String requestId[] = geofence.getRequestId().split(":");
//
//                    CheckInOutDBModel checkInOutDBModel = new CheckInOutDBModel();
//                    checkInOutDBModel.setEventType("check-in");
//                    checkInOutDBModel.setLocationId(Integer.parseInt(requestId[1]));
//                    checkInOutDBModel.setUserId(Integer.parseInt(requestId[2]));
//                    checkInOutDBModel.setSecret(Helper.getBase64Secret(requestId[2]+":"+requestId[3]));
//                    checkInOutDBModel.setCreatedAt(System.currentTimeMillis());
//
//                    addLogInQueue(checkInOutDBModel);
//                } else if (transaction == Geofence.GEOFENCE_TRANSITION_EXIT) {
//                    sendNotification("Exited");
//                    Log.d(TAG, "You are outside " + geofence.getRequestId());
//                    String requestId[] = geofence.getRequestId().split(":");
//
//                    CheckInOutDBModel checkInOutDBModel = new CheckInOutDBModel();
//                    checkInOutDBModel.setEventType("check-out");
//                    checkInOutDBModel.setLocationId(Integer.parseInt(requestId[1]));
//                    checkInOutDBModel.setUserId(Integer.parseInt(requestId[2]));
//                    checkInOutDBModel.setSecret(Helper.getBase64Secret(requestId[2]+":"+requestId[3]));
//                    checkInOutDBModel.setCreatedAt(System.currentTimeMillis());
//
//                    addLogInQueue(checkInOutDBModel);
//
//                } else {
//                    Log.d(TAG, "onHandleIntent: Unknown Transition");
//                }
//
//            }
//        }
//    }
//
//
//    /**
//     * Posts a notification in the notification bar when a transition is detected.
//     * If the user clicks the notification, control goes to the MainActivity.
//     */
//    private void sendNotification(String notificationDetails) {
//        // Get an instance of the Notification manager
//        NotificationManager mNotificationManager =
//                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//        // Android O requires a Notification Channel.
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            CharSequence name = getString(R.string.app_name);
//            // Create the channel for the notification
//            NotificationChannel mChannel =
//                    new NotificationChannel(AppConstants.NOTIFIC_CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);
//
//            // Set the Notification Channel for the Notification Manager.
//            mNotificationManager.createNotificationChannel(mChannel);
//        }
//
//        // Create an explicit content Intent that starts the main Activity.
//        Intent notificationIntent = new Intent(getApplicationContext(), GuardHomeActivity.class);
//
//        // Construct a task stack.
//        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
//
//        // Add the main Activity to the task stack as the parent.
//        stackBuilder.addParentStack(GuardHomeActivity.class);
//
//        // Push the content Intent onto the stack.
//        stackBuilder.addNextIntent(notificationIntent);
//
//        // Get a PendingIntent containing the entire back stack.
//        PendingIntent notificationPendingIntent =
//                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
//
//        // Get a notification builder that's compatible with platform versions >= 4
//        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
//
//        // Define the notification settings.
//        builder.setSmallIcon(R.drawable.ic_launcher)
//                // In a real app, you may want to use a library like Volley
//                // to decode the Bitmap.
//                .setLargeIcon(BitmapFactory.decodeResource(getResources(),
//                        R.drawable.ic_launcher))
//                .setColor(Color.RED)
//                .setContentTitle(notificationDetails)
//                .setContentText(getString(R.string.geofence_transition_notification_text))
//                .setContentIntent(notificationPendingIntent);
//
//        // Set the Channel ID for Android O.
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            builder.setChannelId(AppConstants.NOTIFIC_CHANNEL_ID); // Channel ID
//        }
//
//        // Dismiss notification once the user touches it.
//        builder.setAutoCancel(true);
//
//        // Issue the notification
//        mNotificationManager.notify(0, builder.build());
//    }
//
//
//    public void addLogInQueue(CheckInOutDBModel checkInOutDBModel) {
//
//        Observable.fromCallable(new Callable<Object>() {
//            @Override
//            public Object call() throws Exception {
//                return AppDatabase.getAppDatabase(getApplicationContext()).checkInOutEventDao().add(checkInOutDBModel);
//            }
//        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Object>() {
//            @Override
//            public void accept(@NonNull Object object) throws Exception {
//                Log.d(TAG, "accept: New IN Out recording >>>" + checkInOutDBModel.getEventType());
//            }
//        });
//    }
//}
