package com.crossingguard.service;

import com.crossingguard.db.entity.CheckInOutDBModel;

public interface LocationMvpView {


    void onNextTaskReceived(CheckInOutDBModel videoEntityModel);
}
