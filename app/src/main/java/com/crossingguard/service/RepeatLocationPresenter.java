package com.crossingguard.service;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.crossingguard.apis.ApiInterface;
import com.crossingguard.db.AppDatabase;
import com.crossingguard.db.entity.CheckInOutDBModel;
import com.crossingguard.models.checkins.CheckInOutRequestModel;
import com.crossingguard.ui.login.LoginActivity;
import com.crossingguard.utils.ApiUtility;
import com.crossingguard.utils.AppConstants;
import com.crossingguard.utils.CheckNullable;
import com.crossingguard.utils.ConnectionDetector;
import com.crossingguard.utils.Helper;
import com.crossingguard.utils.MySharedPreferences;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;

import static com.bumptech.glide.gifdecoder.GifHeaderParser.TAG;

public class RepeatLocationPresenter {

    private Context context;

    private ConnectionDetector connectionDetector;
    private MySharedPreferences mySharedPreferences;
    protected CompositeDisposable mCompositeDisposable;
    protected ApiInterface commonApiInterface;
    private LocationMvpView mvpView;

    RepeatLocationPresenter(Context context, LocationMvpView mvpView) {
        this.context = context;
        connectionDetector = new ConnectionDetector(context);
        mySharedPreferences = new MySharedPreferences(context);
        this.mvpView = mvpView;
        mCompositeDisposable = new CompositeDisposable();
        commonApiInterface = ApiUtility.getInstance().getRetrofit(context).create(ApiInterface.class);
    }

    public void hitCheckInOutApi(CheckInOutDBModel checkInOutDBModel) {
        if (connectionDetector.isInternetConnected()) {

            String url;

            if (checkInOutDBModel.getEventType().equalsIgnoreCase("check-in")) {
                url = AppConstants.mBaseUrl + "check-in";
            } else {
                url = AppConstants.mBaseUrl + "check-out";
            }


            CheckInOutRequestModel requestModel = new CheckInOutRequestModel();
            requestModel.setLocationId(checkInOutDBModel.getLocationId());
            requestModel.setUserId(checkInOutDBModel.getUserId());
            requestModel.setSecret(checkInOutDBModel.getSecret());
            requestModel.setTriggerTime(String.valueOf(checkInOutDBModel.getCreatedAt()));
            requestModel.setGeoAddress(checkInOutDBModel.getGeoAddress());
            requestModel.setGeoLat(checkInOutDBModel.getGeoLat());
            requestModel.setGeoLng(checkInOutDBModel.getGeoLng());

            mCompositeDisposable.add(
                    commonApiInterface.hitCheckInOutApi(url, requestModel)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(commonResponse -> {
                                if (commonResponse.code() == 401) {

                                    mySharedPreferences.setObj(AppConstants.LOGIN_DATA, null);
                                    mySharedPreferences.putString(AppConstants.LOGIN_TYPE, "");
                                    mySharedPreferences.putString(AppConstants.ACCESS_TOKEN, "");
                                    mySharedPreferences.putBoolean(AppConstants.GEOFENCES_ADDED_KEY, false);

                                    RepeatLocationPresenter.deleteAllDataFromQueue(context);

                                    context.startActivity(new Intent(context, LoginActivity.class));

                                } else if (commonResponse.code() == 200 || commonResponse.code() == 201) {
                                    if (commonResponse.body().getSuccess()) {

                                        deleteTaskFromDB(checkInOutDBModel.getId(), context);

                                    } else {
                                        // if user entered in location before check in time so the log will not be maintained on server. for that again resend request from locationservice
                                        if (checkInOutDBModel.getEventType().equalsIgnoreCase("check-out")) {
                                            deleteTaskFromDB(checkInOutDBModel.getId(), context);
                                        }
                                    }
                                } else if (commonResponse.code() == 400) {

                                    // delete check-out if any issue occured
                                    if (checkInOutDBModel.getEventType().equalsIgnoreCase("check-out")) {
                                        deleteTaskFromDB(checkInOutDBModel.getId(), context);
                                    } else {
                                        // if check-in update created at field with current time stamp


                                    }
                                }
                            }, throwable -> {

                            })
            );

        }
    }


    public void getPendingTaskFromQueue() {
        mCompositeDisposable.add(
                AppDatabase.getAppDatabase(context).checkInOutEventDao().getPendingTaskFromQueue()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(item -> {
                            Log.d(TAG, "getPendingTaskFromQueue: " + item);
                            if (item != null) {
                                updateProcessTime(item, context);
                                mvpView.onNextTaskReceived(item);
                            }
                        }, throwable -> {
                            Log.d(TAG, "getPendingTaskFromQueue: throwable");
                        })
        );
    }


    public void deleteTaskFromDB(long id, Context context) {
        Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {

                AppDatabase.getAppDatabase(context).checkInOutEventDao().delete(id);
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe();

    }

    public static void deleteAllDataFromQueue(Context context) {
//        Completable.fromAction(new Action() {
//            @Override
//            public void run() throws Exception {
//
//                AppDatabase.getAppDatabase(context).checkInOutEventDao().nukeTable();
//            }
//        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe();

    }


    public void updateProcessTime(CheckInOutDBModel checkInOutDBModel, Context mContext) {

        Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {

                AppDatabase.getAppDatabase(mContext).checkInOutEventDao().updateProcessingTime(checkInOutDBModel.getId(), System.currentTimeMillis());

            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new CompletableObserver() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onComplete() {

            }

            @Override
            public void onError(Throwable e) {

            }
        });
    }
}
