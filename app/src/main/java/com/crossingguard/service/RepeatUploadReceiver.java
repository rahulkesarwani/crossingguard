package com.crossingguard.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.crossingguard.db.entity.CheckInOutDBModel;

public class RepeatUploadReceiver extends BroadcastReceiver implements LocationMvpView {

    private String TAG = RepeatUploadReceiver.class.getCanonicalName();
    private Context context;
    private String WAKE_TAG = RepeatUploadReceiver.class.getCanonicalName();
    private RepeatLocationPresenter mPresenter;

    //the method will be fired when the alarm is triggerred
    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        //you can check the log that it is fired
        //Here we are actually not doing anything
        //but you can do any task here that you want to be done at a specific time everyday
         Log.d(TAG, "Alarm just fired");
        wakeUp();
        mPresenter = new RepeatLocationPresenter(context, this);
        mPresenter.getPendingTaskFromQueue();
    }


    private void wakeUp() {

        ServiceSingleton.setAlarm(false);
    }


    @Override
    public void onNextTaskReceived(CheckInOutDBModel checkInLogModel) {
        mPresenter.hitCheckInOutApi(checkInLogModel);
    }

}