package com.crossingguard.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.crossingguard.MyApp;
public class ServiceSingleton {

    private static int REQUEST_CODE = 902;
    static ServiceSingleton mInstance = null;
    static PendingIntent pendingIntent;

    static Context context = MyApp.getInstance();
//    public static ServiceSingleton getInstance() {
//        if (mInstance == null)
//            mInstance = new ServiceSingleton();
//        return mInstance;
//    }


    public static void setAlarm(boolean force) {

        cancelAlarm();
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        //setting the repeating alarm that will be fired every day
        pendingIntent = getPendingIntent();
        //am.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 3000, pendingIntent);

        long alarmTime = System.currentTimeMillis();
        if (!force) {
            alarmTime += 5000l;
        }

        try {
            int SDK_INT = Build.VERSION.SDK_INT;

            if (SDK_INT < Build.VERSION_CODES.KITKAT) {
                am.set(AlarmManager.RTC_WAKEUP, alarmTime, pendingIntent);
            } else if (SDK_INT < Build.VERSION_CODES.M) {
                am.setExact(AlarmManager.RTC_WAKEUP, alarmTime, pendingIntent);
            } else {
                am.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, alarmTime, pendingIntent);
            }
        } catch (Exception e) {
        }
    }

    public static void cancelAlarm() {
        if (pendingIntent == null)
            return;
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        am.cancel(pendingIntent);

    }

    public static PendingIntent getPendingIntent() {
        Intent intent = new Intent(context, RepeatUploadReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        return pendingIntent;
    }

}
