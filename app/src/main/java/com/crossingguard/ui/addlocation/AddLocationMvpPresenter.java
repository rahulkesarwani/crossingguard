package com.crossingguard.ui.addlocation;

import com.crossingguard.baseactivities.base.MvpPresenter;
import com.crossingguard.models.locations.LocationModel;

/**
 * Created by rahul on 6/14/2018.
 */

public interface AddLocationMvpPresenter<V extends AddLocationMvpView>  extends MvpPresenter<V> {
    void hitAddNewLocationApi(LocationModel locationModel);

    void hitEditLocationApi(LocationModel locationModel);
}