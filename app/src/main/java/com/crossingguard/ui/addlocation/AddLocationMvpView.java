package com.crossingguard.ui.addlocation;

import com.crossingguard.baseactivities.base.MvpView;
import com.crossingguard.models.logins.LoginResponseModel;

import retrofit2.Response;


public interface AddLocationMvpView extends MvpView {

    void onSuccess(Response<LoginResponseModel> response);

    void onSuccessEdit(Response<LoginResponseModel> response);

    void onFailure(Throwable error);

}
