package com.crossingguard.ui.addlocation;

import com.crossingguard.baseactivities.base.BasePresenter;
import com.crossingguard.controller.IDataManager;
import com.crossingguard.models.locations.LocationModel;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 *
 */
public class AddLocationPresenter<V extends AddLocationMvpView> extends BasePresenter<V> implements AddLocationMvpPresenter<V> {


    public AddLocationPresenter(IDataManager IDataManager) {
        super(IDataManager);
    }


    @Override
    public void hitAddNewLocationApi(LocationModel locationModel) {
        getIDataManager().getCompositeDisposable().add(
                getIDataManager().getApiInterface().hitLocationAddApi(locationModel)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(commonResponse -> {

                            getMvpView().onSuccess(commonResponse);
                        }, throwable -> {

                            getMvpView().onFailure(throwable);
                        })
        );
    }

    @Override
    public void hitEditLocationApi(LocationModel locationModel) {
        getIDataManager().getCompositeDisposable().add(
                getIDataManager().getApiInterface().hitLocationEditApi(locationModel.getId(), locationModel)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(commonResponse -> {

                            getMvpView().onSuccessEdit(commonResponse);
                        }, throwable -> {

                            getMvpView().onFailure(throwable);
                        })
        );
    }


}
