package com.crossingguard.ui.broadcastmessage;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.crossingguard.R;
import com.crossingguard.adapters.broadcast.BroadCastMessageAdapter;
import com.crossingguard.baseactivities.BaseActivity;
import com.crossingguard.models.createguard.GuardModel;
import com.crossingguard.models.firebase.BroadCastMessage;
import com.crossingguard.models.logins.LoginResponseModel;
import com.crossingguard.ui.login.LoginActivity;
import com.crossingguard.ui.supervisorhome.SuperVisorHomeActivity;
import com.crossingguard.utils.ApiUtility;
import com.crossingguard.utils.AppConstants;
import com.crossingguard.utils.CheckNullable;
import com.crossingguard.utils.EndlessRecyclerViewScrollListener;
import com.crossingguard.utils.Helper;
import com.crossingguard.utils.LogHelper;
import com.crossingguard.utils.MessagesUtils;
import com.crossingguard.utils.MySharedPreferences;
import com.crossingguard.utils.customviews.CustomButton;
import com.crossingguard.utils.customviews.CustomEditText;
import com.crossingguard.utils.customviews.CustomTextView;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;


public class BroadcastMessageActivity extends BaseActivity implements BroadcastMessageMvpView {

    private String TAG = BroadcastMessageActivity.class.getSimpleName();

    @BindView(R.id.backIV)
    ImageView backIV;
    @BindView(R.id.messageRV)
    RecyclerView messageRV;
    @BindView(R.id.sendBtn)
    Button sendBtn;
    @BindView(R.id.messageET)
    AppCompatEditText messageET;
    @BindView(R.id.sendRL)
    RelativeLayout sendRL;
    @BindView(R.id.titleTV)
    CustomTextView titleTV;
    @BindView(R.id.noDataFound)
    CustomTextView noDataFound;


    @Inject
    BroadcastMessageMvpPresenter<BroadcastMessageMvpView> mPresenter;


    private ArrayList<BroadCastMessage> mBroadCastMessageList = new ArrayList<>();
    private BroadCastMessageAdapter broadCastMessageAdapter;
    private FirebaseDatabase rootNode;
    private EndlessRecyclerViewScrollListener scrollListener;
    private boolean isLoading = false;
    private boolean endPagination = false;

    private String mLastKey = "";
    private String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivityComponent().inject(BroadcastMessageActivity.this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(BroadcastMessageActivity.this);

        rootNode = FirebaseDatabase.getInstance();
        mySharedPreferences.putInt(AppConstants.NOTIFICATION_COUNTER, 0);
        if (MySharedPreferences.getInstance().getString(AppConstants.LOGIN_TYPE).equalsIgnoreCase("supervisor")) {

            sendRL.setVisibility(View.VISIBLE);
            userId = "" + MySharedPreferences.getInstance().getInt(AppConstants.USER_ID);
        } else {

            sendRL.setVisibility(View.GONE);
            userId = "" + MySharedPreferences.getInstance().getInt(AppConstants.SUPERVISOR_ID);

        }

        initUI();

    }


    @Override
    protected int getActivityLayout() {
        return R.layout.activity_broadcast_message;
    }

    @Override
    protected void initUI() {
//        initErrorViews(findViewById(R.id.error_views_layout));

        titleTV.setText("Broascast Message");

        broadCastMessageAdapter = new BroadCastMessageAdapter(this, mBroadCastMessageList);
        LinearLayoutManager manager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        messageRV.setLayoutManager(manager);

        scrollListener = new EndlessRecyclerViewScrollListener(manager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
//                if (currentPage < totalPage) {

                //if pagination end return
//                if (endPagination)
//                    return;
//
//                if (!isLoading) {
//                    isLoading = true;
//                    messageRV.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            mBroadCastMessageList.add(null);
//                            broadCastMessageAdapter.notifyItemInserted(broadCastMessageAdapter.getItemCount() - 1);
//                            getMessagesList();
//                        }
//                    });
//                }
//                }
            }
        };
        messageRV.addOnScrollListener(scrollListener);
        messageRV.setAdapter(broadCastMessageAdapter);

        messageRV.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (bottom < oldBottom) {
                    scrollToTop();
                }
            }
        });

        getMessagesList();

    }

    private void scrollToTop() {
        try {
            if (mBroadCastMessageList.size() == 0)
                return;

            messageRV.postDelayed(new Runnable() {
                @Override
                public void run() {
                    messageRV.smoothScrollToPosition(0);
                }
            }, 100);
        } catch (Exception e) {
        }

    }


    @Override
    public void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();

    }

    @Override
    protected void handleNoInternet() {

    }

    @Override
    protected void handleSlowInternet() {


    }

    @Override
    protected void handleServerError() {

    }

    @Override
    protected void handleNoDataFound() {


    }

    protected void handleNoLocationFound() {

    }


    @OnClick({R.id.backIV, R.id.sendBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIV:
                finish();
                break;
            case R.id.sendBtn:

                if (mConnectionDetector.isInternetConnected()) {
                    if (TextUtils.isEmpty(messageET.getText().toString().trim())) {
                        MessagesUtils.showToastError(this, "Please Write something..");
                    } else {
                        mPresenter.broadCastMessage(messageET.getText().toString().trim());
                    }
                } else {
                    MessagesUtils.showToastInfo(this, "No internet Available!");
                }

                break;
        }
    }

    @Override
    public void onSuccessSendBroadcast(BroadCastMessage message) {
        messageET.setText("");
        mPresenter.hitNotificationToAllGuards(message);
        messageRV.smoothScrollToPosition(mBroadCastMessageList.size());
        MessagesUtils.showToastSuccess(this, "Message BroadCasted Successfully");

        getSingleMessage();
    }

    @Override
    public void onMessageListReceived(ArrayList<BroadCastMessage> messagesList) {

        if (isLoading) {
            mBroadCastMessageList.remove(mBroadCastMessageList.size() - 1);
            broadCastMessageAdapter.notifyItemRemoved(mBroadCastMessageList.size());
        }

        mBroadCastMessageList.addAll(messagesList);
        broadCastMessageAdapter.notifyDataSetChanged();
//        scrollListener.setLoading();

        scrollToTop();

        noDataFound.setVisibility(View.GONE);

        if (isLoading) {
            isLoading = false;
        }

        if (!MySharedPreferences.getInstance().getString(AppConstants.LOGIN_TYPE).equalsIgnoreCase("supervisor")) {
            getSingleMessage();
        }
    }

    @Override
    public void onSingleMessageReceived(BroadCastMessage message) {

//        if (isLoading) {
//            mBroadCastMessageList.remove(mBroadCastMessageList.size() - 1);
//            broadCastMessageAdapter.notifyItemRemoved(mBroadCastMessageList.size());
//        }
//        mBroadCastMessageList.add(0, message);

        try {
            mBroadCastMessageList.add(0, message);
            broadCastMessageAdapter.notifyDataSetChanged();
            if (messageRV != null)
                messageRV.smoothScrollToPosition(0);
        } catch (Exception e) {
        }
//        scrollListener.setLoading();

//        noDataFound.setVisibility(View.GONE);

//        if (isLoading) {
//            isLoading = false;
//        }
    }

    @Override
    public void onMessageChanged(BroadCastMessage message) {
        for (int i = 0; i < mBroadCastMessageList.size(); i++) {
            if (mBroadCastMessageList.get(i).getTimestamp().equals(message.getTimestamp())) {
                // update object
                mBroadCastMessageList.set(i, message);
                break;
            }
        }
        broadCastMessageAdapter.notifyDataSetChanged();
    }

    @Override
    public void onMessageRemoved(BroadCastMessage message) {
        for (int i = 0; i < mBroadCastMessageList.size(); i++) {
            if (mBroadCastMessageList.get(i).getTimestamp().equals(message.getTimestamp())) {
                // update object
                mBroadCastMessageList.remove(i);
                broadCastMessageAdapter.notifyItemRemoved(i);
                break;
            }
        }

    }


    @Override
    public void onFailure(Throwable error) {
        handleFailure((Exception) error);
    }


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }


    public static void start(Context context) {
        context.startActivity(new Intent(context, BroadcastMessageActivity.class));
    }


    public void getMessagesList() {

        if (mBroadCastMessageList.size() > 0) {
//            mLastKey = mBroadCastMessageList.get(mBroadCastMessageList.size() - 1) != null ? mBroadCastMessageList.get(mBroadCastMessageList.size() - 1).getTimestamp() : mBroadCastMessageList.get(mBroadCastMessageList.size() - 2).getTimestamp();
            mLastKey = mBroadCastMessageList.get(0).getTimestamp();

        }

        LogHelper.d(TAG, "userIdd>>>>  " + userId + "   lastkey>>>>>" + mLastKey);

        Query messageQuery = null;
        if (mBroadCastMessageList.size() == 0) {

            messageQuery = rootNode.getReference(AppConstants.FIREBASE_REFERENCE.BROAD_CAST_MESSAGE).child(userId).orderByChild("timestamp");
//                    .limitToFirst(21);
            messageQuery.keepSynced(true);

        } else {

            LogHelper.d(TAG, "Query End >>>" + mLastKey);
            messageQuery = rootNode.getReference(AppConstants.FIREBASE_REFERENCE.BROAD_CAST_MESSAGE).child(userId).orderByChild("timestamp")
                    .startAt(mLastKey);
            messageQuery.keepSynced(true);
        }

        messageQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                LogHelper.d(TAG, "onDataChange ");

                ArrayList<BroadCastMessage> arrayList = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    BroadCastMessage item = snapshot.getValue(BroadCastMessage.class);
                    arrayList.add(0, item);
                }
                LogHelper.d(TAG, "onDataChange " + arrayList.size());

                //stop pagination here
                //if last timestamp matches with the recent recived data from server then its end of the end


                //for finding last key for next pagination
                if (arrayList.size() > 0)
                    mLastKey = arrayList.get(0).getTimestamp();

                //remove if size is 21
//                 if (arrayList.size() == 21)
//                    arrayList.remove(arrayList.size() - 1);
//
//
//                if (mBroadCastMessageList.size() > 0) {
//                    if (mBroadCastMessageList.get(mBroadCastMessageList.size() - 1) == null) {
//                        if (mBroadCastMessageList.get(mBroadCastMessageList.size() - 2).getTimestamp().equals(arrayList.get(arrayList.size() - 1).getTimestamp())) {
//                            endPagination = true;
//                        }
//                    } else {
//                        if (mBroadCastMessageList.get(mBroadCastMessageList.size() - 1).getTimestamp().equals(arrayList.get(arrayList.size() - 1).getTimestamp())) {
//                            endPagination = true;
//                        }
//                    }
//                }
                onMessageListReceived(arrayList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }


    private void getSingleMessage() {
        Query messageQuery = null;
        if (MySharedPreferences.getInstance().getString(AppConstants.LOGIN_TYPE).equalsIgnoreCase("supervisor")) {
            messageQuery = rootNode.getReference(AppConstants.FIREBASE_REFERENCE.BROAD_CAST_MESSAGE).child(userId).orderByChild("timestamp")
                    .startAt(mLastKey)
                    .limitToLast(1);

        } else {
            messageQuery = rootNode.getReference(AppConstants.FIREBASE_REFERENCE.BROAD_CAST_MESSAGE).child(userId).orderByChild("timestamp");
        }

        messageQuery.keepSynced(true);

        ChildEventListener childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                LogHelper.d(TAG, "onChildAdded: ");
                BroadCastMessage message = dataSnapshot.getValue(BroadCastMessage.class);
                LogHelper.d(TAG, "" + message.getMessage());

                mLastKey = message.getTimestamp();
                if (!mBroadCastMessageList.contains(message))
                    onSingleMessageReceived(message);


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                LogHelper.d(TAG, "onChildChanged: ");
                if (dataSnapshot != null && dataSnapshot.getValue() != null) {

                    BroadCastMessage message = dataSnapshot.getValue(BroadCastMessage.class);
                    onMessageChanged(message);

                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

                BroadCastMessage message = dataSnapshot.getValue(BroadCastMessage.class);
                LogHelper.d(TAG, "onChildRemoved: " + message.getMessage());
//                onMessageRemoved(message);

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                LogHelper.d(TAG, "onChildMoved: ");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                LogHelper.d(TAG, "onCancelled: ");
            }
        };

        messageQuery.addChildEventListener(childEventListener);
    }
}
