package com.crossingguard.ui.broadcastmessage;

import com.crossingguard.baseactivities.base.MvpPresenter;
import com.crossingguard.models.createguard.GuardModel;
import com.crossingguard.models.firebase.BroadCastMessage;

/**
 * Created by rahul on 6/14/2018.
 */

public interface BroadcastMessageMvpPresenter<V extends BroadcastMessageMvpView>  extends MvpPresenter<V> {
    void broadCastMessage(String message);

    void hitNotificationToAllGuards(BroadCastMessage message);
}