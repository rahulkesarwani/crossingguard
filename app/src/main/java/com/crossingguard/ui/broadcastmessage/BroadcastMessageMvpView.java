package com.crossingguard.ui.broadcastmessage;

import com.crossingguard.baseactivities.base.MvpView;
import com.crossingguard.models.firebase.BroadCastMessage;
import com.crossingguard.models.logins.LoginResponseModel;

import java.util.ArrayList;

import retrofit2.Response;


public interface BroadcastMessageMvpView extends MvpView {

    void onSuccessSendBroadcast(BroadCastMessage message);

    void onMessageListReceived(ArrayList<BroadCastMessage> messagesList);

    void onSingleMessageReceived(BroadCastMessage message);

    void onMessageChanged(BroadCastMessage message);

    void onMessageRemoved(BroadCastMessage message);

    void onFailure(Throwable error);

}
