package com.crossingguard.ui.broadcastmessage;

import android.util.Log;

import androidx.annotation.NonNull;

import com.crossingguard.baseactivities.base.BasePresenter;
import com.crossingguard.controller.IDataManager;
import com.crossingguard.models.createguard.GuardModel;
import com.crossingguard.models.firebase.BroadCastMessage;
import com.crossingguard.utils.AppConstants;
import com.crossingguard.utils.LogHelper;
import com.crossingguard.utils.MySharedPreferences;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 *
 */
public class BroadcastMessagePresenter<V extends BroadcastMessageMvpView> extends BasePresenter<V> implements BroadcastMessageMvpPresenter<V> {


    public BroadcastMessagePresenter(IDataManager IDataManager) {
        super(IDataManager);
    }

    private String TAG=BroadcastMessageActivity.class.getCanonicalName();

    @Override
    public void broadCastMessage(String message) {

        String superVisorId = "" + MySharedPreferences.getInstance().getInt(AppConstants.USER_ID);
        String timStamp = "" + System.currentTimeMillis();
        BroadCastMessage broadCastMessage = new BroadCastMessage();
        broadCastMessage.setMessage(message);
        broadCastMessage.setTimestamp("" + timStamp);

        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy h:mm:ss a", Locale.ENGLISH);
        Date date = Calendar.getInstance().getTime();

        broadCastMessage.setCreatedAt(df.format(date));

        getIDataManager().getFirebaseRootNode().getReference(AppConstants.FIREBASE_REFERENCE.BROAD_CAST_MESSAGE).child(superVisorId).child(timStamp).setValue(broadCastMessage);
        getMvpView().onSuccessSendBroadcast(broadCastMessage);
    }



    @Override
    public void hitNotificationToAllGuards(BroadCastMessage broadCastMessage) {
        getIDataManager().getCompositeDisposable().add(
                getIDataManager().getApiInterface().hitSendNotificationToGuard(broadCastMessage)
                        .subscribeOn(Schedulers.io())
                        .retry(3)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(commonResponse -> {
//                            getMvpView().onSuccess(commonResponse);
                        }, throwable -> {
//                            getMvpView().onFailure(throwable);
                        })
        );
    }
}
