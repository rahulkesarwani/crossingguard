package com.crossingguard.ui.createnewguard;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;

import androidx.appcompat.widget.AppCompatSpinner;

import com.crossingguard.R;
import com.crossingguard.baseactivities.BaseActivity;
import com.crossingguard.models.createguard.GuardModel;
import com.crossingguard.models.logins.LoginResponseModel;
import com.crossingguard.ui.login.LoginActivity;
import com.crossingguard.utils.AppConstants;
import com.crossingguard.utils.CheckNullable;
import com.crossingguard.utils.Helper;
import com.crossingguard.utils.MessagesUtils;
import com.crossingguard.utils.customviews.CustomButton;
import com.crossingguard.utils.customviews.CustomEditText;
import com.crossingguard.utils.customviews.CustomTextView;
import com.google.android.material.textfield.TextInputLayout;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;


public class CreateGuardActivity extends BaseActivity implements CreateGuardMvpView {


    @BindView(R.id.backIV)
    ImageView backIV;
    @BindView(R.id.firstNameET)
    CustomEditText firstNameET;
    @BindView(R.id.lastNameET)
    CustomEditText lastNameET;
    @BindView(R.id.mobileNoET)
    CustomEditText mobileNoET;
    @BindView(R.id.emailET)
    CustomEditText emailET;
    @BindView(R.id.addressET)
    CustomEditText addressET;
    @BindView(R.id.guardTypeSpinner)
    AppCompatSpinner guardTypeSpinner;
    @BindView(R.id.guardStatusSpinner)
    AppCompatSpinner guardStatusSpinner;
    @BindView(R.id.createGuardBtn)
    CustomButton createGuardBtn;
    @BindView(R.id.updateGuardBtn)
    CustomButton updateGuardBtn;
    @BindView(R.id.passwordET)
    CustomEditText passwordET;
    @BindView(R.id.titleTV)
    CustomTextView titleTV;
    @BindView(R.id.passwordLayout)
    TextInputLayout passwordLayout;


    private String TAG = CreateGuardActivity.class.getSimpleName();
    private int hitApiCounter;
    @Inject
    CreateGuardMvpPresenter<CreateGuardMvpView> mPresenter;

    private GuardModel updateGuardData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivityComponent().inject(CreateGuardActivity.this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(CreateGuardActivity.this);

        try {
            updateGuardData = getIntent().getExtras().getParcelable(AppConstants.GUARD_DATA);
        } catch (Exception e) {
        }
        handleGuardData();
        initUI();


    }

    private void handleGuardData() {
        if (updateGuardData == null) {
            titleTV.setText("Create Guard");
            createGuardBtn.setVisibility(View.VISIBLE);
            updateGuardBtn.setVisibility(View.GONE);
        } else {
            titleTV.setText("Update Guard");
            createGuardBtn.setVisibility(View.GONE);
            updateGuardBtn.setVisibility(View.VISIBLE);
            setGuardDataToViews();
        }
    }

    private void setGuardDataToViews() {

        firstNameET.setText(CheckNullable.checkNullable(updateGuardData.getFirstName()));
        lastNameET.setText(CheckNullable.checkNullable(updateGuardData.getLastName()));
        mobileNoET.setText(CheckNullable.checkNullable(updateGuardData.getPhone()));
        emailET.setText(CheckNullable.checkNullable(updateGuardData.getEmail()));
        addressET.setText(CheckNullable.checkNullable(updateGuardData.getAddress()));

        if (CheckNullable.checkNullable(updateGuardData.getJobType()).equalsIgnoreCase(AppConstants.GUARD_TYPE.FULL_TIME)) {
            guardTypeSpinner.setSelection(0);
        } else if (CheckNullable.checkNullable(updateGuardData.getJobType()).equalsIgnoreCase(AppConstants.GUARD_TYPE.PART_TIME)) {
            guardTypeSpinner.setSelection(1);
        } else if (CheckNullable.checkNullable(updateGuardData.getJobType()).equalsIgnoreCase(AppConstants.GUARD_TYPE.ON_CALL)) {
            guardTypeSpinner.setSelection(2);
        }

        if (CheckNullable.checkNullable(updateGuardData.getJobStatus()).equalsIgnoreCase(AppConstants.GUARD_STATUS.APPLICANT)) {
            guardStatusSpinner.setSelection(0);
        } else if (CheckNullable.checkNullable(updateGuardData.getJobStatus()).equalsIgnoreCase(AppConstants.GUARD_STATUS.ACTIVE)) {
            guardStatusSpinner.setSelection(1);
        } else if (CheckNullable.checkNullable(updateGuardData.getJobStatus()).equalsIgnoreCase(AppConstants.GUARD_STATUS.NOT_ACTIVE)) {
            guardStatusSpinner.setSelection(2);
        }
    }


    @Override
    protected int getActivityLayout() {
        return R.layout.activity_create_guard;
    }

    @Override
    protected void initUI() {
        initErrorViews(findViewById(R.id.error_views_layout));

        guardStatusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 1) {
                    passwordLayout.setVisibility(View.VISIBLE);
                } else {
                    passwordLayout.setVisibility(View.GONE);
                    passwordET.setText("");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    private void hitCreateGuardApi() {

        hitApiCounter = 1;
        if (checkBeforeApiHit()) {
            GuardModel requestModel = new GuardModel();

            requestModel.setFirstName(firstNameET.getText().toString().trim());
            requestModel.setLastName(lastNameET.getText().toString().trim());
            requestModel.setPhone(mobileNoET.getText().toString().trim());
            requestModel.setEmail(emailET.getText().toString().trim());
            requestModel.setAddress(addressET.getText().toString().trim());

            if (guardTypeSpinner.getSelectedItemId() == 0) {
                requestModel.setJobType(AppConstants.GUARD_TYPE.FULL_TIME);
            } else if (guardTypeSpinner.getSelectedItemId() == 1) {
                requestModel.setJobType(AppConstants.GUARD_TYPE.PART_TIME);
            } else if (guardTypeSpinner.getSelectedItemId() == 2) {
                requestModel.setJobType(AppConstants.GUARD_TYPE.ON_CALL);
            }

            if (guardStatusSpinner.getSelectedItemId() == 0) {
                requestModel.setJobStatus(AppConstants.GUARD_STATUS.APPLICANT);
            } else if (guardStatusSpinner.getSelectedItemId() == 1) {
                requestModel.setJobStatus(AppConstants.GUARD_STATUS.ACTIVE);
            } else if (guardStatusSpinner.getSelectedItemId() == 2) {
                requestModel.setJobStatus(AppConstants.GUARD_STATUS.NOT_ACTIVE);
            }


            requestModel.setVulnerableScreening(0);
            requestModel.setPassword(passwordET.getText().toString().trim());

            mPresenter.hitCreateGuardApi(requestModel);
        }
    }

    private void hitUpdateGuardApi() {

        hitApiCounter = 2;
        if (checkBeforeApiHit()) {


            updateGuardData.setFirstName(firstNameET.getText().toString().trim());
            updateGuardData.setLastName(lastNameET.getText().toString().trim());
            updateGuardData.setPhone(mobileNoET.getText().toString().trim());
            updateGuardData.setEmail(emailET.getText().toString().trim());
            updateGuardData.setAddress(addressET.getText().toString().trim());
            if (guardTypeSpinner.getSelectedItemId() == 0) {
                updateGuardData.setJobType(AppConstants.GUARD_TYPE.FULL_TIME);
            } else if (guardTypeSpinner.getSelectedItemId() == 1) {
                updateGuardData.setJobType(AppConstants.GUARD_TYPE.PART_TIME);
            } else if (guardTypeSpinner.getSelectedItemId() == 2) {
                updateGuardData.setJobType(AppConstants.GUARD_TYPE.ON_CALL);
            }

            if (guardStatusSpinner.getSelectedItemId() == 0) {
                updateGuardData.setJobStatus(AppConstants.GUARD_STATUS.APPLICANT);
            } else if (guardStatusSpinner.getSelectedItemId() == 1) {
                updateGuardData.setJobStatus(AppConstants.GUARD_STATUS.ACTIVE);
            } else if (guardStatusSpinner.getSelectedItemId() == 2) {
                updateGuardData.setJobStatus(AppConstants.GUARD_STATUS.NOT_ACTIVE);
            }

            updateGuardData.setPassword(passwordET.getText().toString().trim());

            mPresenter.hitUpdateGuardApi(updateGuardData);
        }
    }


    @Override
    public void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();

    }

    @Override
    protected void handleNoInternet() {

        checkAndhitApi();
    }

    @Override
    protected void handleSlowInternet() {

        checkAndhitApi();
    }

    @Override
    protected void handleServerError() {

        checkAndhitApi();
    }

    @Override
    protected void handleNoDataFound() {

        checkAndhitApi();
    }

    protected void handleNoLocationFound() {

        checkAndhitApi();
    }


    @Override
    public void onSuccess(Response<LoginResponseModel> response) {

        hideProgressBar();

        if(response.code()==401){
            startActivity(new Intent(this, LoginActivity.class));
            finishAffinity();
            return;
        }


        String errorResponse = "";
        try {
            errorResponse = response.errorBody().string();
        } catch (Exception e) {

        }

        if (!TextUtils.isEmpty(errorResponse)) {
            Helper.handleErrorBody(CreateGuardActivity.this, errorResponse);
        } else if (isResponseOK((short) response.code())) {
            showToast(response.body().getMessage());
            finish();
        }

    }

    @Override
    public void onSuccessUpdate(Response<LoginResponseModel> response) {
        hideProgressBar();
        String errorResponse = "";
        try {
            errorResponse = response.errorBody().string();
        } catch (Exception e) {

        }

        if (!TextUtils.isEmpty(errorResponse)) {
            Helper.handleErrorBody(CreateGuardActivity.this, errorResponse);
        } else if (isResponseOK((short) response.code())) {
            showToast(response.body().getMessage());
            finish();
        }
    }

    @Override
    public void onFailure(Throwable error) {
        handleFailure((Exception) error);
    }


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @OnClick({R.id.backIV, R.id.createGuardBtn, R.id.updateGuardBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIV:
                finish();
                break;
            case R.id.createGuardBtn:
                if (validateFields()) {
                    hitCreateGuardApi();
                }
                break;
            case R.id.updateGuardBtn:
                if (validateFields()) {
                    hitUpdateGuardApi();
                }
                break;
        }
    }


    private boolean validateFields() {

        if (firstNameET.getText().toString().trim().length() == 0) {
            MessagesUtils.showToastError(this, "Enter First Name");
            return false;
        } else if (lastNameET.getText().toString().trim().length() == 0) {
            MessagesUtils.showToastError(this, "Enter Last Name");
            return false;
        } else if (mobileNoET.getText().toString().length() == 0) {
            MessagesUtils.showToastError(this, "Enter Phone number");
            return false;
        } else if (emailET.getText().toString().length() == 0) {
            MessagesUtils.showToastError(this, "Enter Email Address");
            return false;
        } else if (addressET.getText().toString().length() == 0) {
            MessagesUtils.showToastError(this, "Enter Address");
            return false;
        } else if (passwordET.getText().toString().length() == 0 && passwordLayout.getVisibility() == (View.VISIBLE)) {
            MessagesUtils.showToastError(this, "Enter Password");
            return false;
        } else {
            return true;
        }

    }

    private void checkAndhitApi() {

        if (hitApiCounter == 1) {
            hitCreateGuardApi();
        } else if (hitApiCounter == 2) {
            hitUpdateGuardApi();
        }
    }

}
