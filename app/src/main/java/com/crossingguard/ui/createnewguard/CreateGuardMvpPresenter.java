package com.crossingguard.ui.createnewguard;

import com.crossingguard.baseactivities.base.MvpPresenter;
import com.crossingguard.models.createguard.GuardModel;

/**
 * Created by rahul on 6/14/2018.
 */

public interface CreateGuardMvpPresenter<V extends CreateGuardMvpView>  extends MvpPresenter<V> {
    void hitCreateGuardApi(GuardModel requestModel);

    void hitUpdateGuardApi(GuardModel requestModel);
}