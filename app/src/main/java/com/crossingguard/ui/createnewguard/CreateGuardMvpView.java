package com.crossingguard.ui.createnewguard;

import com.crossingguard.baseactivities.base.MvpView;
import com.crossingguard.models.logins.LoginResponseModel;

import retrofit2.Response;


public interface CreateGuardMvpView extends MvpView {

    void onSuccess(Response<LoginResponseModel> response);

    void onSuccessUpdate(Response<LoginResponseModel> response);

    void onFailure(Throwable error);

}
