package com.crossingguard.ui.createnewguard;

import com.crossingguard.baseactivities.base.BasePresenter;
import com.crossingguard.controller.IDataManager;
import com.crossingguard.models.createguard.GuardModel;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 *
 */
public class CreateGuardPresenter<V extends CreateGuardMvpView> extends BasePresenter<V> implements CreateGuardMvpPresenter<V> {


    public CreateGuardPresenter(IDataManager IDataManager) {
        super(IDataManager);
    }


    @Override
    public void hitCreateGuardApi(GuardModel requestModel) {
        getIDataManager().getCompositeDisposable().add(
                getIDataManager().getApiInterface().hitCreateGuardApi(requestModel)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(commonResponse -> {
                            getMvpView().onSuccess(commonResponse);
                        }, throwable -> {

                            getMvpView().onFailure(throwable);
                        })
        );
    }

    @Override
    public void hitUpdateGuardApi(GuardModel requestModel) {
        getIDataManager().getCompositeDisposable().add(
                getIDataManager().getApiInterface().hitUpdateGuardApi(requestModel.getId(), requestModel)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(commonResponse -> {
                            getMvpView().onSuccess(commonResponse);
                        }, throwable -> {

                            getMvpView().onFailure(throwable);
                        })
        );
    }
}
