package com.crossingguard.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.crossingguard.R;
import com.crossingguard.adapters.recyclerview.LocationListAdapter;
import com.crossingguard.baseactivities.BaseFragment;
import com.crossingguard.models.locations.LocationListModel;
import com.crossingguard.models.locations.LocationModel;
import com.crossingguard.ui.addlocation.AddLocationActivity;
import com.crossingguard.ui.login.LoginActivity;
import com.crossingguard.utils.Helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;


public class AllLocationsListFragment extends BaseFragment {

    @BindView(R.id.listRV)
    RecyclerView listRV;

    private View mView;
    private ArrayList<LocationModel> mList = new ArrayList<>();
    private LocationListAdapter locationListAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_all_locations_list, container, false);
        ButterKnife.bind(this, mView);
        getActivity().setTitle("Locations");
        initErrorViews(mView.findViewById(R.id.error_views_layout));
        initList();
        return mView;
    }

    @Override
    protected int getTitle() {
        return 0;
    }

    @Override
    protected int getFragmentLayout() {
        return 0;
    }

    @Override
    protected void handleNoInternet() {
        hitListApi();
    }

    @Override
    protected void handleSlowInternet() {
        hitListApi();
    }

    @Override
    protected void handleServerError() {
        hitListApi();
    }

    @Override
    public void onResume() {
        super.onResume();
        hitListApi();
    }

    private void initList() {
        locationListAdapter = new LocationListAdapter(getActivity(), mList);
        listRV.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        listRV.setAdapter(locationListAdapter);
    }

    @OnClick(R.id.addNewLocation)
    public void onViewClicked() {
        startActivity(new Intent(getActivity(), AddLocationActivity.class));
    }


    private void hitListApi() {
        if (checkBeforeApiHit()) {
            mCompositeDisposable.add(
                    commonApiInterface.hitLocationListApi()
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(commonResponse -> {
                                onSuccess(commonResponse);
                            }, throwable -> {
                                onFailure(throwable);
                            })
            );
        }
    }


    public void onSuccess(Response<LocationListModel> response) {

        hideProgressBar();

        if (response.code() == 401) {
            startActivity(new Intent(getActivity(), LoginActivity.class));
            getActivity().finishAffinity();
            return;
        }


        String errorResponse = "";
        try {
            errorResponse = response.errorBody().string();
        } catch (Exception e) {

        }

        if (!TextUtils.isEmpty(errorResponse)) {
            Helper.handleErrorBody(getActivity(), errorResponse);
        } else if (isResponseOK((short) response.code())) {

            mList.clear();
            mList.addAll(response.body().getData());
            if(mList.size()>1) {
                Collections.sort(mList, new MyLocationOrder());
            }
            locationListAdapter.notifyDataSetChanged();
        }

    }

    class MyLocationOrder implements Comparator<LocationModel> {
        @Override
        public int compare(LocationModel e1, LocationModel e2) {
            if (Integer.parseInt(e1.getGeofence().getId()) > Integer.parseInt(e2.getGeofence().getId())) {
                return 1;
            } else {
                return -1;
            }
        }
    }
    public void onFailure(Throwable error) {
        handleFailure((Exception) error);
    }
}
