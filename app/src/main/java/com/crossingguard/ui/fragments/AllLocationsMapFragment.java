package com.crossingguard.ui.fragments;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.crossingguard.R;
import com.crossingguard.baseactivities.BaseFragment;
import com.crossingguard.models.locations.LocationListModel;
import com.crossingguard.models.locations.LocationModel;
import com.crossingguard.ui.addlocation.AddLocationActivity;
import com.crossingguard.ui.login.LoginActivity;
import com.crossingguard.utils.AppConstants;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class AllLocationsMapFragment extends BaseFragment implements OnMapReadyCallback {


    private View mView;
    private GoogleMap mMap;
    private float zoom = 12;
    private ArrayList<LocationModel> mList = new ArrayList<>();

    private Handler refreshDataHandler = null;
    private Runnable refreshDataRunnable = null;

    private String currentLatitude, currentLongitude;
    private boolean isFirstTime=true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_all_locations_map, container, false);
        ButterKnife.bind(this, mView);
        getActivity().setTitle("Map");


        currentLatitude = mySharedPreferences.getString(AppConstants.LATITUDE, "0.0");
        currentLongitude = mySharedPreferences.getString(AppConstants.LONGITUDE, "0.0");

        addGoogleMap();
        startMembersLocationUpdateOnMap();
        return mView;
    }

    @Override
    protected int getTitle() {
        return 0;
    }

    @Override
    protected int getFragmentLayout() {
        return 0;
    }

    @Override
    protected void handleNoInternet() {

    }

    @Override
    protected void handleSlowInternet() {

    }

    @Override
    protected void handleServerError() {

    }


    /*Add Map in our fragment */
    private void addGoogleMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapFragment);
        mapFragment.getMapAsync(AllLocationsMapFragment.this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setIndoorLevelPickerEnabled(false);
        mMap.animateCamera(CameraUpdateFactory.zoomIn());
        mMap.animateCamera(CameraUpdateFactory.zoomTo(zoom), 5000, null);

        LatLng currentLatLng = new LatLng(Double.parseDouble(currentLatitude), Double.parseDouble(currentLongitude));
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(currentLatLng,
                zoom);
        googleMap.moveCamera(update);
    }

    @OnClick(R.id.addNewLocation)
    public void onViewClicked() {
        startActivity(new Intent(getActivity(), AddLocationActivity.class));
    }


    private void hitListApi() {
        if (mConnectionDetector.isInternetConnected()) {
            mCompositeDisposable.add(
                    commonApiInterface.hitLocationListApi()
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(commonResponse -> {
                                onSuccess(commonResponse);
                            }, throwable -> {
                                onFailure(throwable);
                            })
            );
        }
    }


    public void onSuccess(Response<LocationListModel> response) {

        if(response.code()==401){
            startActivity(new Intent(getActivity(), LoginActivity.class));
            getActivity().finishAffinity();
            return;
        }

        String errorResponse = "";
        try {
            errorResponse = response.errorBody().string();
        } catch (Exception e) {

        }

        if (!TextUtils.isEmpty(errorResponse)) {
//            Helper.handleErrorBody(getActivity(), errorResponse);
        } else if (response.code() == 200) {

            mList.clear();
            mList.addAll(response.body().getData());

            dropMarkerAtLocations();
        }

    }


    public void onFailure(Throwable error) {
//        handleFailure((Exception) error);
    }


    private void startMembersLocationUpdateOnMap() {
        refreshDataHandler = new Handler();
        refreshDataRunnable = new Runnable() {

            @Override
            public void run() {
                hitListApi();
//                5 seconds    //5000
                refreshDataHandler.postDelayed(this, 10000);
            }
        };
        refreshDataHandler.postDelayed(refreshDataRunnable, 0);
    }

    private void stopMembersLocationUpdateOnMap() {
        refreshDataHandler.removeCallbacks(refreshDataRunnable);
    }

    @Override
    public void onDestroyView() {
        stopMembersLocationUpdateOnMap();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        stopMembersLocationUpdateOnMap();
        super.onDestroy();
    }

    private void dropMarkerAtLocations() {

        mMap.clear();

        for (int i = 0; i < mList.size(); i++) {

            BitmapDescriptor bitmapDescriptor = null;

            LatLng markerLatLng = new LatLng(mList.get(i).getGeofence().getLatitude(), mList.get(i).getGeofence().getLongitude());


            // if guard availablethen put put green marker
            if (mList.get(i).getGuardAvailable()) {
//                bitmapDescriptor = BitmapDescriptorFactory.fromResource(R.drawable.open);
                mMap.addCircle(new CircleOptions()
                        .center(markerLatLng)
                        .radius(mList.get(i).getGeofence().getRadius())
                        .strokeColor(Color.BLUE)
                        .strokeWidth(1)
                        .fillColor(ContextCompat.getColor(getActivity(),R.color.circle_color)));
            } else {
//                bitmapDescriptor = BitmapDescriptorFactory.fromResource(R.drawable.closed);
                mMap.addCircle(new CircleOptions()
                        .center(markerLatLng)
                        .radius(mList.get(i).getGeofence().getRadius())
                        .strokeColor(Color.BLUE)
                        .strokeWidth(1)
                        .fillColor(ContextCompat.getColor(getActivity(),R.color.circle_dim_color)));
            }

            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(markerLatLng);
            markerOptions.snippet(mList.get(i).getName());
            markerOptions.title("#"+mList.get(i).getGeofence().getId()+" - "+mList.get(i).getName());
//            markerOptions.icon(bitmapDescriptor);
            mMap.addMarker(markerOptions);

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(markerLatLng)       // Sets the center of the map to Mountain View
                    .zoom(zoom)                   // Sets the zoom
                    .bearing(0)                // Sets the orientation of the camera to east
                    .tilt(30)             // Sets the tilt of the camera to 30 degrees
                    .build(); // .tilt(30)                    // Creates a CameraPosition from the builder

            if(isFirstTime) {
                isFirstTime=false;
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        }
    }
}
