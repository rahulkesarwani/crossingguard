package com.crossingguard.ui.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.crossingguard.R;
import com.crossingguard.adapters.recyclerview.AssignmentAdapter;
import com.crossingguard.baseactivities.BaseFragment;
import com.crossingguard.dialogs.AssignGuardDialog;
import com.crossingguard.models.assignments.AssignRequestModel;
import com.crossingguard.models.assignments.AssignmentListModel;
import com.crossingguard.models.assignments.AssignmentModel;
import com.crossingguard.models.createguard.GuardListModel;
import com.crossingguard.models.createguard.GuardModel;
import com.crossingguard.models.logins.LoginResponseModel;
import com.crossingguard.ui.login.LoginActivity;
import com.crossingguard.utils.Helper;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class AssignmentListFragment extends BaseFragment implements AssignmentAdapter.AssignClickListener {


    @BindView(R.id.listRV)
    RecyclerView listRV;

    private View mView;

    private AssignmentAdapter assignmentAdapter;
    private ArrayList<AssignmentModel> mList = new ArrayList<>();
    private ArrayList<GuardModel> guardModelArrayList = new ArrayList<>();
    private int hitApiCounter;

    private AssignRequestModel assignRequestModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_assignment_list, container, false);
        getActivity().setTitle("Assignments");
        ButterKnife.bind(this, mView);
        initErrorViews(mView.findViewById(R.id.error_views_layout));
        initList();
        hitActiveGuardListApi();
        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        hitListApi();
    }

    @Override
    protected int getTitle() {
        return 0;
    }

    @Override
    protected int getFragmentLayout() {
        return 0;
    }

    @Override
    protected void handleNoInternet() {
        checkAndhitApi();
    }

    @Override
    protected void handleSlowInternet() {
        checkAndhitApi();
    }

    @Override
    protected void handleServerError() {
        checkAndhitApi();
    }

    private void initList() {

        assignmentAdapter = new AssignmentAdapter(getActivity(), mList,this);
        listRV.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        listRV.setAdapter(assignmentAdapter);
    }

    private void hitListApi() {
        hitApiCounter = 1;
        if (checkBeforeApiHit()) {
            mCompositeDisposable.add(
                    commonApiInterface.hitAssignmentListModel()
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(commonResponse -> {
                                onSuccess(commonResponse);
                            }, throwable -> {
                                onFailure(throwable);
                            })
            );
        }
    }

    public void onSuccess(Response<AssignmentListModel> response) {
        hideProgressBar();

        if (response.code() == 401) {
            startActivity(new Intent(getActivity(), LoginActivity.class));
            getActivity().finishAffinity();
            return;
        }

        String errorResponse = "";
        try {
            errorResponse = response.errorBody().string();
        } catch (Exception e) {

        }

        if (!TextUtils.isEmpty(errorResponse)) {
            Helper.handleErrorBody(getActivity(), errorResponse);
        } else if (isResponseOK((short) response.code())) {

            mList.clear();
            mList.addAll(response.body().getData());
            assignmentAdapter.notifyDataSetChanged();
        }

    }


    public void onSuccessGuardList(Response<GuardListModel> response) {
        hideProgressBar();
        if (response.code() == 401) {
            startActivity(new Intent(getActivity(), LoginActivity.class));
            getActivity().finishAffinity();
            return;
        }


        String errorResponse = "";
        try {
            errorResponse = response.errorBody().string();
        } catch (Exception e) {

        }

        if (!TextUtils.isEmpty(errorResponse)) {
            Helper.handleErrorBody(getActivity(), errorResponse);
        } else if (isResponseOK((short) response.code())) {


            guardModelArrayList.addAll(response.body().getData());

        }
    }

    public void onSuccessAssignment(Response<LoginResponseModel> response) {
        hideProgressBar();
        if (response.code() == 401) {
            startActivity(new Intent(getActivity(), LoginActivity.class));
            getActivity().finishAffinity();
            return;
        }


        String errorResponse = "";
        try {
            errorResponse = response.errorBody().string();
        } catch (Exception e) {

        }

        if (!TextUtils.isEmpty(errorResponse)) {
            Helper.handleErrorBody(getActivity(), errorResponse);
        } else if (isResponseOK((short) response.code())) {
            hitListApi();
        }
    }

    public void onFailure(Throwable error) {
        handleFailure((Exception) error);
    }

    private void hitActiveGuardListApi() {
        hitApiCounter = 2;
        if (checkBeforeApiHit()) {

            HashMap<String,String> requestMap=new HashMap<>();

            requestMap.put("job_status","active");
            mCompositeDisposable.add(
                    commonApiInterface.hitGuardListModel(requestMap)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(commonResponse -> {

                                onSuccessGuardList(commonResponse);
                            }, throwable -> {

                                onFailure(throwable);
                            })
            );
        }
    }


    public void hitLocationAssignApi() {
        hitApiCounter = 3;
        if (checkBeforeApiHit()) {
            mCompositeDisposable.add(
                    commonApiInterface.hitGuardAssigmentApi(assignRequestModel)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(commonResponse -> {
                                onSuccessAssignment(commonResponse);
                            }, throwable -> {

                                onFailure(throwable);
                            })
            );
        }
    }


    public void hitNoGuardApi() {
        hitApiCounter = 4;
        if (checkBeforeApiHit()) {
            mCompositeDisposable.add(
                    commonApiInterface.hitNoGuardApi(assignRequestModel)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(commonResponse -> {
                                onSuccessNoGuard(commonResponse);
                            }, throwable -> {

                                onFailure(throwable);
                            })
            );
        }
    }


    public void onSuccessNoGuard(Response<LoginResponseModel> response) {
        hideProgressBar();
        if (response.code() == 401) {
            startActivity(new Intent(getActivity(), LoginActivity.class));
            getActivity().finishAffinity();
            return;
        }


        String errorResponse = "";
        try {
            errorResponse = response.errorBody().string();
        } catch (Exception e) {

        }

        if (!TextUtils.isEmpty(errorResponse)) {
            Helper.handleErrorBody(getActivity(), errorResponse);
        } else if (isResponseOK((short) response.code())) {
            hitListApi();
        }
    }


    private void checkAndhitApi() {
        if (hitApiCounter == 1) {
            hitListApi();
        } else if (hitApiCounter == 2) {
            hitActiveGuardListApi();
        } else if (hitApiCounter == 3) {
            hitLocationAssignApi();
        }else if(hitApiCounter==4){
            hitNoGuardApi();
        }
    }

    @Override
    public void onAssignClicked(int position, int childPosition) {
        AssignGuardDialog assignGuardDialog=new AssignGuardDialog(getActivity(), mList.get(position),childPosition,guardModelArrayList);
        assignGuardDialog.setonAssignListener(new AssignGuardDialog.AssignListerner() {
            @Override
            public void onDoneClicked(AssignRequestModel assignRequestModel) {

                AssignmentListFragment.this.assignRequestModel=assignRequestModel;

                hitLocationAssignApi();
            }

            @Override
            public void onNoGuardClicked(AssignRequestModel assignRequestModel) {
                AssignmentListFragment.this.assignRequestModel=assignRequestModel;
                hitNoGuardApi();
            }
        });
        assignGuardDialog.show();
    }
}
