package com.crossingguard.ui.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.crossingguard.R;
import com.crossingguard.adapters.recyclerview.EntryLogGroupAdapter;
import com.crossingguard.baseactivities.BaseFragment;
import com.crossingguard.models.checkins.EntryLog;
import com.crossingguard.models.checkins.EntryLogResponseModel;
import com.crossingguard.ui.login.LoginActivity;
import com.crossingguard.utils.EndlessRecyclerViewScrollListener;
import com.crossingguard.utils.Helper;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class CheckInLogFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {


    @BindView(R.id.listRV)
    RecyclerView listRV;
    @BindView(R.id.swipeLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    private View mView;


    private ArrayList<EntryLog> mList = new ArrayList<>();
    private EntryLogGroupAdapter checkInListAdapter;
    private int totalPage;
    private int currentPage = 1;
    private EndlessRecyclerViewScrollListener scrollListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_check_in_log, container, false);
        ButterKnife.bind(this, mView);
        swipeRefreshLayout.setOnRefreshListener(this);
        getActivity().setTitle("Check-In Logs");
        initErrorViews(mView.findViewById(R.id.error_views_layout));
        initList();
        hitListApi();
        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected int getTitle() {
        return 0;
    }

    @Override
    protected int getFragmentLayout() {
        return 0;
    }

    @Override
    protected void handleNoInternet() {
        hitListApi();
    }

    @Override
    protected void handleSlowInternet() {
        hitListApi();
    }

    @Override
    protected void handleServerError() {
        hitListApi();
    }


    private void initList() {

        checkInListAdapter = new EntryLogGroupAdapter(getActivity(), mList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        listRV.setLayoutManager(linearLayoutManager);

        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (currentPage < totalPage) {
                    currentPage += 1;
                    listRV.post(new Runnable() {
                        @Override
                        public void run() {
                            mList.add(null);
                            checkInListAdapter.notifyItemInserted(checkInListAdapter.getItemCount() - 1);
                            hitListApi();
                        }
                    });
                }
            }
        };
        listRV.addOnScrollListener(scrollListener);
        listRV.setAdapter(checkInListAdapter);
    }


    private void hitListApi() {
        if (checkBeforeApiHit(currentPage)) {
            mCompositeDisposable.add(
                    commonApiInterface.hitEntryLogModel(currentPage)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(commonResponse -> {
                                onSuccess(commonResponse);
                            }, throwable -> {
                                onFailure(throwable);
                            })
            );
        }
    }


    public void onSuccess(Response<EntryLogResponseModel> response) {
        if (swipeRefreshLayout != null)
            if (swipeRefreshLayout.isRefreshing())
                swipeRefreshLayout.setRefreshing(false);
        hideProgressBar();
        if (currentPage > 1) {
            mList.remove(mList.size() - 1);
            checkInListAdapter.notifyItemRemoved(mList.size());
        } else {
            mList.clear();
            checkInListAdapter.notifyDataSetChanged();
        }

        if (response.code() == 401) {
            startActivity(new Intent(getActivity(), LoginActivity.class));
            getActivity().finishAffinity();
            return;
        }

        String errorResponse = "";
        try {
            errorResponse = response.errorBody().string();
        } catch (Exception e) {

        }

        if (!TextUtils.isEmpty(errorResponse)) {
            Helper.handleErrorBody(getActivity(), errorResponse);
        } else if (isResponseOK((short) response.code())) {

            mList.addAll(response.body().getData().getEntryLog());

            checkInListAdapter.notifyDataSetChanged();
            if (response.body().getData().getLastPage() != null) {
                totalPage = response.body().getData().getLastPage();
            }
//            if (currentPage != 1) {
            scrollListener.setLoading();
        }

    }


    public void onFailure(Throwable error) {
        if (swipeRefreshLayout != null)
            if (swipeRefreshLayout.isRefreshing())
                swipeRefreshLayout.setRefreshing(false);
        handleFailure((Exception) error);
    }

    @Override
    public void onRefresh() {
        hitListApi();
    }
}
