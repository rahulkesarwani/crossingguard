package com.crossingguard.ui.fragments;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.crossingguard.R;
import com.crossingguard.adapters.recyclerview.ComplianceLogAdapter;
import com.crossingguard.baseactivities.BaseFragment;
import com.crossingguard.models.compliance.ComplianceLog;
import com.crossingguard.models.compliance.ComplianceResponseModel;
import com.crossingguard.ui.login.LoginActivity;
import com.crossingguard.utils.AppConstants;
import com.crossingguard.utils.EndlessRecyclerViewScrollListener;
import com.crossingguard.utils.Helper;
import com.crossingguard.utils.customviews.CustomTextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ComplianceLogFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {


    @BindView(R.id.filterBtn)
    AppCompatImageButton filterBtn;
    @BindView(R.id.listRV)
    RecyclerView listRV;
    @BindView(R.id.swipeLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    private ArrayList<ComplianceLog> mList = new ArrayList<>();

    private ComplianceLogAdapter complianceLogAdapter;
    private int totalPage;
    private int currentPage = 1;
    private EndlessRecyclerViewScrollListener scrollListener;

    private String filterString = AppConstants.COMPLIANCE_FILTER.ACTIVE;
    private View mView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_compliance_log, container, false);
        ButterKnife.bind(this, mView);
        swipeRefreshLayout.setOnRefreshListener(this);
        getActivity().setTitle("Compliance Logs");
        initErrorViews(mView.findViewById(R.id.error_views_layout));
        initList();
        hitListApi();
        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected int getTitle() {
        return 0;
    }

    @Override
    protected int getFragmentLayout() {
        return 0;
    }

    @Override
    protected void handleNoInternet() {
        hitListApi();
    }

    @Override
    protected void handleSlowInternet() {
        hitListApi();
    }

    @Override
    protected void handleServerError() {
        hitListApi();
    }


    private void initList() {

        complianceLogAdapter = new ComplianceLogAdapter(getActivity(), mList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        listRV.setLayoutManager(linearLayoutManager);

        scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (currentPage < totalPage) {
                    currentPage += 1;
                    listRV.post(new Runnable() {
                        @Override
                        public void run() {
                            mList.add(null);
                            complianceLogAdapter.notifyItemInserted(complianceLogAdapter.getItemCount() - 1);
                            hitListApi();
                        }
                    });
                }
            }
        };
        listRV.addOnScrollListener(scrollListener);
        listRV.setAdapter(complianceLogAdapter);
    }


    private void hitListApi() {
        if (checkBeforeApiHit(currentPage)) {

            String url;
            if (filterString.equalsIgnoreCase(AppConstants.COMPLIANCE_FILTER.ACTIVE)) {
                url = AppConstants.mBaseUrl + "compliance/logs?page=" + currentPage;
            } else {
                url = AppConstants.mBaseUrl + "compliance/issues?page=" + currentPage;
            }

            mCompositeDisposable.add(
                    commonApiInterface.hitComplianceLogApi(url)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(commonResponse -> {
                                onSuccess(commonResponse);
                            }, throwable -> {
                                onFailure(throwable);
                            })
            );
        }
    }


    public void onSuccess(Response<ComplianceResponseModel> response) {
        if (swipeRefreshLayout != null)
            if (swipeRefreshLayout.isRefreshing())
                swipeRefreshLayout.setRefreshing(false);
        hideProgressBar();
        if (currentPage > 1) {
            mList.remove(mList.size() - 1);
            complianceLogAdapter.notifyItemRemoved(mList.size());
        } else {
            mList.clear();
            complianceLogAdapter.notifyDataSetChanged();
        }

        if (response.code() == 401) {
            startActivity(new Intent(getActivity(), LoginActivity.class));
            getActivity().finishAffinity();
            return;
        }

        String errorResponse = "";
        try {
            errorResponse = response.errorBody().string();
        } catch (Exception e) {

        }

        if (!TextUtils.isEmpty(errorResponse)) {
            Helper.handleErrorBody(getActivity(), errorResponse);
        } else if (isResponseOK((short) response.code())) {

            if (currentPage == 1 && response.body().getData().getComplianceLog().size() == 0) {
                errorViews[3].setVisibility(View.VISIBLE);
                ((CustomTextView) errorViews[3].findViewById(R.id.no_data_found_message_CTV)).setText("No compliance issues");
                return;
            } else {
                errorViews[3].setVisibility(View.GONE);
            }

            mList.addAll(response.body().getData().getComplianceLog());
            if (mList.size() > 1) {
                Collections.sort(mList, new LocationOrderComparator());
            }

            complianceLogAdapter.notifyDataSetChanged();
            if (response.body().getData().getLastPage() != null) {
                totalPage = response.body().getData().getLastPage();
            }
//            if (currentPage != 1) {
            scrollListener.setLoading();
        }

    }

    private void showFilterPopup() {
        String[] colors = {"Active Locations", "Compliance Issues"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Apply Filter");
        builder.setItems(colors, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                switch (position) {
                    case 0:
                        filterString = AppConstants.COMPLIANCE_FILTER.ACTIVE;
                        currentPage = 1;
                        hitListApi();
                        break;
                    case 1:
                        filterString = AppConstants.COMPLIANCE_FILTER.ISSUES;
                        currentPage = 1;
                        hitListApi();
                        break;
                }

                // hitListApi();
            }
        });
        builder.show();
    }


    public void onFailure(Throwable error) {
        if (swipeRefreshLayout != null)
            if (swipeRefreshLayout.isRefreshing())
                swipeRefreshLayout.setRefreshing(false);
        handleFailure((Exception) error);
    }

    @Override
    public void onRefresh() {
        currentPage = 1;
        hitListApi();
    }

    @OnClick(R.id.filterBtn)
    public void onViewClicked() {
        showFilterPopup();
    }

    class LocationOrderComparator implements Comparator<ComplianceLog> {
        @Override
        public int compare(ComplianceLog e1, ComplianceLog e2) {
            if (Integer.parseInt(e1.getGeofenceId()) > Integer.parseInt(e2.getGeofenceId())) {
                return 1;
            } else {
                return -1;
            }
        }
    }
}
