package com.crossingguard.ui.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.crossingguard.R;
import com.crossingguard.adapters.recyclerview.GuardListAdapter;
import com.crossingguard.baseactivities.BaseFragment;
import com.crossingguard.models.createguard.GuardListModel;
import com.crossingguard.models.createguard.GuardModel;
import com.crossingguard.ui.createnewguard.CreateGuardActivity;
import com.crossingguard.ui.login.LoginActivity;
import com.crossingguard.utils.AppConstants;
import com.crossingguard.utils.Helper;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;


public class GuardListFragment extends BaseFragment {

    @BindView(R.id.listRV)
    RecyclerView listRV;
    @BindView(R.id.addNewGuardButton)
    FloatingActionButton addNewGuardButton;
    @BindView(R.id.filterBtn)
    AppCompatImageButton filterBtn;


    private View mView;
    private GuardListAdapter guardListAdapter;
    private ArrayList<GuardModel> mList = new ArrayList<>();
    private String filterString;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_guard_list, container, false);
        getActivity().setTitle("Guards List");
        ButterKnife.bind(this, mView);
        initErrorViews(mView.findViewById(R.id.error_views_layout));
        filterString = AppConstants.GUARD_STATUS.ALL;
        initList();

        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        hitListApi();
    }

    @Override
    protected int getTitle() {
        return 0;
    }

    @Override
    protected int getFragmentLayout() {
        return 0;
    }

    @Override
    protected void handleNoInternet() {
        hitListApi();
    }

    @Override
    protected void handleSlowInternet() {
        hitListApi();
    }

    @Override
    protected void handleServerError() {
        hitListApi();
    }


    private void initList() {
        guardListAdapter = new GuardListAdapter(getActivity(), mList);
        listRV.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        listRV.setAdapter(guardListAdapter);

    }

    @OnClick({R.id.addNewGuardButton, R.id.filterBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.addNewGuardButton:
                startActivity(new Intent(getActivity(), CreateGuardActivity.class));
                break;
            case R.id.filterBtn:
                showFilterPopup();
                break;
        }

    }


    private void hitListApi() {
        if (checkBeforeApiHit()) {

            HashMap<String, String> requestMap = new HashMap<>();

            if (!filterString.equalsIgnoreCase(AppConstants.GUARD_STATUS.ALL))
                requestMap.put("job_status", filterString);

            mCompositeDisposable.add(
                    commonApiInterface.hitGuardListModel(requestMap)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(commonResponse -> {
                                onSuccess(commonResponse);
                            }, throwable -> {
                                onFailure(throwable);
                            })
            );
        }
    }


    public void onSuccess(Response<GuardListModel> response) {
        hideProgressBar();

        if (response.code() == 401) {
            startActivity(new Intent(getActivity(), LoginActivity.class));
            getActivity().finishAffinity();
            return;
        }

        String errorResponse = "";
        try {
            errorResponse = response.errorBody().string();
        } catch (Exception e) {

        }

        if (!TextUtils.isEmpty(errorResponse)) {
            Helper.handleErrorBody(getActivity(), errorResponse);
        } else if (isResponseOK((short) response.code())) {

            mList.clear();
            mList.addAll(response.body().getData());
            if (mList.size() > 0) {
                Collections.sort(mList, new MyNameOrder());
            }
            guardListAdapter.notifyDataSetChanged();
        }

    }


    class MyNameOrder implements Comparator<GuardModel> {
        @Override
        public int compare(GuardModel e1, GuardModel e2) {

            return (e1.getFirstName() + " " + e1.getLastName()).compareToIgnoreCase(e2.getFirstName() + " " + e2.getLastName());
        }
    }

    private void showFilterPopup() {
        String[] colors = {"All", "Applicants", "Active (Hired)", "Not Active"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Pick status");
        builder.setItems(colors, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                switch (position) {
                    case 0:
                        filterString = AppConstants.GUARD_STATUS.ALL;
                        break;
                    case 1:
                        filterString = AppConstants.GUARD_STATUS.APPLICANT;
                        break;
                    case 2:
                        filterString = AppConstants.GUARD_STATUS.ACTIVE;
                        break;
                    case 3:
                        filterString = AppConstants.GUARD_STATUS.NOT_ACTIVE;
                        break;
                }

                hitListApi();
            }
        });
        builder.show();
    }

    public void onFailure(Throwable error) {
        handleFailure((Exception) error);
    }
}
