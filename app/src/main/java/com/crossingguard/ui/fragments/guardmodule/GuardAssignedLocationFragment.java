package com.crossingguard.ui.fragments.guardmodule;


import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.crossingguard.R;
import com.crossingguard.adapters.recyclerview.AssignmentAdapter;
import com.crossingguard.adapters.recyclerview.GuardAssignmentAdapter;
import com.crossingguard.baseactivities.BaseFragment;
import com.crossingguard.dialogs.AssignGuardDialog;
import com.crossingguard.models.assignments.AssignRequestModel;
import com.crossingguard.models.assignments.AssignmentListModel;
import com.crossingguard.models.assignments.AssignmentModel;
import com.crossingguard.models.createguard.GuardListModel;
import com.crossingguard.models.createguard.GuardModel;
import com.crossingguard.models.guardsassignedlocations.GuardLocationResponseModel;
import com.crossingguard.models.locations.LocationModel;
import com.crossingguard.models.logins.LoginResponseModel;
import com.crossingguard.ui.login.LoginActivity;
import com.crossingguard.utils.Helper;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class GuardAssignedLocationFragment extends BaseFragment {


    @BindView(R.id.listRV)
    RecyclerView listRV;

    private View mView;

    private GuardAssignmentAdapter assignmentAdapter;
    private ArrayList<LocationModel> mList = new ArrayList<>();
    private ArrayList<GuardModel> guardModelArrayList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_assignment_list, container, false);
        getActivity().setTitle("Assignments");
        ButterKnife.bind(this, mView);
        initErrorViews(mView.findViewById(R.id.error_views_layout));
        initList();
        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        hitListApi();
    }

    @Override
    protected int getTitle() {
        return 0;
    }

    @Override
    protected int getFragmentLayout() {
        return 0;
    }

    @Override
    protected void handleNoInternet() {
        checkAndhitApi();
    }

    @Override
    protected void handleSlowInternet() {
        checkAndhitApi();
    }

    @Override
    protected void handleServerError() {
        checkAndhitApi();
    }

    private void initList() {

        assignmentAdapter = new GuardAssignmentAdapter(getActivity(), mList);
        listRV.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        listRV.setAdapter(assignmentAdapter);
    }

    private void hitListApi() {

        if (checkBeforeApiHit()) {
            mCompositeDisposable.add(
                    commonApiInterface.hitGuardAssignedLocationApi()
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(commonResponse -> {
                                onSuccess(commonResponse);
                            }, throwable -> {
                                onFailure(throwable);
                            })
            );
        }
    }

    public void onSuccess(Response<GuardLocationResponseModel> response) {
        hideProgressBar();
        mList.clear();


        if (response.code() == 401) {
            startActivity(new Intent(getActivity(), LoginActivity.class));
            getActivity().finishAffinity();
            return;
        }

        String errorResponse = "";
        try {
            errorResponse = response.errorBody().string();
        } catch (Exception e) {

        }

        if (!TextUtils.isEmpty(errorResponse)) {
            Helper.handleErrorBody(getActivity(), errorResponse);
        } else if (isResponseOK((short) response.code())) {

            if (response.body().getLocations().size() > 0) {


                // update the assigned times with unique location

                for (int i = 0; i < response.body().getLocations().size(); i++) {
                    int isExistPos = isExistInList(response.body().getLocations().get(i).getId());

                    if (isExistPos == -1) {
                        //if doesnot exist then add into the list with time
                        mList.add(response.body().getLocations().get(i));
                        mList.get(mList.size()-1).getAssignedTimes().add(response.body().getLocations().get(i).getPivot().getJobTimes());

                    } else {
                        // else update the list data for that particular location
                        mList.get(isExistPos).getAssignedTimes().add(response.body().getLocations().get(i).getPivot().getJobTimes());
                    }
                }
                assignmentAdapter.notifyDataSetChanged();

            }
        }

    }

    public void onFailure(Throwable error) {
        handleFailure((Exception) error);
    }


    private void checkAndhitApi() {
        hitListApi();
    }


    private int isExistInList(Integer locationId) {
        int existPos = -1;
        for (int i = 0; i < mList.size(); i++) {
            if (mList.get(i).getId().equals(locationId)) {
                existPos = i;
                break;
            }
        }

        return existPos;
    }

}
