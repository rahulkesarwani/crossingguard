package com.crossingguard.ui.fragments.guardmodule;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.crossingguard.R;
import com.crossingguard.baseactivities.BaseFragment;
import com.crossingguard.db.AppDatabase;
import com.crossingguard.db.entity.CheckInOutDBModel;
import com.crossingguard.models.guardsassignedlocations.GuardLocationResponseModel;
import com.crossingguard.models.locations.GeofenceModel;
import com.crossingguard.models.locations.LocationModel;
import com.crossingguard.models.logins.Data;
import com.crossingguard.service.GeofenceLocationService;
import com.crossingguard.utils.AppConstants;
import com.crossingguard.utils.Helper;
import com.crossingguard.utils.LogHelper;
import com.crossingguard.utils.MessagesUtils;
import com.crossingguard.utils.MySharedPreferences;
import com.crossingguard.utils.Utility;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class GuardMapFragment extends BaseFragment implements OnMapReadyCallback {


    @BindView(R.id.checkinOutBtn)
    Button checkinOutBtn;


    private View mView;

    private String TAG = GuardMapFragment.class.getCanonicalName();
    private GoogleMap mMap;
    private float zoom = 15;
    private Marker currentUserMarker;
    private Location currentUserLatLng;

    private GuardLocationResponseModel responseModel;
    private HashMap<String, Circle> circleHashMap;
    private Handler refreshDataHandler = null;
    private Runnable refreshDataRunnable = null;

    private LocationModel currentActiveLocationForCheckInOut = null;
    private Integer userId;
    private String userEmail;
    private List<LocationModel> tempLocationsList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_guard_geofence, container, false);
        ButterKnife.bind(this, mView);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        responseModel = (GuardLocationResponseModel) mySharedPreferences.getObj(AppConstants.GUARD_LOCATION_DATA, GuardLocationResponseModel.class);
        circleHashMap = new HashMap<>();

        if (!Utility.isMyServiceRunning(GeofenceLocationService.class, getActivity())) {
            Utility.createNotificationChannel(getActivity());
            Utility.StartCheckerService(getActivity());
        }

        Data loginData = (Data) mySharedPreferences.getObj(AppConstants.LOGIN_DATA, Data.class);
        if (loginData != null) {
            userId = loginData.getUser().getId();
            userEmail = loginData.getUser().getEmail();
        }

        addGoogleMap();
        handleDatafromServer(responseModel, true);
        startCheckingUpdatedDataForGuard();
        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    protected int getTitle() {
        return 0;
    }

    @Override
    protected int getFragmentLayout() {
        return 0;
    }

    @Override
    protected void handleNoInternet() {

    }

    @Override
    protected void handleSlowInternet() {

    }

    @Override
    protected void handleServerError() {

    }

    private void initGofenceData() {
        circleHashMap.clear();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setIndoorLevelPickerEnabled(false);
        mMap.animateCamera(CameraUpdateFactory.zoomIn());
        mMap.animateCamera(CameraUpdateFactory.zoomTo(zoom), 5000, null);

        LogHelper.d(TAG, "onMapReady");
        updateDataOnMap();
    }


    private void updateDataOnMap() {
        mMap.clear();
        if (responseModel == null)
            return;

        if (responseModel.getLocations().size() > 0) {

            for (int i = 0; i < responseModel.getLocations().size(); i++) {
                LatLng locationLatlng = new LatLng(responseModel.getLocations().get(i).getGeofence().getLatitude(), responseModel.getLocations().get(i).getGeofence().getLongitude());

                CameraUpdate update = CameraUpdateFactory.newLatLngZoom(locationLatlng, zoom);
                mMap.moveCamera(update);

                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(locationLatlng);
                markerOptions.title("#" + responseModel.getLocations().get(i).getGeofence().getId() + " - " + responseModel.getLocations().get(i).getName());
                markerOptions.snippet(responseModel.getLocations().get(i).getGeofence().getAddress());
                mMap.addMarker(markerOptions);
                addUpdateCircle(responseModel.getLocations().get(i).getGeofence().getId(), ContextCompat.getColor(getActivity(), R.color.circle_color), locationLatlng, responseModel.getLocations().get(i).getGeofence().getRadius());
            }
        }
    }

    /*Add Map in our fragment */
    private void addGoogleMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapFragment);
        mapFragment.getMapAsync(GuardMapFragment.this);

    }


    private void dropMarkerAtCurrentLocation() {
        if (currentUserMarker != null) {
            currentUserMarker.remove();
        }

        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_profile);
        Bitmap smallMarker = Bitmap.createScaledBitmap(icon, 100, 100, false);

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(new LatLng(currentUserLatLng.getLatitude(), currentUserLatLng.getLongitude()));

        View markerView = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker, null);
        CircleImageView markerImageView = markerView.findViewById(R.id.imgUserImage);
        markerImageView.setImageBitmap(smallMarker);
        markerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        markerView.layout(0, 0, markerView.getMeasuredWidth(), markerView.getMeasuredHeight());
        markerView.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(markerView.getMeasuredWidth(), markerView.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = markerView.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        markerView.draw(canvas);
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(returnedBitmap));


        currentUserMarker = mMap.addMarker(markerOptions);

    }


    @Override
    public void onDestroyView() {
        stopMembersLocationUpdateOnMap();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Location location) {
        Log.d(TAG, "onMessageEvent: " + location.getLatitude() + "   " + location.getLongitude());
        this.currentUserLatLng = location;

        //reset current active location
        currentActiveLocationForCheckInOut = null;
        if (currentUserLatLng != null && responseModel.getLocations().size() > 0) {
            dropMarkerAtCurrentLocation();

            for (int i = 0; i < responseModel.getLocations().size(); i++) {
                // calculate distance for drawing circle in red or green
                Location placeLoction = new Location("Place Location");
                placeLoction.setLatitude(responseModel.getLocations().get(i).getGeofence().getLatitude());
                placeLoction.setLongitude(responseModel.getLocations().get(i).getGeofence().getLongitude());

                float distance = getDistanceBetweenGuardCurrentAndLocation(currentUserLatLng, placeLoction);

                if (distance < responseModel.getLocations().get(i).getGeofence().getRadius()) {

                    //within the radius now put green circle
                    addUpdateCircle(responseModel.getLocations().get(i).getGeofence().getId(), ContextCompat.getColor(getActivity(), R.color.circle_color_green), new LatLng(responseModel.getLocations().get(i).getGeofence().getLatitude(), responseModel.getLocations().get(i).getGeofence().getLongitude()), responseModel.getLocations().get(i).getGeofence().getRadius());


                } else {
                    // outside of radius so put red circle
                    addUpdateCircle(responseModel.getLocations().get(i).getGeofence().getId(), ContextCompat.getColor(getActivity(), R.color.circle_color_red), new LatLng(responseModel.getLocations().get(i).getGeofence().getLatitude(), responseModel.getLocations().get(i).getGeofence().getLongitude()), responseModel.getLocations().get(i).getGeofence().getRadius());

                }
            }
        }
    }


    private boolean isAlreadyCheckin(Integer id) {
        if (MySharedPreferences.getInstance().getBoolean(AppConstants.LAST_CHECKED_IN_LOCATION + id)) {
            //last checked in location is same as current active location so the user is in the same place
            return true;
        } else {
            return false;
        }

    }


    private float getDistanceBetweenGuardCurrentAndLocation(Location guardcurrentLocation, Location actualPlaceLocation) {

        return guardcurrentLocation.distanceTo(actualPlaceLocation);
    }

    private void addUpdateCircle(String id, int colorCode, LatLng latLng, Float radius) {
        if (!circleHashMap.containsKey(id)) {
            Circle geofenceCircle = mMap.addCircle(new CircleOptions()
                    .center(latLng)
                    .radius(radius)
                    .strokeColor(Color.BLUE)
                    .strokeWidth(1)
                    .fillColor(colorCode));
            circleHashMap.put(id, geofenceCircle);
        } else {
            Circle geofenceCircle = circleHashMap.get(id);
            geofenceCircle.setFillColor(colorCode);
        }
    }


    private void submitCheckinOut() {
        if (checkinOutBtn.getText().toString().equals(getString(R.string.checking_string))) {

            LogHelper.d(TAG, "You are Checkin " + currentActiveLocationForCheckInOut.getId());

            CheckInOutDBModel checkInOutDBModel = new CheckInOutDBModel();
            checkInOutDBModel.setEventType("check-in");
            checkInOutDBModel.setLocationId(currentActiveLocationForCheckInOut.getId());
            checkInOutDBModel.setUserId(userId);
            checkInOutDBModel.setSecret(Helper.getBase64Secret(userId + ":" + userEmail));
            checkInOutDBModel.setCreatedAt(System.currentTimeMillis());
            checkInOutDBModel.setGeoLat(String.valueOf(currentUserLatLng.getLatitude()));
            checkInOutDBModel.setGeoLng(String.valueOf(currentUserLatLng.getLongitude()));
            checkInOutDBModel.setGeoAddress(MySharedPreferences.getInstance().getString(AppConstants.ADDRESS));

            addLogInQueue(checkInOutDBModel);

            //save last checkin location id
            MySharedPreferences.getInstance().putBoolean(AppConstants.LAST_CHECKED_IN_LOCATION + currentActiveLocationForCheckInOut.getId(), true);
            checkinOutBtn.setText(getString(R.string.checkout_string));

            MessagesUtils.showToastSuccess(getActivity(), "Check-In Recorded Successfully");
        } else if (checkinOutBtn.getText().toString().equals(getString(R.string.checkout_string))) {

            LogHelper.d(TAG, "You are checkout ");

            CheckInOutDBModel checkInOutDBModel = new CheckInOutDBModel();
            checkInOutDBModel.setEventType("check-out");
            checkInOutDBModel.setLocationId(currentActiveLocationForCheckInOut.getId());
            checkInOutDBModel.setUserId(userId);
            checkInOutDBModel.setSecret(Helper.getBase64Secret(userId + ":" + userEmail));
            checkInOutDBModel.setCreatedAt(System.currentTimeMillis());

            addLogInQueue(checkInOutDBModel);

            //remove last checkin location
            MySharedPreferences.getInstance().putBoolean(AppConstants.LAST_CHECKED_IN_LOCATION + currentActiveLocationForCheckInOut.getId(), false);
            checkinOutBtn.setText(getString(R.string.checking_string));
            MessagesUtils.showToastSuccess(getActivity(), "Check-Out Recorded Successfully");
        }
    }

    public void addLogInQueue(CheckInOutDBModel checkInOutDBModel) {

        Observable.fromCallable(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                return AppDatabase.getAppDatabase(getContext()).checkInOutEventDao().add(checkInOutDBModel);
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<Object>() {
            @Override
            public void accept(@NonNull Object object) throws Exception {
                LogHelper.d(TAG, "accept: New IN Out recording >>>" + checkInOutDBModel.getEventType());
            }
        });
    }

    @OnClick({R.id.checkinOutBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.checkinOutBtn:
                if (currentActiveLocationForCheckInOut == null) {
                    MessagesUtils.showToastInfo(getActivity(), "No Active Location Found");
                    return;
                }
                submitCheckinOut();
                break;
        }
    }


    private void startCheckingUpdatedDataForGuard() {
        refreshDataHandler = new Handler();
        refreshDataRunnable = new Runnable() {

            @Override
            public void run() {

                checkCurrentTimeExistInAssignedLocations();
                hitApiToPullLatestLocation();
//                7 seconds    //7000
                refreshDataHandler.postDelayed(this, 7000);
            }
        };
        refreshDataHandler.postDelayed(refreshDataRunnable, 0);
    }

    private void stopMembersLocationUpdateOnMap() {
        refreshDataHandler.removeCallbacks(refreshDataRunnable);
    }


    private void hitApiToPullLatestLocation() {
        mCompositeDisposable.add(
                commonApiInterface.hitGuardAssignedLocationApi()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(commonResponse -> {
                            onSuccess(commonResponse);
                        }, throwable -> {
                        })
        );
    }


    public void onSuccess(Response<GuardLocationResponseModel> response) {
        if (response.code() == 200) {
            if (response.body().getLocations().size() > 0) {
                handleDatafromServer(response.body(), false);
            } else {
//                MessagesUtils.showToastError(this,"No Location assigned");
                mMap.clear();
                mySharedPreferences.setObj(AppConstants.GUARD_LOCATION_DATA, null);
            }
        }
    }


    private void handleDatafromServer(GuardLocationResponseModel response, boolean isInitial) {
        GuardLocationResponseModel localData = (GuardLocationResponseModel) mySharedPreferences.getObj(AppConstants.GUARD_LOCATION_DATA, GuardLocationResponseModel.class);

        //if server pulled data is changed from the last stored data then update data and remove and set geofence again.
        boolean dataChanged = isDataChanged(response, localData);
        LogHelper.d(TAG, "onSuccess: " + dataChanged);
        if (dataChanged || isInitial) {

            mySharedPreferences.setObj(AppConstants.GUARD_LOCATION_DATA, response);
            tempLocationsList = new ArrayList<>();

            for (int i = 0; i < response.getLocations().size(); i++) {
                int isExistPos = isExistInList(response.getLocations().get(i).getId());

                if (isExistPos == -1) {
                    //if doesnot exist then add into the list with time
                    tempLocationsList.add(response.getLocations().get(i));
                    tempLocationsList.get(tempLocationsList.size() - 1).getAssignedTimes().add(response.getLocations().get(i).getPivot().getJobTimes());

                } else {
                    // else update the list data for that particular location
                    tempLocationsList.get(isExistPos).getAssignedTimes().add(response.getLocations().get(i).getPivot().getJobTimes());
                }
            }
            //remove response locations andd update the locations here.

            response.getLocations().clear();
            response.setLocations(tempLocationsList);

            responseModel = response;

            checkCurrentTimeExistInAssignedLocations();

            initGofenceData();
            if (!isInitial) {
                updateDataOnMap();
                onMessageEvent(currentUserLatLng);
            }
        }

    }

    private void checkCurrentTimeExistInAssignedLocations() {

        DateFormat df = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
        String currentTime = df.format(new Date());

        for (int i = 0; i < responseModel.getLocations().size(); i++) {

            boolean isActiveTime = false;

            for (int j = 0; j < responseModel.getLocations().get(i).getAssignedTimes().size(); j++) {
                String assignedTimes[] = responseModel.getLocations().get(i).getAssignedTimes().get(j).replaceAll("\\[", "").replaceAll("]", "").replaceAll("\"", "").split(";");
                assignedTimes[0] = assignedTimes[0] + ":00";
                assignedTimes[1] = assignedTimes[1] + ":00";

                LogHelper.d(TAG, " assgined Times>>>>  " + assignedTimes[0] + "    " + assignedTimes[1]);
                if (checkTime(assignedTimes[0], assignedTimes[1], currentTime)) {

                    checkinOutBtn.setVisibility(View.VISIBLE);
                    //find location with current time so enable check-in
                    currentActiveLocationForCheckInOut = responseModel.getLocations().get(i);

                    if (isAlreadyCheckin(currentActiveLocationForCheckInOut.getId())) {
                        checkinOutBtn.setText(getString(R.string.checkout_string));
                    } else {
                        checkinOutBtn.setText(getString(R.string.checking_string));
                    }

                    isActiveTime = true;
                    break;
                } else {
                    checkinOutBtn.setVisibility(View.GONE);
                }
            }

            if(!isActiveTime) {
                //handle for locations which the guard was checkin but could not apply checkout so the server will auto do that part. and local has to show again checkin
                mySharedPreferences.putBoolean(AppConstants.LAST_CHECKED_IN_LOCATION+responseModel.getLocations().get(i).getId(),false);
            }
        }

    }


    private boolean checkTime(String startTime, String endTime, String checkTime) {

        LogHelper.d(TAG, "startTime>>>>   " + startTime + "   EndTime>>>>> " + endTime + "   checkTime>>>>>>>" + checkTime);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss", Locale.US);
        LocalTime startLocalTime = LocalTime.parse(startTime, formatter);
        LocalTime endLocalTime = LocalTime.parse(endTime, formatter);
        LocalTime checkLocalTime = LocalTime.parse(checkTime, formatter);

        boolean isInBetween = false;
        if (endLocalTime.isAfter(startLocalTime)) {
            if (startLocalTime.isBefore(checkLocalTime) && endLocalTime.isAfter(checkLocalTime)) {
                isInBetween = true;
            }
        } else if (checkLocalTime.isAfter(startLocalTime) || checkLocalTime.isBefore(endLocalTime)) {
            isInBetween = true;
        }

        if (isInBetween) {
            LogHelper.d(TAG, "Is in between!");
            return true;
        } else {
            LogHelper.d(TAG, "Is not in between!");
            return false;
        }
    }

    private boolean isDataChanged(GuardLocationResponseModel serverResponse, GuardLocationResponseModel localData) {

        if (localData == null)
            return true;

        LogHelper.d(TAG, "isDataChanged: " + serverResponse.toString() + " \n localdata: " + localData.toString());

        if (!serverResponse.toString().equalsIgnoreCase(localData.toString())) {
            return true;
        } else {
            return false;
        }
    }

    private int isExistInList(Integer locationId) {
        int existPos = -1;
        for (int i = 0; i < tempLocationsList.size(); i++) {
            if (tempLocationsList.get(i).getId().equals(locationId)) {
                existPos = i;
                break;
            }
        }

        return existPos;
    }
}
