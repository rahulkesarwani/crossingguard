package com.crossingguard.ui.guardhome;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.crossingguard.R;
import com.crossingguard.baseactivities.BaseActivity;
import com.crossingguard.models.guardsassignedlocations.GuardLocationResponseModel;
import com.crossingguard.models.logins.Data;
import com.crossingguard.service.RepeatLocationPresenter;
import com.crossingguard.ui.broadcastmessage.BroadcastMessageActivity;
import com.crossingguard.ui.fragments.guardmodule.GuardAssignedLocationFragment;
import com.crossingguard.ui.fragments.guardmodule.GuardMapFragment;
import com.crossingguard.ui.login.LoginActivity;
import com.crossingguard.ui.profile.ProfileActivity;
import com.crossingguard.utils.AppConstants;
import com.crossingguard.utils.CheckNullable;
import com.crossingguard.utils.MySharedPreferences;
import com.crossingguard.utils.Utility;
import com.crossingguard.utils.customviews.CustomTextView;
import com.google.android.material.navigation.NavigationView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import retrofit2.Response;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;


public class GuardHomeActivity extends BaseActivity implements GuardHomeMvpView, NavigationView.OnNavigationItemSelectedListener {

    private String TAG = GuardHomeActivity.class.getSimpleName();

    @Inject
    GuardHomeMvpPresenter<GuardHomeMvpView> mPresenter;

    private DrawerLayout drawerLayout;
    private FragmentManager fragmentManager;
    private NavigationView navigationView;

    private FrameLayout redCircle;
    private CustomTextView countTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       if(!EventBus.getDefault().isRegistered(this)){
           EventBus.getDefault().register(this);
       }

        getActivityComponent().inject(GuardHomeActivity.this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(GuardHomeActivity.this);


        initUI();

    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_guard_home;
    }

    @Override
    protected void initUI() {
        initErrorViews(findViewById(R.id.error_views_layout));

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view_guard);
        navigationView.bringToFront();

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        setNavigationHeaderData();

        hitLocationListApi();
    }


    private void hitLocationListApi() {

//        boolean isDataAlreadyPulled = mySharedPreferences.getBoolean(AppConstants.LOCATION_DATA_PULLED, false);
//        if (isDataAlreadyPulled)
//            return;
        if (checkBeforeApiHit()) {
            mPresenter.hitGuardAssignedLocationsApi();
        }
    }

    private void setNavigationHeaderData() {
        navigationView.getHeaderView(0).findViewById(R.id.headerLayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(ProfileActivity.class);
            }
        });


        Data loginData = (Data) mySharedPreferences.getObj(AppConstants.LOGIN_DATA, Data.class);

        if (loginData != null) {
            ((CustomTextView) navigationView.getHeaderView(0).findViewById(R.id.superVisorNameTV)).setText(CheckNullable.checkNullable(loginData.getUser().getFirstName()) + " " + CheckNullable.checkNullable(loginData.getUser().getLastName()));

            if (loginData.getUser().getType().equalsIgnoreCase("supervisor")) {
                ((CustomTextView) navigationView.getHeaderView(0).findViewById(R.id.gaurdTypeNameTV)).setText("Supervisor");
            } else {
                ((CustomTextView) navigationView.getHeaderView(0).findViewById(R.id.gaurdTypeNameTV)).setText("Guard");
            }
            ((CustomTextView) navigationView.getHeaderView(0).findViewById(R.id.superVisorNameTV)).setText(CheckNullable.checkNullable(loginData.getUser().getFirstName()) + " " + CheckNullable.checkNullable(loginData.getUser().getLastName()));

            try {
                PackageManager manager = getPackageManager();
                PackageInfo info = manager.getPackageInfo(getPackageName(), PackageManager.GET_ACTIVITIES);
                ((CustomTextView) navigationView.getHeaderView(0).findViewById(R.id.versionTV)).setText("Version - " + info.versionName);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


    @Override
    public void onDestroy() {
        mPresenter.onDetach();
        if(EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);
        }
        super.onDestroy();

    }

    @Override
    protected void handleNoInternet() {

        hitLocationListApi();
    }

    @Override
    protected void handleSlowInternet() {

        hitLocationListApi();
    }

    @Override
    protected void handleServerError() {
        hitLocationListApi();
    }

    @Override
    protected void handleNoDataFound() {

        hitLocationListApi();
    }

    protected void handleNoLocationFound() {

        hitLocationListApi();
    }

    @Override
    public void onSuccess(Response<GuardLocationResponseModel> response) {

        if (isResponseOK((short) response.code())) {
            mySharedPreferences.setObj(AppConstants.GUARD_LOCATION_DATA, response.body());
            loadMapFragment();

        }
    }

    @Override
    public void onFailure(Throwable error) {
        handleFailure((Exception) error);
    }


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    private void loadMapFragment() {

        changeFragment(new GuardMapFragment(), AppConstants.GEOFENCE_FRAGMENT);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        drawerLayout.closeDrawer(GravityCompat.START);

        switch (menuItem.getItemId()) {
            case R.id.nav_logout:
                mySharedPreferences.setObj(AppConstants.LOGIN_DATA, null);
                mySharedPreferences.putString(AppConstants.LOGIN_TYPE, "");
                mySharedPreferences.putString(AppConstants.ACCESS_TOKEN, "");
                mySharedPreferences.putBoolean(AppConstants.GEOFENCES_ADDED_KEY, false);
                mySharedPreferences.putInt(AppConstants.NOTIFICATION_COUNTER,0);

                RepeatLocationPresenter.deleteAllDataFromQueue(this);

                Utility.StopService(this);
                startActivity(LoginActivity.class);
                finishAffinity();

                break;
            case R.id.nav_guardassignents:
                changeFragment(new GuardAssignedLocationFragment(), AppConstants.GUARD_ASSIGNED_LOCATION);

                break;
            case R.id.nav_guardmap:

                changeFragment(new GuardMapFragment(), AppConstants.GEOFENCE_FRAGMENT);
                break;
            case R.id.nav_broadcastMessage:
                BroadcastMessageActivity.start(this);
                break;
        }
        return true;
    }


    public void changeFragment(Fragment fragment, String tag) {
        try {
            fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.nav_host_fragment, fragment).addToBackStack(tag).commitAllowingStateLoss();
        } catch (Exception e) {
        }
    }


    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else if (getVisibleFragment() != null && getVisibleFragment() instanceof GuardMapFragment) {
            finish();
        } else if (fragmentManager.getBackStackEntryCount() > 1) {
            super.onBackPressed();
        }
    }


    public Fragment getVisibleFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if (fragment != null && fragment.isVisible())
                    return fragment;
            }
        }
        return null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.top_guard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_broadcast:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        final MenuItem alertMenuItem = menu.findItem(R.id.menu_broadcast);
        FrameLayout rootView = (FrameLayout) alertMenuItem.getActionView();

        redCircle = (FrameLayout) rootView.findViewById(R.id.view_alert_red_circle);
        countTextView = (CustomTextView) rootView.findViewById(R.id.view_alert_count_textview);

        ((FrameLayout) rootView.findViewById(R.id.main)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BroadcastMessageActivity.start(GuardHomeActivity.this);

            }
        });
        updateAlertIcon();
        return super.onPrepareOptionsMenu(menu);
    }

    public void updateAlertIcon() {
        // if alert count extends into two digits, just show the red circle
        if(countTextView==null || redCircle==null)
            return;

        if (MySharedPreferences.getInstance().getInt(AppConstants.NOTIFICATION_COUNTER, 0) > 0) {
            countTextView.setText(String.valueOf(MySharedPreferences.getInstance().getInt(AppConstants.NOTIFICATION_COUNTER, 0)));
            redCircle.setVisibility(VISIBLE);
        } else {
            countTextView.setText("");
            redCircle.setVisibility(GONE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateAlertIcon();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(String eventType) {

        if(eventType.equals(AppConstants.EVENT_CONSTANTS.EVENT_BROADCAST_NOTIFICATION)){
            updateAlertIcon();
        }
    }


}
