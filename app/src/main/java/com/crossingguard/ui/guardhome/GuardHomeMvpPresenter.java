package com.crossingguard.ui.guardhome;

import com.crossingguard.baseactivities.base.MvpPresenter;

/**
 * Created by rahul on 6/14/2018.
 */

public interface GuardHomeMvpPresenter<V extends GuardHomeMvpView>  extends MvpPresenter<V> {
    void hitGuardAssignedLocationsApi();
}