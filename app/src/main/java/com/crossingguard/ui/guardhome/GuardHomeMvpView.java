package com.crossingguard.ui.guardhome;

import com.crossingguard.baseactivities.base.MvpView;
import com.crossingguard.models.guardsassignedlocations.GuardLocationResponseModel;

import retrofit2.Response;


public interface GuardHomeMvpView extends MvpView {

    void onSuccess(Response<GuardLocationResponseModel> response);

    void onFailure(Throwable error);

}
