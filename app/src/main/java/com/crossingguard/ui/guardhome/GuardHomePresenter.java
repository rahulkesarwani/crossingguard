package com.crossingguard.ui.guardhome;

import com.crossingguard.baseactivities.base.BasePresenter;
import com.crossingguard.controller.IDataManager;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 *
 * */
public class GuardHomePresenter<V extends GuardHomeMvpView> extends BasePresenter<V> implements GuardHomeMvpPresenter<V> {


    public GuardHomePresenter(IDataManager IDataManager) {
        super(IDataManager);
    }



    @Override
    public void hitGuardAssignedLocationsApi() {
        getIDataManager().getCompositeDisposable().add(
                getIDataManager().getApiInterface().hitGuardAssignedLocationApi()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(commonResponse -> {
                            getMvpView().onSuccess(commonResponse);
                        }, throwable -> {

                            getMvpView().onFailure(throwable);
                        })
        );
    }
}
