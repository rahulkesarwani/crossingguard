package com.crossingguard.ui.locationassignment;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import androidx.appcompat.widget.AppCompatSpinner;

import com.crossingguard.R;
import com.crossingguard.baseactivities.BaseActivity;
import com.crossingguard.models.createguard.GuardListModel;
import com.crossingguard.models.createguard.GuardModel;
import com.crossingguard.models.locations.LocationListModel;
import com.crossingguard.models.locations.LocationModel;
import com.crossingguard.models.logins.LoginResponseModel;
import com.crossingguard.ui.login.LoginActivity;
import com.crossingguard.utils.Helper;
import com.crossingguard.utils.customviews.CustomButton;
import com.crossingguard.utils.customviews.CustomTextView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;


public class LocationAssignmentActivity extends BaseActivity implements LocationAssignmentMvpView {


    @BindView(R.id.backIV)
    ImageView backIV;
    @BindView(R.id.titleTV)
    CustomTextView titleTV;
    @BindView(R.id.locationSpinner)
    AppCompatSpinner locationSpinner;
    @BindView(R.id.guardSpinner)
    AppCompatSpinner guardSpinner;
    @BindView(R.id.assignBtn)
    CustomButton assignBtn;
    private String TAG = LocationAssignmentActivity.class.getSimpleName();


    private ArrayList<GuardModel> guardModelArrayList = new ArrayList<>();
    private ArrayList<LocationModel> locationModelArrayList = new ArrayList<>();

    @Inject
    LocationAssignmentMvpPresenter<LocationAssignmentMvpView> mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivityComponent().inject(LocationAssignmentActivity.this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(LocationAssignmentActivity.this);


        initUI();

    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_location_assignment;
    }

    @Override
    protected void initUI() {
        initErrorViews(findViewById(R.id.error_views_layout));

    }


    private void hitAddNewLocationApi() {

        if (checkBeforeApiHit()) {

        }
    }


    @Override
    public void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();

    }

    @Override
    protected void handleNoInternet() {

        hitAddNewLocationApi();
    }

    @Override
    protected void handleSlowInternet() {

        hitAddNewLocationApi();
    }

    @Override
    protected void handleServerError() {

        hitAddNewLocationApi();
    }

    @Override
    protected void handleNoDataFound() {

        hitAddNewLocationApi();
    }

    protected void handleNoLocationFound() {

        hitAddNewLocationApi();
    }


    @Override
    public void onSuccessGuardList(Response<GuardListModel> response) {
        hideProgressBar();
        if (response.code() == 401) {
            startActivity(new Intent(this, LoginActivity.class));
            finishAffinity();
            return;
        }


        String errorResponse = "";
        try {
            errorResponse = response.errorBody().string();
        } catch (Exception e) {

        }

        if (!TextUtils.isEmpty(errorResponse)) {
            Helper.handleErrorBody(LocationAssignmentActivity.this, errorResponse);
        } else if (isResponseOK((short) response.code())) {

            guardModelArrayList.addAll(response.body().getData());
            initGuardsSpinner();
        }
    }

    @Override
    public void onSuccessLocationList(Response<LocationListModel> response) {
        hideProgressBar();
        if (response.code() == 401) {
            startActivity(new Intent(this, LoginActivity.class));
            finishAffinity();
            return;
        }


        String errorResponse = "";
        try {
            errorResponse = response.errorBody().string();
        } catch (Exception e) {

        }

        if (!TextUtils.isEmpty(errorResponse)) {
            Helper.handleErrorBody(LocationAssignmentActivity.this, errorResponse);
        } else if (isResponseOK((short) response.code())) {
            locationModelArrayList.addAll(response.body().getData());
            initLocationSpinner();
        }
    }

    @Override
    public void onSuccessAssignment(Response<LoginResponseModel> response) {

    }

    @Override
    public void onFailure(Throwable error) {
        handleFailure((Exception) error);
    }


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }


    @OnClick({R.id.back, R.id.assignBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.assignBtn:


                break;

        }
    }



    private boolean validateFields(){
        return true;
    }


    private void initLocationSpinner() {

        String[] locations = new String[locationModelArrayList.size() + 1];


        locations[0] = "";
        for (int i = 0; i < locationModelArrayList.size(); i++) {
            locations[i + 1] = locationModelArrayList.get(i).getName();
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, locations);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        locationSpinner.setAdapter(dataAdapter);
    }


    private void initGuardsSpinner() {

        String[] guards = new String[guardModelArrayList.size() + 1];

        guards[0]="";
        for (int i = 0; i < guardModelArrayList.size(); i++) {
            guards[i] = guardModelArrayList.get(i).getFirstName() + " " + guardModelArrayList.get(i).getLastName();
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, guards);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        guardSpinner.setAdapter(dataAdapter);
    }
}
