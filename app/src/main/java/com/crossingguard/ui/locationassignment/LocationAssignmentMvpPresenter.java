package com.crossingguard.ui.locationassignment;

import com.crossingguard.baseactivities.base.MvpPresenter;
import com.crossingguard.models.assignments.AssignRequestModel;

/**
 * Created by rahul on 6/14/2018.
 */

public interface LocationAssignmentMvpPresenter<V extends LocationAssignmentMvpView>  extends MvpPresenter<V> {
    void hitGuardList();

    void hitLocationsList();

    void hitLocationAssignApi(AssignRequestModel assignRequestModel);
}