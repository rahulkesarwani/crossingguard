package com.crossingguard.ui.locationassignment;

import com.crossingguard.baseactivities.base.MvpView;
import com.crossingguard.models.createguard.GuardListModel;
import com.crossingguard.models.locations.LocationListModel;
import com.crossingguard.models.logins.LoginResponseModel;

import retrofit2.Response;


public interface LocationAssignmentMvpView extends MvpView {

    void onSuccessGuardList(Response<GuardListModel> response);

    void onSuccessLocationList(Response<LocationListModel> response);

    void onSuccessAssignment(Response<LoginResponseModel> response);

    void onFailure(Throwable error);

}
