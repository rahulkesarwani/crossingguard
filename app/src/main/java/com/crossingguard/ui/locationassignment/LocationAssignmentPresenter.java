package com.crossingguard.ui.locationassignment;

import com.crossingguard.baseactivities.base.BasePresenter;
import com.crossingguard.controller.IDataManager;
import com.crossingguard.models.assignments.AssignRequestModel;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 *
 * */
public class LocationAssignmentPresenter<V extends LocationAssignmentMvpView> extends BasePresenter<V> implements LocationAssignmentMvpPresenter<V> {


    public LocationAssignmentPresenter(IDataManager IDataManager) {
        super(IDataManager);
    }


    @Override
    public void hitGuardList() {
//        getIDataManager().getCompositeDisposable().add(
//                getIDataManager().getApiInterface().hitGuardListModel()
//                        .subscribeOn(Schedulers.io())
//                        .observeOn(AndroidSchedulers.mainThread())
//                        .subscribe(commonResponse -> {
//
//                            getMvpView().onSuccessGuardList(commonResponse);
//                        }, throwable -> {
//
//                            getMvpView().onFailure(throwable);
//                        })
//        );
    }

    @Override
    public void hitLocationsList() {
        getIDataManager().getCompositeDisposable().add(
                getIDataManager().getApiInterface().hitLocationListApi()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(commonResponse -> {

                            getMvpView().onSuccessLocationList(commonResponse);
                        }, throwable -> {

                            getMvpView().onFailure(throwable);
                        })
        );
    }

    @Override
    public void hitLocationAssignApi(AssignRequestModel assignRequestModel) {
        getIDataManager().getCompositeDisposable().add(
                getIDataManager().getApiInterface().hitGuardAssigmentApi(assignRequestModel)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(commonResponse -> {
                            getMvpView().onSuccessAssignment(commonResponse);
                        }, throwable -> {

                            getMvpView().onFailure(throwable);
                        })
        );
    }
}
