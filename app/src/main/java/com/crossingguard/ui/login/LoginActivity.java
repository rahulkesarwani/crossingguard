package com.crossingguard.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.crossingguard.R;
import com.crossingguard.baseactivities.BaseActivity;
import com.crossingguard.models.logins.LoginRequestModel;
import com.crossingguard.models.logins.LoginResponseModel;
import com.crossingguard.ui.guardhome.GuardHomeActivity;
import com.crossingguard.ui.supervisorhome.SuperVisorHomeActivity;
import com.crossingguard.utils.AppConstants;
import com.crossingguard.utils.Helper;
import com.crossingguard.utils.MessagesUtils;
import com.crossingguard.utils.MySharedPreferences;
import com.crossingguard.utils.customviews.CustomButton;
import com.crossingguard.utils.customviews.CustomEditText;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;


public class LoginActivity extends BaseActivity implements LoginMvpView {

    @BindView(R.id.userNameET)
    CustomEditText userNameET;
    @BindView(R.id.passwordET)
    CustomEditText passwordET;
    @BindView(R.id.loginBtn)
    CustomButton loginBtn;
    private String TAG = LoginActivity.class.getSimpleName();

    @Inject
    LoginMvpPresenter<LoginMvpView> mPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivityComponent().inject(LoginActivity.this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(LoginActivity.this);

        initUI();

    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_login;
    }

    @Override
    protected void initUI() {
        initErrorViews(findViewById(R.id.error_views_layout));

    }


    private void hitLoginApi() {

        if (checkBeforeApiHit()) {
            LoginRequestModel loginRequestModel = new LoginRequestModel();
            loginRequestModel.setEmail(userNameET.getText().toString().trim());
            loginRequestModel.setPassword(passwordET.getText().toString().trim());
            loginRequestModel.setFcmToken(MySharedPreferences.getInstance().getString(AppConstants.FCM_TOKEN));

            mPresenter.hitLogin(loginRequestModel);
        }
    }


    @Override
    public void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();

    }

    @Override
    protected void handleNoInternet() {

        hitLoginApi();
    }

    @Override
    protected void handleSlowInternet() {

        hitLoginApi();
    }

    @Override
    protected void handleServerError() {
        hitLoginApi();
    }

    @Override
    protected void handleNoDataFound() {
        hitLoginApi();
    }

    protected void handleNoLocationFound() {
        hitLoginApi();
    }


    @Override
    public void onSuccess(Response<LoginResponseModel> response) {


        if (response.code() == 401) {
            startActivity(new Intent(this, LoginActivity.class));
            finishAffinity();
            return;
        }

        String errorResponse = "";
        try {
            errorResponse = response.errorBody().string();
        } catch (Exception e) {

        }

        if (!TextUtils.isEmpty(errorResponse)) {
            hideProgressBar();
            Helper.handleErrorBody(LoginActivity.this, errorResponse);
        } else if (isResponseOK((short) response.code())) {

            if (response.body().getSuccess()) {
                mPresenter.registerUserOnFirebase(response.body());
            } else {
                hideProgressBar();
                showToast(response.body().getMessage());
            }
        }
    }

    @Override
    public void onSuccessFirebaseRegister(LoginResponseModel response) {
        hideProgressBar();
        if (response.getData().getUser().getType().equalsIgnoreCase("supervisor")) {
            mySharedPreferences.putString(AppConstants.ACCESS_TOKEN, response.getData().getUser().getAuthToken());
            mySharedPreferences.putString(AppConstants.LOGIN_TYPE, response.getData().getUser().getType());
            mySharedPreferences.setObj(AppConstants.LOGIN_DATA, response.getData());
            mySharedPreferences.putInt(AppConstants.USER_ID, response.getData().getUser().getId());

            startActivity(SuperVisorHomeActivity.class);
            finish();
        } else {
            mySharedPreferences.putString(AppConstants.ACCESS_TOKEN, response.getData().getUser().getAuthToken());
            mySharedPreferences.putString(AppConstants.LOGIN_TYPE, response.getData().getUser().getType());
            mySharedPreferences.setObj(AppConstants.LOGIN_DATA, response.getData());
            mySharedPreferences.putInt(AppConstants.USER_ID, response.getData().getUser().getId());
            mySharedPreferences.putInt(AppConstants.SUPERVISOR_ID, response.getData().getUser().getSupervisorId() == null ? 2 : response.getData().getUser().getSupervisorId());

            startActivity(GuardHomeActivity.class);
            finish();
        }
    }

    @Override
    public void onFailure(Throwable error) {
        handleFailure((Exception) error);
    }


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @OnClick(R.id.loginBtn)
    public void onViewClicked() {
        if (validateFields())
            hitLoginApi();
    }

    private boolean validateFields() {
        if (userNameET.getText().toString().trim().length() == 0) {
            MessagesUtils.showToastError(this, "Enter Email");
            return false;
        } else if (passwordET.getText().toString().trim().length() == 0) {
            MessagesUtils.showToastError(this, "Enter Password");
            return false;
        } else {
            return true;
        }
    }
}
