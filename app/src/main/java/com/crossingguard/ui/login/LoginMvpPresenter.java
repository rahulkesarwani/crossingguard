package com.crossingguard.ui.login;

import com.crossingguard.baseactivities.base.MvpPresenter;
import com.crossingguard.models.firebase.UserModel;
import com.crossingguard.models.logins.LoginRequestModel;
import com.crossingguard.models.logins.LoginResponseModel;

/**
 * Created by rahul on 6/14/2018.
 */

public interface LoginMvpPresenter<V extends LoginMvpView>  extends MvpPresenter<V> {
    void hitLogin(LoginRequestModel loginRequestModel);

    void registerUserOnFirebase(LoginResponseModel responseModel);
}