package com.crossingguard.ui.login;

import com.crossingguard.baseactivities.base.MvpView;
import com.crossingguard.models.logins.LoginResponseModel;

import retrofit2.Response;


public interface LoginMvpView extends MvpView {

    void onSuccess(Response<LoginResponseModel> response);

    void onSuccessFirebaseRegister(LoginResponseModel responseModel);

    void onFailure(Throwable error);

}
