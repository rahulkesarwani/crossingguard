package com.crossingguard.ui.login;

import com.crossingguard.baseactivities.base.BasePresenter;
import com.crossingguard.controller.IDataManager;
import com.crossingguard.models.firebase.UserModel;
import com.crossingguard.models.logins.LoginRequestModel;
import com.crossingguard.models.logins.LoginResponseModel;
import com.crossingguard.utils.AppConstants;
import com.crossingguard.utils.MySharedPreferences;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 *
 */
public class LoginPresenter<V extends LoginMvpView> extends BasePresenter<V> implements LoginMvpPresenter<V> {


    public LoginPresenter(IDataManager IDataManager) {
        super(IDataManager);
    }

    @Override
    public void hitLogin(LoginRequestModel loginRequestModel) {

        getIDataManager().getCompositeDisposable().add(getIDataManager().getApiInterface().hitLoginApi(loginRequestModel)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse, this::handleError));

    }

    @Override
    public void registerUserOnFirebase(LoginResponseModel responseModel) {

        UserModel userModel = new UserModel();
        userModel.setId(responseModel.getData().getUser().getId());
        userModel.setEmail(responseModel.getData().getUser().getEmail());
        userModel.setFirstName(responseModel.getData().getUser().getFirstName());
        userModel.setLastName(responseModel.getData().getUser().getLastName());
        userModel.setType(responseModel.getData().getUser().getType());
        userModel.setFcmToken(MySharedPreferences.getInstance().getString(AppConstants.FCM_TOKEN));

        //add supervisor id to reference the supervisor broadcast messages
        userModel.setSupervisorId(responseModel.getData().getUser().getSupervisorId());

        //add userid as key index in firebase
        getIDataManager().getFirebaseRootNode().getReference(AppConstants.FIREBASE_REFERENCE.USERS).child("" + responseModel.getData().getUser().getId()).setValue(userModel);

        getMvpView().onSuccessFirebaseRegister(responseModel);
    }


    private void handleResponse(Response<LoginResponseModel> value) {
        getMvpView().onSuccess(value);
    }

    private void handleError(Throwable error) {

        getMvpView().onFailure(error);
    }


}
