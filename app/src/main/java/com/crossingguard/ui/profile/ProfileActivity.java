package com.crossingguard.ui.profile;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.crossingguard.R;
import com.crossingguard.baseactivities.BaseActivity;
import com.crossingguard.models.logins.Data;
import com.crossingguard.models.logins.LoginResponseModel;
import com.crossingguard.utils.AppConstants;
import com.crossingguard.utils.CheckNullable;
import com.crossingguard.utils.Helper;
import com.crossingguard.utils.customviews.CustomTextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Response;


public class ProfileActivity extends BaseActivity implements ProfileMvpView {

    @BindView(R.id.backIV)
    ImageView backIV;
    @BindView(R.id.topRL)
    RelativeLayout topRL;
    @BindView(R.id.profileIV)
    CircleImageView profileIV;
    @BindView(R.id.nameTV)
    CustomTextView nameTV;
    @BindView(R.id.userTypeTV)
    CustomTextView userTypeTV;
    @BindView(R.id.guardTypeTV)
    CustomTextView guardTypeTV;
    @BindView(R.id.assignedToTV)
    CustomTextView assignedToTV;
    @BindView(R.id.timingTV)
    CustomTextView timingTV;
    @BindView(R.id.guardSinceTV)
    CustomTextView guardSinceTV;
    private String TAG = ProfileActivity.class.getSimpleName();

    @Inject
    ProfileMvpPresenter<ProfileMvpView> mPresenter;

    private Data userData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivityComponent().inject(ProfileActivity.this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(ProfileActivity.this);

        userData = (Data) mySharedPreferences.getObj(AppConstants.LOGIN_DATA, Data.class);
        initUI();

    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_profile;
    }

    @Override
    protected void initUI() {

        setData();
    }


    @Override
    public void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();

    }

    @Override
    protected void handleNoInternet() {


    }

    @Override
    protected void handleSlowInternet() {


    }

    @Override
    protected void handleServerError() {


    }

    @Override
    protected void handleNoDataFound() {


    }

    protected void handleNoLocationFound() {


    }


    @Override
    public void onSuccess(Response<LoginResponseModel> response) {
        if (isResponseOK((short) response.code())) {


        }

    }

    @Override
    public void onFailure(Throwable error) {
        handleFailure((Exception) error);
    }


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @OnClick({R.id.backIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backIV:
                finish();
                break;

        }
    }


    private void setData() {

        if (userData != null) {
            nameTV.setText(CheckNullable.checkNullable(userData.getUser().getFirstName()) + " " + CheckNullable.checkNullable(userData.getUser().getLastName()));

            if (CheckNullable.checkNullable(userData.getUser().getType()).equalsIgnoreCase("supervisor")) {
                userTypeTV.setText("Supervisor");


                guardTypeTV.setVisibility(View.GONE);
                assignedToTV.setVisibility(View.GONE);
                timingTV.setVisibility(View.GONE);
                guardSinceTV.setVisibility(View.GONE);

            } else {
                userTypeTV.setText("Crossing Guard");

                try {
                    String assignlocationName = CheckNullable.checkNullable(userData.getUser().getLocations().get(0).getGeofence().getId()) + " " + CheckNullable.checkNullable(userData.getUser().getLocations().get(0).getName());
                    assignedToTV.setText("Assigned to: #" + assignlocationName);

                    StringBuilder timings = new StringBuilder();
                    for (String timedta : userData.getUser().getLocations().get(0).getCoverageTimes()) {

                        String time[] = timedta.split(";");
                        timings.append(time[0] + " - " + time[1]);
                        timings.append(" / ");

                    }
                    timingTV.setText(timings.toString());

                    if(CheckNullable.checkNullable(userData.getUser().getJobType()).equalsIgnoreCase(AppConstants.GUARD_TYPE.FULL_TIME)){
                            guardTypeTV.setText("Full Time");
                    }else if(CheckNullable.checkNullable(userData.getUser().getJobType()).equalsIgnoreCase(AppConstants.GUARD_TYPE.PART_TIME)){
                        guardTypeTV.setText("Part Time");
                    }else if(CheckNullable.checkNullable(userData.getUser().getJobType()).equalsIgnoreCase(AppConstants.GUARD_TYPE.ON_CALL)){
                        guardTypeTV.setText("On Call");
                    }

//                2019-12-03 05:37:17
                    String date = Helper.convertDateFormmat(CheckNullable.checkNullable(userData.getUser().getCreatedAt()), "yyyy-MM-dd HH:mm:ss", "MM-yyyy");
                    guardSinceTV.setText("Guard Since: "+date);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }
}
