package com.crossingguard.ui.profile;

import com.crossingguard.baseactivities.base.MvpPresenter;
import com.crossingguard.models.logins.LoginRequestModel;

/**
 * Created by rahul on 6/14/2018.
 */

public interface ProfileMvpPresenter<V extends ProfileMvpView>  extends MvpPresenter<V> {
    void hitCreateGuardApi(LoginRequestModel requestModel);
}