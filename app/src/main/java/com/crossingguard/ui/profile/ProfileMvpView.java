package com.crossingguard.ui.profile;

import com.crossingguard.baseactivities.base.MvpView;
import com.crossingguard.models.logins.LoginResponseModel;

import retrofit2.Response;


public interface ProfileMvpView extends MvpView {

    void onSuccess(Response<LoginResponseModel> response);

    void onFailure(Throwable error);

}
