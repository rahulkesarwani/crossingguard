package com.crossingguard.ui.profile;

import com.crossingguard.baseactivities.base.BasePresenter;
import com.crossingguard.controller.IDataManager;
import com.crossingguard.models.logins.LoginRequestModel;
import com.crossingguard.models.logins.LoginResponseModel;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 *
 * */
public class ProfilePresenter<V extends ProfileMvpView> extends BasePresenter<V> implements ProfileMvpPresenter<V> {


    public ProfilePresenter(IDataManager IDataManager) {
        super(IDataManager);
    }

    private void handleResponse(Response<LoginResponseModel> value) {
        getMvpView().onSuccess(value);
    }

    private void handleError(Throwable error) {

        getMvpView().onFailure(error);
    }

    @Override
    public void hitCreateGuardApi(LoginRequestModel requestModel) {
        getIDataManager().getCompositeDisposable().add(
                getIDataManager().getApiInterface().hitLoginApi(requestModel)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(commonResponse -> {

                            getMvpView().onSuccess(commonResponse);
                        }, throwable -> {

                            getMvpView().onFailure(throwable);
                        })
        );
    }
}
