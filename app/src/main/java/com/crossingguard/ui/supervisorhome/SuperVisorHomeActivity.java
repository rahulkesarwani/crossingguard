package com.crossingguard.ui.supervisorhome;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.ui.AppBarConfiguration;

import com.crossingguard.R;
import com.crossingguard.baseactivities.BaseActivity;
import com.crossingguard.models.logins.Data;
import com.crossingguard.models.logins.LoginRequestModel;
import com.crossingguard.models.logins.LoginResponseModel;
import com.crossingguard.service.RepeatLocationPresenter;
import com.crossingguard.ui.broadcastmessage.BroadcastMessageActivity;
import com.crossingguard.ui.fragments.AllLocationsListFragment;
import com.crossingguard.ui.fragments.AllLocationsMapFragment;
import com.crossingguard.ui.fragments.AssignmentListFragment;
import com.crossingguard.ui.fragments.CheckInLogFragment;
import com.crossingguard.ui.fragments.ComplianceLogFragment;
import com.crossingguard.ui.fragments.GuardListFragment;
import com.crossingguard.ui.login.LoginActivity;
import com.crossingguard.ui.profile.ProfileActivity;
import com.crossingguard.utils.AppConstants;
import com.crossingguard.utils.CheckNullable;
import com.crossingguard.utils.customviews.CustomTextView;
import com.google.android.material.navigation.NavigationView;

import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import retrofit2.Response;


public class SuperVisorHomeActivity extends BaseActivity implements SuperVisorHomeMvpView, NavigationView.OnNavigationItemSelectedListener {

    private String TAG = SuperVisorHomeActivity.class.getSimpleName();

    @Inject
    SuperVisorHomeMvpPresenter<SuperVisorHomeMvpView> mPresenter;
    private AppBarConfiguration mAppBarConfiguration;

    private DrawerLayout drawerLayout;
    private FragmentManager fragmentManager;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivityComponent().inject(SuperVisorHomeActivity.this);
        setUnBinder(ButterKnife.bind(this));
        mPresenter.onAttach(SuperVisorHomeActivity.this);

        initUI();
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_super_visor_home;
    }

    @Override
    protected void initUI() {
//        initErrorViews(findViewById(R.id.error_views_layout));


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        setNavigationHeaderData();

        loadMapFragment();
    }

    private void setNavigationHeaderData() {
        navigationView.getHeaderView(0).findViewById(R.id.headerLayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(ProfileActivity.class);
            }
        });


        Data loginData = (Data) mySharedPreferences.getObj(AppConstants.LOGIN_DATA, Data.class);

        if (loginData != null) {
            ((CustomTextView) navigationView.getHeaderView(0).findViewById(R.id.superVisorNameTV)).setText(CheckNullable.checkNullable(loginData.getUser().getFirstName()) + " " + CheckNullable.checkNullable(loginData.getUser().getLastName()));

            if (loginData.getUser().getType().equalsIgnoreCase("supervisor")) {
                ((CustomTextView) navigationView.getHeaderView(0).findViewById(R.id.gaurdTypeNameTV)).setText("Supervisor");
            } else {
                ((CustomTextView) navigationView.getHeaderView(0).findViewById(R.id.gaurdTypeNameTV)).setText("Guard");
            }

            try {
                PackageManager manager = getPackageManager();
                PackageInfo info = manager.getPackageInfo(getPackageName(), PackageManager.GET_ACTIVITIES);
                ((CustomTextView) navigationView.getHeaderView(0).findViewById(R.id.versionTV)).setText("Version - " + info.versionName);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void hitLoginApi() {

        if (checkBeforeApiHit()) {
            LoginRequestModel loginRequestModel = new LoginRequestModel();
            mPresenter.hitLogin(loginRequestModel);
        }
    }


    @Override
    public void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();

    }

    @Override
    protected void handleNoInternet() {

        hitLoginApi();
    }

    @Override
    protected void handleSlowInternet() {

        hitLoginApi();
    }

    @Override
    protected void handleServerError() {
        hitLoginApi();
    }

    @Override
    protected void handleNoDataFound() {

        hitLoginApi();
    }

    protected void handleNoLocationFound() {

        hitLoginApi();
    }


    @Override
    public void onSuccess(Response<LoginResponseModel> response) {
        if (isResponseOK((short) response.code())) {


        }

    }

    @Override
    public void onFailure(Throwable error) {
        handleFailure((Exception) error);
    }


    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    private void loadMapFragment() {
        AllLocationsMapFragment mapFragment = new AllLocationsMapFragment();
        changeFragment(mapFragment, AppConstants.MAP_FRAGMENT);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        drawerLayout.closeDrawer(GravityCompat.START);

        switch (menuItem.getItemId()) {

            case R.id.nav_mapview:
                loadMapFragment();
                break;
            case R.id.nav_guardList:
                GuardListFragment guardListFragment = new GuardListFragment();
                changeFragment(guardListFragment, AppConstants.MAP_GUARD_LIST_FRAGMENT);

                break;
            case R.id.nav_locations:
                AllLocationsListFragment locationsListFragment = new AllLocationsListFragment();
                changeFragment(locationsListFragment, AppConstants.LOCATION_LIST_FRAGMENT);
                break;
            case R.id.nav_assignments:
                AssignmentListFragment assignmentListFragment = new AssignmentListFragment();
                changeFragment(assignmentListFragment, AppConstants.ASSIGNMENT_FRAGMENT);

                break;
            case R.id.nav_checkinLogs:
                CheckInLogFragment checkInLogFragment = new CheckInLogFragment();
                changeFragment(checkInLogFragment, AppConstants.CHECKIN_LOG_FRAGMENT);
                break;
            case R.id.nav_logout:
                mySharedPreferences.setObj(AppConstants.LOGIN_DATA, null);
                mySharedPreferences.putString(AppConstants.LOGIN_TYPE, "");
                mySharedPreferences.putString(AppConstants.ACCESS_TOKEN, "");
                mySharedPreferences.putInt(AppConstants.NOTIFICATION_COUNTER,0);

                RepeatLocationPresenter.deleteAllDataFromQueue(this);

                startActivity(LoginActivity.class);
                finishAffinity();
                break;
            case R.id.nav_complianceLogs:
                ComplianceLogFragment complianceLogFragment = new ComplianceLogFragment();
                changeFragment(complianceLogFragment, AppConstants.COMPLIANCE_LOG_FRAGMENT);
                break;
            case R.id.nav_broadcastMessage:
                BroadcastMessageActivity.start(this);
                break;
        }
        return true;
    }


    public void changeFragment(Fragment fragment, String tag) {
        try {
            fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.nav_host_fragment, fragment).addToBackStack(tag).commitAllowingStateLoss();
        } catch (Exception e) {
        }
    }


    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else if (getVisibleFragment() != null && getVisibleFragment() instanceof AllLocationsListFragment) {
            finish();
        } else if (fragmentManager.getBackStackEntryCount() > 1) {
            super.onBackPressed();
        }
    }


    public Fragment getVisibleFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if (fragment != null && fragment.isVisible())
                    return fragment;
            }
        }
        return null;
    }
}
