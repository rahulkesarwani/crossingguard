package com.crossingguard.ui.supervisorhome;

import com.crossingguard.baseactivities.base.MvpPresenter;
import com.crossingguard.models.logins.LoginRequestModel;

/**
 * Created by rahul on 6/14/2018.
 */

public interface SuperVisorHomeMvpPresenter<V extends SuperVisorHomeMvpView>  extends MvpPresenter<V> {
    void hitLogin(LoginRequestModel loginRequestModel);
}