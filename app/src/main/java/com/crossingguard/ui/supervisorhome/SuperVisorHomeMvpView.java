package com.crossingguard.ui.supervisorhome;

import com.crossingguard.baseactivities.base.MvpView;
import com.crossingguard.models.logins.LoginResponseModel;

import retrofit2.Response;


public interface SuperVisorHomeMvpView extends MvpView {

    void onSuccess(Response<LoginResponseModel> response);

    void onFailure(Throwable error);

}
