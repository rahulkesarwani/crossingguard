package com.crossingguard.ui.supervisorhome;

import com.crossingguard.baseactivities.base.BasePresenter;
import com.crossingguard.controller.IDataManager;
import com.crossingguard.models.logins.LoginRequestModel;
import com.crossingguard.models.logins.LoginResponseModel;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 *
 * */
public class SuperVisorHomePresenter<V extends SuperVisorHomeMvpView> extends BasePresenter<V> implements SuperVisorHomeMvpPresenter<V> {


    public SuperVisorHomePresenter(IDataManager IDataManager) {
        super(IDataManager);
    }

    @Override
    public void hitLogin(LoginRequestModel loginRequestModel) {

        getIDataManager().getCompositeDisposable().add(getIDataManager().getApiInterface().hitLoginApi(loginRequestModel)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse, this::handleError));

    }


    private void handleResponse(Response<LoginResponseModel> value) {
        getMvpView().onSuccess(value);
    }

    private void handleError(Throwable error) {

        getMvpView().onFailure(error);
    }

}
