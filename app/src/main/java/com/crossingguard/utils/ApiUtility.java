package com.crossingguard.utils;

import android.content.Context;

import com.crossingguard.BuildConfig;
import com.crossingguard.MyApp;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.concurrent.TimeUnit;

import io.reactivex.schedulers.Schedulers;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by rahul on 1/5/2018.
 */

public class ApiUtility {

    private static ApiUtility instance = null;

    public static ApiUtility getInstance() {
        if (instance != null) {
            return instance;
        } else {
            instance = new ApiUtility();
            return instance;
        }
    }

    public Retrofit getRetrofit() {
       return getRetrofitInstance(MyApp.getInstance());
    }

    public Retrofit getRetrofit(Context context) {
        return getRetrofitInstance(context);
    }
    public Retrofit getRetrofitInstance(Context context){
        Retrofit retrofit = null;


        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG) {
//                httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        } else {
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
        }


        Interceptor interceptor = chain -> {
            Request.Builder builder = chain.request().newBuilder();

            builder.header("Content-Type", "application/json; charset=utf-8")
                    .header("Authorization", Helper.getAccesstoken(context))
                    .header("Accept", "application/json");

            return chain.proceed(builder.build());

        };

        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(2, TimeUnit.MINUTES).readTimeout(2, TimeUnit.MINUTES)
                .writeTimeout(2, TimeUnit.MINUTES).addInterceptor(interceptor).addInterceptor(httpLoggingInterceptor).build();

        Gson gson = new GsonBuilder().serializeNulls().create();
        retrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.mBaseUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build();
        return retrofit;
    }
}