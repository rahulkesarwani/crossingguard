package com.crossingguard.utils;

/**
 * Created by rahul on 1/5/2018.
 */

public class AppConstants {

    /*Urls */

    public static final String mRegisterDeviceUrl = "http://apipush.sia.co.in/api/";
    public static final String mBaseUrl = "http://138.197.148.79/api/";
    public static final String mRefreshTokenBaseURL = "https://apiv3.myswaasth.com/o/";
    public static final String mBaseWSS = "wss://apiv3.myswaasth.com/";
    public static final String mImageBaseUrl = "https://apiv3.myswaasth.com";


    /**/
    public static final String ADDRESS = "CURRENT_ADDRESS";
    public static final String LATITUDE = "CURRENT_LATITUDE";
    public static final String LONGITUDE = "CURRENT_LONGITUDE";
    public static final String ACCESS_TOKEN = "ACCESS_TOKEN";

    /*for refresh token*/
    public final static String FIRST_TIME_LOGIN_KEY = "firstTimeLoginTime";
    public final static String CLIENT_ID = "client_id";
    public final static String CLIENT_SECRET = "client_secret";
    public final static String TOKEN_EXPIRY_TIME = "expires_in";
    public final static String REFRESH_TOKEN = "refresh_token";
    public final static String mGrantType = "refresh_token";
    public final static String EXTRA_ANIMAL_IMAGE_TRANSITION_NAME = "EXTRA_ANIMAL_IMAGE_TRANSITION_NAME";


    public static final String MAP_FRAGMENT = "MAP_FRAGMENT";
    public static final String MAP_GUARD_LIST_FRAGMENT = "MAP_GUARD_LIST_FRAGMENT";
    public static final String LOCATION_LIST_FRAGMENT = "LOCATION_LIST_FRAGMENT";
    public static final String ASSIGNMENT_FRAGMENT = "ASSIGNMENT_FRAGMENT";
    public static final String CHECKIN_LOG_FRAGMENT = "CHECKIN_LOG_FRAGMENT";
    public static final String COMPLIANCE_LOG_FRAGMENT = "COMPLIANCE_LOG_FRAGMENT";

    public static final String GEOFENCE_FRAGMENT = "GEOFENCE_FRAGMENT";
    public static final String LOGIN_DATA = "LOGIN_DATA";
    public static final String LOGIN_TYPE = "LOGIN_TYPE";
    public static final String GUARD_DATA = "GUARD_DATA";

    public static final String GEOFENCES_ADDED_KEY = "GEOFENCES_ADDED_KEY";
    public static final String LOCATION_DATA = "LOCATION_DATA";
    public static final String CHANNEL_ID = "crossing_guard";
    public static final String NOTIFIC_CHANNEL_ID = "crossing_guard_notif";
    public static final String GEOFENCE_REQUEST_ID = "geofence";
    public static final String GUARD_LOCATION_DATA = "GUARD_LOCATION_DATA";
    public static final String GUARD_ASSIGNED_LOCATION = "GUARD_ASSIGNED_LOCATION";
    public static final String IS_TOKEN_REFRESHED = "IS_TOKEN_REFRESHED";
    public static final String FCM_TOKEN = "FCM_TOKEN";
    public static final String NOTIFICATION_COUNTER = "NOTIFICATION_COUNTER";


    public static final String PUSH_NOTIFICATION = "pushNotification";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;
    public static final String LAST_CHECKED_IN_LOCATION = "LAST_CHECKED_IN_LOCATION";
    public static final String USER_ID = "USER_ID";
    public static final String SUPERVISOR_ID = "SUPERVISOR_ID";


    public interface GUARD_TYPE {
        String FULL_TIME = "full_time";
        String PART_TIME = "part_time";
        String ON_CALL = "on_call";
    }

    public interface GUARD_STATUS {
        String ALL = "all";
        String APPLICANT = "applicant";
        String ACTIVE = "active";
        String NOT_ACTIVE = "inactive";
    }

    public interface COMPLIANCE_FILTER {
        String ACTIVE = "active";
        String ISSUES = "issues";
    }

    public interface FIREBASE_REFERENCE {
        String BROAD_CAST_MESSAGE = "broadcastmessages";
        String USERS = "users";
    }

    public interface EVENT_CONSTANTS {
        String EVENT_BROADCAST_NOTIFICATION = "EVENT_BROADCAST_NOTIFICATION";
    }
}
