package com.crossingguard.utils;

import android.content.Context;
import android.util.Base64;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Helper {

    public static void handleErrorBody(Context context, String errorResponse) {

        try {
            JSONObject jObjError = new JSONObject(errorResponse);
            Toast.makeText(context, jObjError.getString("message"), Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }


    public static String getErrorMessage(Context context, String errorResponse) {

        String msg = "";
        try {
            JSONObject jObjError = new JSONObject(errorResponse);
            msg = jObjError.getString("message");
        } catch (Exception e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
        }
        return msg;
    }

    public static String getAccesstoken(Context context) {
        MySharedPreferences mySharedPreferences = new MySharedPreferences(context);
        String accessToken = mySharedPreferences.getString(AppConstants.ACCESS_TOKEN, "");
        return "Bearer " + accessToken;
    }

    public static String convertDateFormmat(String dateString, String givenFormat, String requiredFormat) {
        String result = "";
        SimpleDateFormat simpleDateFormat1 = null; //"2017-02-21" //"yyyy-MM-dd"
        SimpleDateFormat simpleDateFormat2 = null;
        Date tempDate = null;

        try {
            simpleDateFormat1 = new SimpleDateFormat(givenFormat);
            simpleDateFormat2 = new SimpleDateFormat(requiredFormat);

            tempDate = simpleDateFormat1.parse(dateString);
            result = simpleDateFormat2.format(tempDate);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getBase64Secret(String text) {
        // Sending side
        String encodedData = null;
        try {
            byte[] data = text.getBytes("UTF-8");
            encodedData = Base64.encodeToString(data, Base64.DEFAULT);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return encodedData;
    }
}
