package com.crossingguard.utils;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.crossingguard.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeAgo {


    String pastDate;
    private String TAG = TimeAgo.class.getCanonicalName();
    @Nullable
    Context context;

    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;
    private static final int WEEKS_MILLIS = 7 * DAY_MILLIS;
    private static final int MONTHS_MILLIS = 4 * WEEKS_MILLIS;
    private static final int YEARS_MILLIS = 12 * MONTHS_MILLIS;

    public TimeAgo() {
    }

    public TimeAgo locale(@NonNull Context context) {
        this.context = context;
        return this;
    }

    public TimeAgo with(@NonNull SimpleDateFormat simpleDateFormat) {
        return this;
    }

    public long getTimeStampFromDate(String dateStr, String format) {
        DateFormat formatter = new SimpleDateFormat(format);
        Date date = null;
        try {
            date = formatter.parse(dateStr);
            return date.getTime();
        } catch (Exception e) {
            e.printStackTrace();
            return 0l;
        }
    }

    public String getTimeAgo(String actualDate, String format) {

        long different = System.currentTimeMillis() - getTimeStampFromDate(actualDate, format);

        if (different < MINUTE_MILLIS) {
            return context.getResources().getString(R.string.just_now);
        } else if (different < 2 * MINUTE_MILLIS) {
            return context.getResources().getString(R.string.a_min_ago);
        } else if (different < 50 * MINUTE_MILLIS) {
            return different / MINUTE_MILLIS + context.getString(R.string.mins_ago);
        } else if (different < 90 * MINUTE_MILLIS) {
            return context.getString(R.string.a_hour_ago);
        } else if (different < 24 * HOUR_MILLIS) {
            // timeFromData = timeFormat.format(startDate);
            int hours = (int) ((different / (1000 * 60 * 60)) % 24);
            return "" + hours + " hrs ago";
        } else if (different < 48 * HOUR_MILLIS) {
            return context.getString(R.string.yesterday);
        } else if (different < 7 * DAY_MILLIS) {
            return different / DAY_MILLIS + context.getString(R.string.days_ago);
        } else if (different < 2 * WEEKS_MILLIS) {
            return different / WEEKS_MILLIS + context.getString(R.string.week_ago);
        } else if (different < 3.5 * WEEKS_MILLIS) {
            return different / WEEKS_MILLIS + context.getString(R.string.weeks_ago);
        } else {
            //pastDate = dateFormat.format(startDate);
            pastDate = TimeFormatHelper.convertDateFormmat(actualDate, format, "MMMM dd, yyyy hh:mm a");
            return pastDate;
        }

    }

    public String getCommentTimeAgo(String actualDate, String format) {

        long different = System.currentTimeMillis() - getTimeStampFromDate(actualDate, format);

        if (different < MINUTE_MILLIS) {
            return context.getResources().getString(R.string.just_now);
        } else if (different < 2 * MINUTE_MILLIS) {
            return "1 min";
        } else if (different < 50 * MINUTE_MILLIS) {
            return different / MINUTE_MILLIS + " mins";
        } else if (different < 90 * MINUTE_MILLIS) {
            return "1 hour";
        } else if (different < 24 * HOUR_MILLIS) {
            // timeFromData = timeFormat.format(startDate);
            int hours = (int) ((different / (1000 * 60 * 60)) % 24);
            return "" + hours + " hours";
        }else {
            //pastDate = dateFormast.format(startDate);
            pastDate = TimeFormatHelper.convertDateFormmat(actualDate, format, "MMM dd");
            return pastDate;
        }

    }
}