package com.crossingguard.utils;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TimeFormatHelper {
    private static final String TAG = "TimeFormatHelper";

    private TimeFormatHelper() {

    }


    public static String convertDateFormmat(String dateString, String givenFormat, String requiredFormat) {
        String result = "";
        SimpleDateFormat simpleDateFormat1 = null; //"2017-02-21" //"yyyy-MM-dd"
        SimpleDateFormat simpleDateFormat2 = null;
        Date tempDate = null;

        try {
            simpleDateFormat1 = new SimpleDateFormat(givenFormat);
            simpleDateFormat2 = new SimpleDateFormat(requiredFormat);

            tempDate = simpleDateFormat1.parse(dateString);
            result = simpleDateFormat2.format(tempDate);
        } catch (ParseException e) {
            LogHelper.e(TAG, "convertDateFormmat -->> ParseException -->> ", e);
//            ExceptionHelper.handleParseException(TAG, e);
            e.printStackTrace();
        } catch (NullPointerException e) {
            LogHelper.e(TAG, "convertDateFormmat -->> NullPointerException -->> ", e);
//            ExceptionHelper.handleNullPointerException(TAG, e);
            e.printStackTrace();
        }
        return result;
    }

}
