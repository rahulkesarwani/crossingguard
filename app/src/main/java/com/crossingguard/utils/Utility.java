package com.crossingguard.utils;

import android.app.ActivityManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.core.content.ContextCompat;

import com.crossingguard.service.GeofenceLocationService;

public class Utility {
    public static boolean isMyServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }


    public static void createNotificationChannel(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(AppConstants.CHANNEL_ID,
                    "CrossingGuard Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );

            NotificationManager manager = context.getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
            //Toast.makeText(this, "Notification Channel Created", Toast.LENGTH_SHORT).show();
        }
    }

    public static void StartCheckerService(Context context) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Intent serviceIntent = new Intent(context, GeofenceLocationService.class);
            ContextCompat.startForegroundService(context, serviceIntent);
        } else {
            Intent serviceIntent = new Intent(context, GeofenceLocationService.class);
            context.startService(serviceIntent);
        }
    }

    public static void StopService(Context context) {

        context.stopService(new Intent(context, GeofenceLocationService.class));

    }

}
