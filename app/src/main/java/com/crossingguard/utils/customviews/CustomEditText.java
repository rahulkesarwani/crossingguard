package com.crossingguard.utils.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import androidx.appcompat.widget.AppCompatEditText;
import android.util.AttributeSet;

import com.crossingguard.R;


/**
 * Created by rahul on 11/4/17.
 */

public class CustomEditText extends AppCompatEditText {
    public CustomEditText(Context context) {
        super(context);
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context,attrs);
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setCustomFont(context,attrs);
    }

    private void setCustomFont(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.CustomTextView);
        String customFont = a.getString(R.styleable.CustomTextView_customFont);
        setCustomFont(ctx, customFont);
        a.recycle();
    }

    public boolean setCustomFont(Context ctx, String asset) {
        Typeface tf = null;

        switch (asset){
            case "bold":
                tf = Typeface.createFromAsset(ctx.getAssets(), "fonts/Montserrat-Bold.ttf");
                break;
            case "light":
                tf = Typeface.createFromAsset(ctx.getAssets(), "fonts/Montserrat-Light.ttf");
                break;
            case "medium":
                tf = Typeface.createFromAsset(ctx.getAssets(), "fonts/Montserrat-Medium.ttf");
                break;
            case "regular":
                tf = Typeface.createFromAsset(ctx.getAssets(), "fonts/Montserrat-Regular.ttf");
                break;
            case "semibold":
                tf = Typeface.createFromAsset(ctx.getAssets(), "fonts/Montserrat-SemiBold.ttf");
                break;
        }
        setTypeface(tf);
        return true;
    }
}
