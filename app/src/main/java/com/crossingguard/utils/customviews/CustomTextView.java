package com.crossingguard.utils.customviews;

/**
 * Created by rahul on 10/4/17.
 */


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import androidx.appcompat.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.crossingguard.R;


public class CustomTextView extends AppCompatTextView {
    private static final String TAG = "CustomTextView";

    public CustomTextView(Context context, String typeface) {
        super(context);
       setCustomFont(context,typeface);
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFont(context, attrs);
    }

    private void setCustomFont(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.CustomTextView);
        String customFont = a.getString(R.styleable.CustomTextView_customFont);
        setCustomFont(ctx, customFont);
        a.recycle();
    }

    public boolean setCustomFont(Context ctx, String asset) {
        Typeface tf = null;

        switch (asset){
            case "bold":
                tf = Typeface.createFromAsset(ctx.getAssets(), "fonts/Montserrat-Bold.ttf");
                break;
            case "light":
                tf = Typeface.createFromAsset(ctx.getAssets(), "fonts/Montserrat-Light.ttf");
                break;
            case "medium":
                tf = Typeface.createFromAsset(ctx.getAssets(), "fonts/Montserrat-Medium.ttf");
                break;
            case "regular":
                tf = Typeface.createFromAsset(ctx.getAssets(), "fonts/Montserrat-Regular.ttf");
                break;
            case "semibold":
                tf = Typeface.createFromAsset(ctx.getAssets(), "fonts/Montserrat-SemiBold.ttf");
                break;
        }

        setTypeface(tf);
        return true;

    }

}